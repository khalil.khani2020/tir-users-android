# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


-dontwarn com.google.errorprone.annotations.**
-keep class com.google.errorprone.annotations.** { *; }
-keep interface com.google.errorprone.annotations.** { *; }

-dontwarn com.google.maps.**
-keep class com.google.maps.** { *; }
-keep interface com.google.maps.** { *; }

-dontwarn com.google.android.gms.**
-keep public class com.google.android.gms.** { *; }
-keep public  interface com.google.android.gms.** { *; }

-dontwarn com.google.gson.internal.**
-keep class com.google.gson.internal.** { *; }
-keep interface com.google.gson.internal.** { *; }

-dontwarn javax.annotation.**
-keep class javax.annotation.** { *; }
-keep interface javax.annotation.** { *; }

-dontwarn org.codehaus.mojo.animal_sniffer.**
-keep class org.codehaus.mojo.animal_sniffer.** { *; }
-keep interface org.codehaus.mojo.animal_sniffer.** { *; }

-dontwarn com.facebook.**
-keep class com.facebook.** { *; }
-keep interface com.facebook.** { *; }

#-dontwarn com.facebook.cache.disk.SettableCacheEvent.**
#-keep class com.facebook.cache.disk.SettableCacheEvent.** { *; }
#-keep interface com.facebook.cache.disk.SettableCacheEvent.** { *; }

-dontwarn org.conscrypt.**
-keep class org.conscrypt.** { *; }
-keep interface org.conscrypt.** { *; }

-dontwarn org.joda.time.**
-keep class org.joda.time.** { *; }
-keep interface org.joda.time.** { *; }

-dontwarn com.tir.user.repository.model.**
-keep class com.tir.user.repository.model.** { *; }
-keep interface com.tir.user.repository.model.** { *; }

-keep class com.zl.reik.dilatingdotsprogressbar.DilatingDotDrawable { *; }

#-dontwarn com.zl.reik.dilatingdotsprogressbar.**
#-keep class com.zl.reik.dilatingdotsprogressbar.** { *; }
#-keep interface com.zl.reik.dilatingdotsprogressbar.** { *; }

#-dontwarn android.arch.**
#-keep class android.arch.** { *; }
#-keep interface android.arch.** { *; }

### Android architecture components: Lifecycle
## LifecycleObserver's empty constructor is considered to be unused by proguard
#-keepclassmembers class * implements android.arch.lifecycle.LifecycleObserver {
#    <init>(...);
#}
## ViewModel's empty constructor is considered to be unused by proguard
#-keepclassmembers class * extends android.arch.lifecycle.ViewModel {
#    <init>(...);
#}
## keep Lifecycle State and Event enums values
#-keepclassmembers class android.arch.lifecycle.Lifecycle$State { *; }
#-keepclassmembers class android.arch.lifecycle.Lifecycle$Event { *; }
## keep methods annotated with @OnLifecycleEvent even if they seem to be unused
## (Mostly for LiveData.LifecycleBoundObserver.onStateChange(), but who knows)
#-keepclassmembers class * {
#    @android.arch.lifecycle.OnLifecycleEvent *;
#}
#
#-keepclassmembers class * implements android.arch.lifecycle.LifecycleObserver {
#    <init>(...);
#}
#
#-keep class * implements android.arch.lifecycle.LifecycleObserver {
#    <init>(...);
#}
#-keepclassmembers class android.arch.** { *; }
#-keep class android.arch.** { *; }
#-dontwarn android.arch.**
#
#-assumenosideeffects class java.io.PrintStream {
#     public void println(%);
#     public void println(**);
# }

-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

#-assumenosideeffects class android.util.Log {
#    public static boolean isLoggable(java.lang.String, int);
#    public static int v(...);
#    public static int i(...);
#    public static int w(...);
#    public static int d(...);
#    public static int e(...);
#}