package com.tir.user.register;


import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.IntentFilter;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.Telephony;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentVerifyPhoneBinding;
import com.tir.user.receivers.SmsReceiver;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseVerifyPhone;
import com.tir.user.repository.model.api.response.SendSms;


public class FragmentVerifyPhone extends BaseFragment
        implements SmsReceiver.Listener {

    private MediatorLiveData<NetworkResponse<ResponseVerifyPhone>> verifyMediator;
    private MediatorLiveData<NetworkResponse<SendSms>> registerMediator;
    private FragmentVerifyPhoneBinding binding;
    private ViewModelRegister viewModel;
    private OnFragmentInteractionListener mListener;
    private SmsReceiver smsReceiver;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_verify_phone, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelRegister.class);
        setupForm();
        setupSmsReceiver();
        return binding.getRoot();
    }

    private void setupSmsReceiver() {
        smsReceiver = new SmsReceiver(getString(R.string.sms_verification_sender),
                getString(R.string.sms_verification_text));
        smsReceiver.setListener(this);
        getActivity().registerReceiver(smsReceiver, new IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION));
    }

    private void setupForm() {
        binding.countdownView.setOnCountdownEndListener(cv -> activateResend(true));

        binding.txtResendCode.setOnClickListener(v -> resendSMS());

        binding.txtSubmit.setOnClickListener(view -> verifyPhoneNumber());

        startTimer();
    }

    private void resendSMS() {
        viewModel.setParamVerification(null);
        if (registerMediator == null) {
            registerMediator = viewModel.resendSMS();
            registerMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                showForm();
                                startTimer();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    case FAILURE:
                        showError();
                        break;
                }
            });
        } else {
            viewModel.resendSMS();
        }
    }

    private void verifyPhoneNumber() {
        if (validateForm()) return;
        if (verifyMediator == null) {
            verifyMediator = viewModel.verifyPhoneNumber();
            verifyMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.setVerification(networkResponse.getApiResponse().data);
                                if (viewModel.isForgotPassword)
                                    mListener.onContinueToResetPassword();
                                else
                                    mListener.onContinueToRegister();
                                break;
                            case ApiResponse.VERIFICATION_SMS_EXPIRED_OR_NOT_VALID:
                                Toast.makeText(getContext(), R.string.incorrect_verification_code, Toast.LENGTH_LONG).show();
                                showForm();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        } else {
            viewModel.verifyPhoneNumber();
        }
    }

    private boolean validateForm() {
        String vCode = binding.eTxtVerificationCode.getText().toString();
        if (TextUtils.isEmpty(vCode)) {
            binding.eTxtVerificationCode.setError(getString(R.string.enter_verification_code), null);
            binding.eTxtVerificationCode.requestFocus();
            return true;
        }
        viewModel.setParamVerification(vCode);
        return false;
    }

    private void startTimer() {
        activateResend(false);
        binding.countdownView.start(60 * 1000);
    }

    private void activateResend(boolean activate) {
        if (activate) {
            binding.txtResendCode.setClickable(true);
            binding.txtResendCode.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        } else {
            binding.txtResendCode.setClickable(false);
            binding.txtResendCode.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_500));
        }
    }

    private void showProgress() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showForm() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.VISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showError() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> {
                if (viewModel.getParamVerification() == null) {
                    viewModel.resendSMS();
                } else {
                    viewModel.verifyPhoneNumber();
                }
            });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentVerifyPhone.OnFragmentInteractionListener) {
            mListener = (FragmentVerifyPhone.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(smsReceiver);
    }

    @Override
    public void onTextReceived(String text) {
        try {
            int vCode = Integer.valueOf(text.trim());
            binding.eTxtVerificationCode.setText(String.valueOf(vCode));
            verifyPhoneNumber();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnFragmentInteractionListener {
        void onContinueToApp();
        void onContinueToResetPassword();
        void onContinueToRegister();
    }
}
