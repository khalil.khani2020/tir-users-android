package com.tir.user.register;


import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentLoginBinding;
import com.tir.user.repository.model.api.ApiResponse;

public class FragmentLogin extends BaseFragment {

    private FragmentLoginBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelRegister viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelRegister.class);

        setupForm();
        return binding.getRoot();
    }

    private void setupForm() {
        binding.spinnerAreaCode.setAdapter(new AdapterSpinnerCountry(getContext(), viewModel.getCountries()));
        binding.txtRegister.setPaintFlags(binding.txtRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        binding.txtForgotPassword.setPaintFlags(binding.txtForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        binding.txtSubmit.setOnClickListener(view -> {
            if (validateForm()) return;
            viewModel.login().observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.setSession(networkResponse.getApiResponse().data);
//                                Toast.makeText(getContext(), networkResponse.getApiResponse().state.message, Toast.LENGTH_LONG).show();
                                mListener.onContinueToApp();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        });

        binding.txtRegister.setOnClickListener(view -> {
            viewModel.isForgotPassword = false;
            mListener.onContinueToSendSms();

        });

        binding.txtForgotPassword.setOnClickListener(view -> {
            viewModel.isForgotPassword = true;
            mListener.onContinueToSendSms();
        });
    }

    private boolean validateForm() {
        String mobilePhone = binding.eTxtPhone.getText().toString();
        if (TextUtils.isEmpty(mobilePhone)){
            binding.eTxtPhone.setError(getString(R.string.enter_mobile_phone_number), null);
            binding.eTxtPhone.requestFocus();
            return true;
        }
        // TODO: 5/14/2018 validate phone numbers for iran
        Long phoneNum;
        try {
            phoneNum = Long.parseLong(binding.eTxtPhone.getText().toString());
        } catch (Exception e) {
            binding.eTxtPhone.setError(getString(R.string.invalid_phone_num), null);
            binding.eTxtPhone.requestFocus();
            e.printStackTrace();
            return true;
        }

//        String phone = String.valueOf(phoneNum);
//        if (phone.length() != 10) {
//            binding.eTxtPhone.setError(getString(R.string.invalid_phone_num), null);
//            binding.eTxtPhone.requestFocus();
//            return true;
//        }

        String password = binding.eTxtPassword.getText().toString();
        if (TextUtils.isEmpty(password)){
            binding.eTxtPassword.setError(getString(R.string.enter_password), null);
            binding.eTxtPassword.requestFocus();
            return true;
        }

        viewModel.setLoginInfo(
                viewModel.getCountries().get((int) binding.spinnerAreaCode.getSelectedItemId()).countryCode,
                String.valueOf(phoneNum), password);
        return false;
    }

    private void showProgress() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showForm() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.VISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showError() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> {
                showProgress();
                viewModel.login();
            });
        }
    }

    public void confirmForm(){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentLogin.OnFragmentInteractionListener) {
            mListener = (FragmentLogin.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onContinueToSendSms();
        void onContinueToApp();
    }
}
