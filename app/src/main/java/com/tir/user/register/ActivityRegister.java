package com.tir.user.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tir.user.R;
import com.tir.user.app.Const;
import com.tir.user.app.StaticVariables;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityRegisterBinding;
import com.tir.user.home.ActivityHome;
import com.tir.user.profile.ActivityProfile;
import com.tir.user.utils.Utils;
import com.tir.user.webview.ActivityWebView;

public class ActivityRegister extends BaseActivity
        implements FragmentSendSms.OnFragmentInteractionListener
        , FragmentVerifyPhone.OnFragmentInteractionListener
        , FragmentLogin.OnFragmentInteractionListener
        , FragmentResetPassword.OnFragmentInteractionListener
        , FragmentRegister.OnFragmentInteractionListener{

    private static final int RIGHT_TO_LEFT = 0;
    private static final int LEFT_TO_RIGHT = 1;
    private MenuItem menuItem;
    private ViewModelRegister viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        ActivityRegisterBinding binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_register, contentFrameLayout, true);
        setSupportActionBar(binding.toolbar);
        setToolbarFont(binding.toolbar);
        ////////////////////////////////////////////////////////////////////////////////////////////
        getSupportActionBar().hide();
        viewModel = ViewModelProviders.of(this).get(ViewModelRegister.class);
        setupMainFrag();
    }

    private void setupMainFrag() {
        FragmentLogin mainFrag = new FragmentLogin();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mainFrag, mainFrag.getClass().getSimpleName())
                .addToBackStack(mainFrag.getClass().getSimpleName())
                .commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void launchFragHorizontally(Fragment fragment, int direction) {
        int a1, a2, a3, a4;
        if (direction == LEFT_TO_RIGHT) {
            a1 = R.anim.enter_from_left;
            a2 = R.anim.exit_to_right;
            a3 = R.anim.enter_from_right;
            a4 = R.anim.exit_to_left;
        } else {
            a1 = R.anim.enter_from_right;
            a2 = R.anim.exit_to_left;
            a3 = R.anim.enter_from_left;
            a4 = R.anim.exit_to_right;
        }
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(a1, a2, a3, a4)
                .replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
//        getSupportFragmentManager().executePendingTransactions();
    }

    private void clearBackStack() {
        while (getSupportFragmentManager().getBackStackEntryCount() > 0){
            getSupportFragmentManager().popBackStackImmediate();
        }
        getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public void onShowAgreements() {
        Intent intent = new Intent(getContext(), ActivityWebView.class);
        intent.putExtra(ActivityWebView.URL, Const.Endpoints.AGREEMENTS_URL);
        startActivity(intent);
    }

    @Override
    public void onCheckAgreements(boolean showMenuItem) {
        menuItem.setVisible(showMenuItem);
    }

    @Override
    public void onContinueToVerify() {
        launchFragHorizontally(new FragmentVerifyPhone(), RIGHT_TO_LEFT);
        Utils.hideKeyboard(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_profile, menu);
        menuItem = menu.findItem(R.id.confirm);
        menuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String tag = getLastFragName();
        switch (item.getItemId()) {
            case R.id.confirm:
                if (tag.equals(FragmentSendSms.class.getSimpleName())) {
                    ((FragmentSendSms) getSupportFragmentManager().findFragmentByTag(tag)).confirmForm();
                } else if (tag.equals(FragmentAgreements.class.getSimpleName())) {
                    ((FragmentAgreements) getSupportFragmentManager().findFragmentByTag(tag)).confirmForm();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        return this.viewModel;
    }

    private String getLastFragName() {
        return getSupportFragmentManager()
                .getBackStackEntryAt(getSupportFragmentManager()
                        .getBackStackEntryCount() - 1).getName();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    @Override
    public void onContinueToSendSms() {
        launchFragHorizontally(new FragmentSendSms(), LEFT_TO_RIGHT);
    }

    @Override
    public void onContinueToLogin() {
        clearBackStack();
        launchFragHorizontally(new FragmentSendSms(), LEFT_TO_RIGHT);
    }

    @Override
    public void onContinueToRegister() {
        launchFragHorizontally(new FragmentRegister(), RIGHT_TO_LEFT);
    }

    @Override
    public void onContinueToApp() {
        if (StaticVariables.SESSION.isProfileCompleted) {
            startActivity(new Intent(this, ActivityHome.class));
            finish();
        } else {
            startActivity(new Intent(this, ActivityProfile.class));
            finish();
        }
    }

    @Override
    public void onContinueToResetPassword() {
        clearBackStack();
        launchFragHorizontally(new FragmentResetPassword(), LEFT_TO_RIGHT);
    }
}
