package com.tir.user.register;


import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentSendSmsBinding;
import com.tir.user.repository.model.api.ApiResponse;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

public class FragmentSendSms extends BaseFragment {

    private FragmentSendSmsBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelRegister viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_send_sms, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelRegister.class);

        setupForm();
        return binding.getRoot();
    }

    private void setupForm() {
        binding.spinnerAreaCode.setAdapter(new AdapterSpinnerCountry(getContext(), viewModel.getCountries()));

        if (viewModel.isForgotPassword) {
            binding.txtIAgree.setVisibility(View.INVISIBLE);
            binding.txtAgreements.setVisibility(View.INVISIBLE);
            binding.checkboxAgreements.setVisibility(View.INVISIBLE);
        } else {
            binding.txtIAgree.setVisibility(View.VISIBLE);
            binding.txtAgreements.setVisibility(View.VISIBLE);
            binding.checkboxAgreements.setVisibility(View.VISIBLE);
            binding.checkboxAgreements.setOnCheckedChangeListener((compoundButton, b) -> mListener.onCheckAgreements(b));
            binding.txtAgreements.setPaintFlags(binding.txtAgreements.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            binding.txtAgreements.setOnClickListener(view -> mListener.onShowAgreements());
        }

        binding.txtSubmit.setOnClickListener(view -> {
            if (validateForm()) return;
            register();
        });
    }

    private void register() {
        viewModel.sendSms().observe(this, networkResponse -> {
            switch (networkResponse.getStatus()) {
                case LOADING:
                    showProgress();
                    break;
                case FAILURE:
                    showError();
                    break;
                case SUCCESS:
                    switch (networkResponse.getApiResponse().status.code) {
                        case ApiResponse.COMMON_SUCCESSFUL:
                            mListener.onContinueToVerify();
                            break;
                        case ApiResponse.COMMON_FAILURE:
                            showError();
                            break;
                        default:
                            showMessage(networkResponse.getApiResponse().status.message);
                            showForm();
                            break;
                    }
                    break;
                default:
                    showError();
                    break;
            }
        });
    }

    private boolean validateForm() {
        String mobilePhone = binding.eTxtPhone.getText().toString();
        if (TextUtils.isEmpty(mobilePhone)) {
            binding.eTxtPhone.setError(getString(R.string.enter_mobile_phone_number), null);
            binding.eTxtPhone.requestFocus();
            return true;
        }
        if (!viewModel.isForgotPassword) {
            if (!binding.checkboxAgreements.isChecked()) {
                Toast.makeText(getContext(), R.string.accepting_license_is_required, Toast.LENGTH_LONG).show();
                return true;
            }
        }
        // TODO: 5/14/2018 validate phone numbers for iran
        Long phoneNum;
        try {
            phoneNum = Long.parseLong(binding.eTxtPhone.getText().toString());
        } catch (Exception e) {
            binding.eTxtPhone.setError(getString(R.string.invalid_phone_num), null);
            binding.eTxtPhone.requestFocus();
            e.printStackTrace();
            return true;
        }

        String phone = String.valueOf(phoneNum);
        if (phone.length() != 10) {
            binding.eTxtPhone.setError(getString(R.string.invalid_phone_num), null);
            binding.eTxtPhone.requestFocus();
            return false;
        }
        viewModel.setSendSmsInfo(
                viewModel.getCountries().get((int) binding.spinnerAreaCode.getSelectedItemId()).countryCode,
                String.valueOf(phoneNum));
        return false;
    }

    private void showProgress() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.txtAgreements.setVisibility(View.GONE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showForm() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.VISIBLE);
        binding.txtAgreements.setVisibility(View.VISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showError() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.txtAgreements.setVisibility(View.GONE);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> {
                showProgress();
                viewModel.sendSms();
            });
        }
    }

    public void confirmForm() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentSendSms.OnFragmentInteractionListener) {
            mListener = (FragmentSendSms.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onShowAgreements();

        void onCheckAgreements(boolean showMenuItem);

        void onContinueToVerify();
    }
}
