package com.tir.user.register;


import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tir.user.R;
import com.tir.user.app.StaticVariables;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentResetPasswordBinding;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.room.entity.Session;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

public class FragmentResetPassword extends BaseFragment {

    private FragmentResetPasswordBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelRegister viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reset_password, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelRegister.class);

        setupForm();
        return binding.getRoot();
    }

    private void setupForm() {
        binding.txtSubmit.setOnClickListener(view -> {
            if (validateForm()) return;
            viewModel.resetPassword().observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                mListener.onContinueToLogin();
//                                Toast.makeText(getContext(), networkResponse.getApiResponse().state.message, Toast.LENGTH_LONG).show();
                                mListener.onContinueToApp();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        });
    }

    private void populateSession(Session session) {
        StaticVariables.SESSION = session;
    }

    private boolean validateForm() {
        String password = binding.eTxtPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            binding.eTxtPassword.setError(getString(R.string.enter_password), null);
            binding.eTxtPassword.requestFocus();
            return true;
        }

        String confirmPassword = binding.eTxtConfirmPassword.getText().toString();
        if (TextUtils.isEmpty(confirmPassword) || !password.equals(confirmPassword)) {
            binding.eTxtConfirmPassword.setError(getString(R.string.password_confirmation_is_invalid), null);
            binding.eTxtConfirmPassword.requestFocus();
            return true;
        }

        viewModel.setPasswordInfo(password, confirmPassword);
        return false;
    }

    private void showProgress() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showForm() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.VISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showError() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> {
                showProgress();
                viewModel.resetPassword();
            });
        }
    }

    public void confirmForm() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentResetPassword.OnFragmentInteractionListener) {
            mListener = (FragmentResetPassword.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onContinueToLogin();
        void onContinueToApp();
    }
}
