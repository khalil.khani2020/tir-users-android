package com.tir.user.register;


import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tir.user.R;
import com.tir.user.app.MyApp;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentRegisterBinding;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.parameter.ParamRegister;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

public class FragmentRegister extends BaseFragment {

    private FragmentRegisterBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelRegister viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelRegister.class);

        setupForm();
        return binding.getRoot();
    }

    private void setupForm() {
        binding.textInputPassword.setTypeface(MyApp.getInstance().getTypeFace(R.font.iran_sans));
        binding.textInputConfirmPassword.setTypeface(MyApp.getInstance().getTypeFace(R.font.iran_sans));

        binding.txtSubmit.setOnClickListener(view -> {
            if (validateForm()) return;
            viewModel.register().observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.setSession(networkResponse.getApiResponse().data);
                                mListener.onContinueToApp();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        });
    }

    private boolean validateForm() {
        // TODO: 21/11/2018 refactor this:
        String firstName = binding.etxtFirstName.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            binding.etxtFirstName.setError(getString(R.string.field_is_required), null);
            binding.etxtFirstName.requestFocus();
            return true;
        }

        String lastName = binding.etxtLastName.getText().toString();
        if (TextUtils.isEmpty(lastName)) {
            binding.etxtLastName.setError(getString(R.string.field_is_required), null);
            binding.etxtLastName.requestFocus();
            return true;
        }

        String password = binding.etxtPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            binding.etxtPassword.setError(getString(R.string.field_is_required), null);
            binding.etxtPassword.requestFocus();
            return true;
        } else if (password.length() < 8) {
            binding.etxtPassword.setError(getString(R.string.password_min_length_is_8), null);
            binding.etxtPassword.requestFocus();
            return true;
        }

        String confirmPassword = binding.etxtConfirmPassword.getText().toString();
        if (TextUtils.isEmpty(confirmPassword) || !password.equals(confirmPassword)) {
            binding.etxtConfirmPassword.setError(getString(R.string.password_confirmation_is_invalid), null);
            binding.etxtConfirmPassword.requestFocus();
            return true;
        }

        String userType = binding.radioGroup.getCheckedRadioButtonId() == R.id.radio_no
                ? ParamRegister.TRADER : ParamRegister.FORWARDER;

        viewModel.setRegisterInfo(firstName, lastName, password, confirmPassword, userType);
        return false;
    }

    private void showProgress() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showForm() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.VISIBLE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showError() {
        binding.progressBar.setVisibility(View.GONE);
        binding.mainFrame.setVisibility(View.INVISIBLE);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> {
                showProgress();
                viewModel.login();
            });
        }
    }

    public void confirmForm() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentRegister.OnFragmentInteractionListener) {
            mListener = (FragmentRegister.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onContinueToLogin();
        void onContinueToApp();
    }
}
