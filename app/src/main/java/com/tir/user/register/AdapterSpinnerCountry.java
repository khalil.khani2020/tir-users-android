package com.tir.user.register;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tir.user.R;
import com.tir.user.repository.model.room.entity.Country;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by khani on 21/02/2018.
 */

class AdapterSpinnerCountry extends ArrayAdapter<Country> {

    private final List<Country> countries;
    private final LayoutInflater inflater;

    public AdapterSpinnerCountry(@NonNull Context context, List<Country> countries) {
        super(context, R.layout.spinner_country_codes);
        this.countries = countries;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rawView = convertView;
        if (rawView == null) {
            rawView = inflater.inflate(R.layout.spinner_country, null);
        }
        TextView txtCode = rawView.findViewById(R.id.txt_item);
        txtCode.setText(countries.get(position).countryCode);
        return rawView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rawView = convertView;
        if (rawView == null) {
            rawView = inflater.inflate(R.layout.spinner_country_codes, null);
        }
        TextView txtCode = rawView.findViewById(R.id.txt_country_code);
        txtCode.setText(countries.get(position).countryCode);

        TextView txtTitle = rawView.findViewById(R.id.txt_country_title);
        txtTitle.setText(countries.get(position).name);

        return rawView;
    }

    @Override
    public int getCount() {
        return countries.size();
    }


}
