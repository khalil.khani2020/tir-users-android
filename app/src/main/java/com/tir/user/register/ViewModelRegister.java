package com.tir.user.register;

import android.app.Application;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.app.Const;
import com.tir.user.app.StaticVariables;
import com.tir.user.base.BaseViewModel;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryRegister;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamLogin;
import com.tir.user.repository.model.api.parameter.ParamRegister;
import com.tir.user.repository.model.api.parameter.ParamResetPassword;
import com.tir.user.repository.model.api.parameter.ParamSendSms;
import com.tir.user.repository.model.api.parameter.ParamVerification;
import com.tir.user.repository.model.api.response.ResponseBasicInfo;
import com.tir.user.repository.model.api.response.ResponseVerifyPhone;
import com.tir.user.repository.model.api.response.SendSms;
import com.tir.user.repository.model.room.entity.Country;
import com.tir.user.repository.model.room.entity.Session;

import java.util.List;

import javax.inject.Inject;


public class ViewModelRegister extends BaseViewModel {
    @Inject
    RepositoryRegister repository;
    private List<Country> countries;
    private final ParamSendSms paramSendSms;
    private final ParamLogin paramLogin;
    private final ParamRegister paramRegister;
    private final ParamResetPassword paramResetPassword;
    private ParamVerification paramVerification;
    public ResponseVerifyPhone verification;
    public boolean isForgotPassword;

    public ViewModelRegister(Application application) {
        super(application);
        getViewModelComponent().inject(this);
        paramLogin = new ParamLogin();
        paramSendSms = new ParamSendSms();
        paramResetPassword = new ParamResetPassword();
        paramRegister = new ParamRegister();
    }

    public MediatorLiveData<NetworkResponse<Session>> login() {
        return repository.login(paramLogin);
    }

    public MediatorLiveData<NetworkResponse<SendSms>> sendSms() {
        if (isForgotPassword)
            paramSendSms.type = Const.SmsValidationTypes.ResetPassword;
        else
            paramSendSms.type = Const.SmsValidationTypes.Register;
        return repository.sendSms(paramSendSms);
    }

    public MediatorLiveData<NetworkResponse<SendSms>> resendSMS() {
        return repository.resendSMS(paramSendSms);
    }

    public MediatorLiveData<NetworkResponse<ResponseVerifyPhone>> verifyPhoneNumber() {
        if (isForgotPassword)
            paramVerification.type = Const.SmsValidationTypes.ResetPassword;
        else
            paramVerification.type = Const.SmsValidationTypes.Register;
        return repository.verifyPhoneNumber(paramVerification);
    }

    public MediatorLiveData<NetworkResponse<Object>> resetPassword() {
        if (isForgotPassword)
            paramVerification.type = Const.SmsValidationTypes.ResetPassword;
        else
            paramVerification.type = Const.SmsValidationTypes.Register;
        return repository.resetPassword(paramResetPassword);
    }

    public MediatorLiveData<NetworkResponse<Session>> register() {
        return repository.register(paramRegister);
    }

    public MediatorLiveData<NetworkResponse<ResponseBasicInfo>> getBasicInfo() {
        return repository.getBasicInfo();
    }

    public List<Country> getCountries() {
        if (countries == null) {
            countries = repository.getCountries();
        }
        return countries;
    }

    public void setSendSmsInfo(String countryCode, String mobilePhone) {
        this.paramSendSms.countryCode = countryCode;
        this.paramSendSms.mobile = mobilePhone;
    }

    public void setLoginInfo(String countryCode, String mobilePhone, String password) {
        this.paramLogin.userName = countryCode + mobilePhone;
        this.paramLogin.password = password;

    }

    public void setPasswordInfo(String password, String confirmPassword) {
        paramResetPassword.identityToken = verification.identityToken;
        paramResetPassword.verificationToken = verification.verificationToken;
        paramResetPassword.password = password;
        paramResetPassword.confirmPassword = confirmPassword;

    }

    public void setRegisterInfo(String firstName, String lastName,
                                String password, String confirmPassword, String userType) {
        paramRegister.countryCode = verification.countryCode;
        paramRegister.mobile = verification.mobile;
        paramRegister.mobileVerificationToken = verification.verificationToken;
        paramRegister.firstName = firstName;
        paramRegister.lastName = lastName;
        paramRegister.password = password;
        paramRegister.confirmPassword = confirmPassword;
        paramRegister.userType = userType;
    }

    public ParamVerification getParamVerification() {
        return paramVerification;
    }

    public void setParamVerification(String vCode) {
        if (paramVerification == null) {
            paramVerification = new ParamVerification();
            paramVerification.mobile = paramSendSms.mobile;
            paramVerification.countryCode = paramSendSms.countryCode;
        }
        paramVerification.verifyCode = vCode;
    }

    public void setSession(Session session) {
        StaticVariables.SESSION = session;
        repository.saveSession(session);
        repository.saveLoginStatus(true);
    }

    public void setVerification(ResponseVerifyPhone verification) {
        this.verification = verification;
    }

    @Override
    public RepositoryBasic getBaseRepository() {
        return repository;
    }
}
