package com.tir.user.splash;

import android.Manifest;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.FrameLayout;

import com.tir.user.R;
import com.tir.user.app.MyApp;
import com.tir.user.app.StaticVariables;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivitySplashBinding;
import com.tir.user.home.ActivityHome;
import com.tir.user.profile.ActivityProfile;
import com.tir.user.register.ActivityRegister;
import com.tir.user.repository.model.AppUpdateLinks;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.Status;
import com.tir.user.repository.model.api.ApiResponse;

import static com.tir.user.utils.Utils.isNullOrEmpty;

public class ActivitySplash extends BaseActivity {

    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final int STORAGE_PERMISSION_REQUEST_CODE = 102;

    private ActivitySplashBinding binding;
    private ViewModelSplash viewModel;
    private boolean isRequestDone;
    private boolean isRequestFailed;
    private boolean isSplashFinished;
    private AppUpdateLinks appUpdateLinks = new AppUpdateLinks();
    private Order unRatedOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_splash, contentFrameLayout, true);
        setSupportActionBar(binding.toolbar);
        ///////////////////////////////////////////////////////////////////////////////////////////
        getSupportActionBar().hide();
        viewModel = ViewModelProviders.of(this).get(ViewModelSplash.class);

        splashScreen();
        getBasicInfo();
        createShortcut();
    }


    private void getBasicInfo() {
        viewModel.getBasicInfo().observe(this, networkResponse -> {
            if (networkResponse.getStatus() == Status.SUCCESS) {
                if (networkResponse.getApiResponse().status.code == ApiResponse.COMMON_SUCCESSFUL) {
                    isRequestDone = true;

                    if (networkResponse.getApiResponse().data.userInfo != null)
                        if (networkResponse.getApiResponse().data.userInfo.unRatedOrders != null)
                            unRatedOrder = networkResponse.getApiResponse().data.userInfo.unRatedOrders.lastOne;

                    appUpdateLinks.isUpdateAvailable = MyApp.getInstance().getVersionCode()
                            < networkResponse.getApiResponse().data.dbChangeInfo.appVersion;
                    appUpdateLinks.isPlayServicesUpdateNeeded = isPlayServicesUpdateNeeded();
                    appUpdateLinks.isForceUpdate = networkResponse.getApiResponse().data.dbChangeInfo.forceUpdate;
                    appUpdateLinks.playLink = networkResponse.getApiResponse().data.dbChangeInfo.googlePlayUrl;
                    appUpdateLinks.bazarLink = networkResponse.getApiResponse().data.dbChangeInfo.cafeBazarUrl;
                    appUpdateLinks.directLink = networkResponse.getApiResponse().data.dbChangeInfo.directUrl;
                    appUpdateLinks.playServicesUpdateUrl = networkResponse.getApiResponse().data.dbChangeInfo.playServicesUpdateUrl;
                } else {
                    isRequestFailed = true;
                }
                nextStep();
            } else if (networkResponse.getStatus() == Status.FAILURE) {
                isRequestFailed = true;
                nextStep();
            }
        });
    }

    private void splashScreen() {
        splashAnimation();
        getHandler().postDelayed(
                () -> {
                    isSplashFinished = true;
                    nextStep();
                },
                5000);
    }

    private void splashAnimation() {
        binding.content.imgLogo.setAlpha(0f);
        binding.content.imgLogo.animate().alpha(1f).setDuration(1500).start();
        getHandler().postDelayed(() -> {
            binding.content.typeWriter.setCharacterDelay(120);
            binding.content.typeWriter.animateText("www.TIR724.com");
        }, 1500);
        getHandler().postDelayed(() -> binding.content.progressBar.show(), 1600);
    }

    private void nextStep() {
        if (isSplashFinished) {
            if (isRequestDone) {
                goToNextScreen();
            } else if (isRequestFailed) {
                errorInConnection();
            }
        }
    }

    private void goToNextScreen() {
        Intent intent;
        if (isNullOrEmpty(StaticVariables.SESSION.accessToken)) {
            intent = new Intent(this, ActivityRegister.class);
        } else if (!StaticVariables.SESSION.isProfileCompleted) {
            intent = new Intent(this, ActivityProfile.class);
        } else {
            intent = new Intent(this, ActivityHome.class);
            if (appUpdateLinks.isUpdateAvailable || appUpdateLinks.isPlayServicesUpdateNeeded)
                intent.putExtra(ActivityHome.UPDATE_LINKS, appUpdateLinks);
            if (unRatedOrder != null)
                intent.putExtra(ActivityHome.UN_RATED_ORDER, unRatedOrder);
        }
        startActivity(intent);
        finish();
    }

    private void errorInConnection() {
        binding.content.errorLayout.root.setVisibility(View.VISIBLE);
        if (!binding.content.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.content.errorLayout.txtRetry.setOnClickListener(view -> {
                binding.content.errorLayout.root.setVisibility(View.GONE);
                viewModel.getBasicInfo();
            });
        }
    }


//    private void registerReceiver(){
//        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(MESSAGE_PROGRESS);
//        bManager.registerReceiver(broadcastReceiver, intentFilter);
//    }

//    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            if(intent.getAction().equals(MESSAGE_PROGRESS)){
//
////                Download download = intent.getParcelableExtra("download");
////                mProgressBar.setProgress(download.getProgress());
////                if(download.getProgress() == 100){
////
////                    mProgressText.setText("File Download Complete");
////
////                } else {
////
////                    mProgressText.setText(String.format("Downloaded (%d/%d) MB",download.getCurrentFileSize(),download.getTotalFileSize()));
////
////                }
//            }
//        }
//    };

    private boolean checkStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }


    private void createShortcut() {
        // Get preference value to check the app run first time.
        SharedPreferences appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isFirstRun = appPreferences.getBoolean("isFirstRun", false);
        if (!isFirstRun) {
            // Create an explicit intent. it will be used to call Our application by click on the short cut
            Intent shortcutIntent = new Intent(getApplicationContext(), ActivitySplash.class);
            shortcutIntent.setAction(Intent.ACTION_MAIN);
            // Create an implicit intent and assign Shortcut Application Name, Icon
            Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
            intent.putExtra(
                    Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                    Intent.ShortcutIconResource.fromContext(
                            getApplicationContext(), R.mipmap.ic_launcher));
            intent.putExtra("duplicate", false); //Avoid duplicate
            intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(intent);
            // Set preference  as true
            SharedPreferences.Editor editor = appPreferences.edit();
            editor.putBoolean("isFirstRun", true);
            editor.apply();
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        return this.viewModel;
    }
}
