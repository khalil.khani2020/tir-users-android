package com.tir.user.splash;

import android.app.Application;

import com.tir.user.base.BaseViewModel;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseBasicInfo;

import javax.inject.Inject;

import androidx.lifecycle.MediatorLiveData;


public class ViewModelSplash extends BaseViewModel {

    @Inject
    RepositoryBasic repositoryBasic;
    public ViewModelSplash(Application application) {
        super(application);
        getViewModelComponent().inject(this);
    }

    @Override
    public RepositoryBasic getBaseRepository() {
        return repositoryBasic;
    }

    public MediatorLiveData<NetworkResponse<ResponseBasicInfo>> getBasicInfo() {
        return repositoryBasic.getBasicInfo();
    }

}
