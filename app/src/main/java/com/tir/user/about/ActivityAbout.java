package com.tir.user.about;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.databinding.DataBindingUtil;

import com.tir.user.R;
import com.tir.user.app.Const;
import com.tir.user.app.MyApp;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityAboutBinding;
import com.tir.user.webview.ActivityWebView;

public class ActivityAbout extends BaseActivity {

    private ActivityAboutBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_about, contentFrame, true);
        setSupportActionBar(binding.toolbar);
        initToolbar(binding.toolbar, binding.toolbarShadow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ////////////////////////////////////////////////////////////////////////////////////////////
        binding.content.txtVersion.setText(MyApp.getInstance().getVersionName());
        binding.content.txtLink.setText(Const.Endpoints.SITE_URL);
        binding.content.txtLink.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ActivityWebView.class);
            intent.putExtra(ActivityWebView.URL, "https://" + Const.Endpoints.SITE_URL);
            startActivity(intent);
        });
    }

    @Override
    protected BaseViewModel getViewModel() {
        return null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
