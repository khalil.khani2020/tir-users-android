package com.tir.user.di.viewmodel;


import com.tir.user.base.OrderViewModel;
import com.tir.user.home.ViewModelHome;
import com.tir.user.neworder.ViewModelOrder;
import com.tir.user.notifications.ViewModelNotification;
import com.tir.user.points.ViewModelPoints;
import com.tir.user.profile.ViewModelProfile;
import com.tir.user.rating.ViewModelRating;
import com.tir.user.register.ViewModelRegister;
import com.tir.user.splash.ViewModelSplash;

import dagger.Subcomponent;

/**
 * Created by khani on 01/10/2017.
 */
@Subcomponent(modules = {ViewModelModule.class})
public interface ViewModelComponent {

    void inject(ViewModelSplash viewModel);

    void inject(ViewModelRegister viewModel);

    void inject(ViewModelProfile viewModel);

    void inject(ViewModelHome viewModel);

    void inject(OrderViewModel viewModel);

    void inject(ViewModelOrder viewModel);

    void inject(ViewModelRating viewModel);

    void inject(ViewModelNotification viewModel);

    void inject(ViewModelPoints viewModel);
}
