package com.tir.user.di.application;

import android.app.Application;

import com.tir.user.app.AppExecutors;
import com.tir.user.repository.SharedPrefsHelper;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by khani on 26/09/2017.
 */
@Module
public class AppModule {
    private final Application mApplication;

    public AppModule(Application application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    AppExecutors provideAppExecutors() {
        return new AppExecutors(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(3),
                new AppExecutors.MainThreadExecutor());
    }

    @Provides
    @Singleton
    SharedPrefsHelper provideSharedPrefsHelper(){
        return new SharedPrefsHelper(mApplication);
    }

}
