package com.tir.user.di.application;

import android.app.Application;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.tir.user.app.Const;
import com.tir.user.app.StaticVariables;
import com.tir.user.repository.SharedPrefsHelper;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.remot.retrofit.ApiClient;
import com.tir.user.repository.remot.retrofit.ApiService;
import com.tir.user.repository.remot.retrofit.ApiServiceHolder;
import com.tir.user.utils.Utils;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tir.user.utils.Utils.isNullOrEmpty;

/**
 * Created by khani on 01/10/2017.
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    ApiClient provideApiClient(Application application, ApiService apiService,
                               AppDatabase appDatabase, SharedPrefsHelper sharedPrefsHelper) {
        return new ApiClient(application, apiService, appDatabase, sharedPrefsHelper);
    }

    @Provides
    @Singleton
    ApiService provideApiService(Retrofit retrofit, ApiServiceHolder serviceHolder) {
        ApiService apiService = retrofit.create(ApiService.class);
        serviceHolder.setApiService(apiService);
        return apiService;
    }

    @Provides
    @Singleton
    ApiServiceHolder provideApiServiceHolder() {
        return new ApiServiceHolder();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    @Singleton
    Interceptor provideInterceptor() {
        return chain -> {
            Request request = chain.request();
            Request newRequest;
            newRequest = request.newBuilder()
                    .removeHeader("User-Agent")
                    .removeHeader("Device")
                    .removeHeader("TimeStamp")
                    .removeHeader("Accept-Language")
                    .removeHeader("SessionId")
                    .removeHeader("Authorization")
                    .addHeader("User-Agent", "AndroidTrader")
                    .addHeader("Device", Utils.getDeviceInfo())
                    .addHeader("TimeStamp", String.valueOf(System.currentTimeMillis()))
                    .addHeader("Accept-Language", StaticVariables.CULTURE)
                    .addHeader("SessionId", isNullOrEmpty(StaticVariables.SESSION.sessionId) ? "" : StaticVariables.SESSION.sessionId)
                    .addHeader("Authorization", isNullOrEmpty(StaticVariables.SESSION.accessToken) ? "" : "Bearer " + StaticVariables.SESSION.accessToken)
                    .build();
            return chain.proceed(newRequest);
        };
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor,
                                     Interceptor interceptor, ApiServiceHolder apiServiceHolder, AppDatabase database) {
        return new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addNetworkInterceptor(interceptor)
                .addNetworkInterceptor(loggingInterceptor)
//                .authenticator(new AccessTokenAuthenticator(apiServiceHolder, database))
                .build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setLenient()
                .addSerializationExclusionStrategy(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getAnnotation(Expose.class) != null && !f.getAnnotation(Expose.class).serialize();
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .addDeserializationExclusionStrategy(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getAnnotation(Expose.class) != null && !f.getAnnotation(Expose.class).deserialize();
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(Const.Endpoints.API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }
}
