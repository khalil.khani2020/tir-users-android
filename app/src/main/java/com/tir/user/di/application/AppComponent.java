package com.tir.user.di.application;


import com.tir.user.app.MyApp;
import com.tir.user.di.service.ServiceComponent;
import com.tir.user.di.service.ServiceModule;
import com.tir.user.di.ui.UiComponent;
import com.tir.user.di.ui.UiModule;
import com.tir.user.di.viewmodel.ViewModelComponent;
import com.tir.user.di.viewmodel.ViewModelModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by khani on 26/09/2017.
 */
@Singleton
@Component(modules = {AppModule.class, DatabaseModule.class, NetworkModule.class})
public interface AppComponent {

    ViewModelComponent newViewModelComponent(ViewModelModule viewModelModule);

    ServiceComponent newServiceComponent(ServiceModule serviceModule);

    UiComponent newUiComponent(UiModule uiModule);

    void inject(MyApp app);
//
//    void inject(SharedViewModel baseViewModel);
//
//    void inject(StudentsRepository studentsRepository);
}
