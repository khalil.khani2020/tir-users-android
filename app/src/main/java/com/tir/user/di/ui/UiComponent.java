package com.tir.user.di.ui;

import com.tir.user.home.ActivityHome;
import com.tir.user.profile.ActivityProfile;
import com.tir.user.splash.ActivitySplash;

import dagger.Subcomponent;

/**
 * Created by khani on 11/11/2017.
 */

@Subcomponent(modules = UiModule.class)
public interface UiComponent {
    void inject(ActivityHome activity);
    void inject(ActivityProfile activity);
    void inject(ActivitySplash activity);

//    void inject(FragmentPersonalInfo fragment);
}
