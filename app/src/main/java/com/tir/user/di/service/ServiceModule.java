package com.tir.user.di.service;


import com.tir.user.app.AppExecutors;
import com.tir.user.repository.RepositoryService;
import com.tir.user.repository.SharedPrefsHelper;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.remot.retrofit.ApiClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by khani on 01/10/2017.
 */
@Module
public class ServiceModule {

    @Provides
    public RepositoryService provideRepositoryService(ApiClient apiClient,
                                                      AppDatabase appDatabase,
                                                      AppExecutors appExecutors,
                                                      SharedPrefsHelper sharedPrefsHelper) {
        return new RepositoryService(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

}
