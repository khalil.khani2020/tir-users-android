package com.tir.user.di.service;


import com.tir.user.services.ServiceFirebaseID;

import dagger.Subcomponent;


@Subcomponent(modules = {ServiceModule.class})
public interface ServiceComponent {
    void inject(ServiceFirebaseID service);
}
