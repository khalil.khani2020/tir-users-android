package com.tir.user.di.ui;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by khani on 11/11/2017.
 */
@Module
public class UiModule {

    private final Activity activity;

    public UiModule(Activity activity) {
        this.activity = activity;
    }

    // activity scope context:
    @Provides
    Activity provideActivity() {
        return activity;
    }
}
