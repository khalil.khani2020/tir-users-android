package com.tir.user.di.viewmodel;


import com.tir.user.app.AppExecutors;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryNotification;
import com.tir.user.repository.RepositoryOrder;
import com.tir.user.repository.RepositoryPoints;
import com.tir.user.repository.RepositoryProfile;
import com.tir.user.repository.RepositoryRegister;
import com.tir.user.repository.SharedPrefsHelper;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.remot.retrofit.ApiClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by khani on 01/10/2017.
 */
@Module
public class ViewModelModule {

    @Provides
    public RepositoryBasic provideRepositoryBasic(ApiClient apiClient,
                                                  AppDatabase appDatabase,
                                                  AppExecutors appExecutors,
                                                  SharedPrefsHelper sharedPrefsHelper) {
        return new RepositoryBasic(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    @Provides
    public RepositoryRegister provideRepositoryRegister(ApiClient apiClient,
                                                        AppDatabase appDatabase,
                                                        AppExecutors appExecutors,
                                                        SharedPrefsHelper sharedPrefsHelper) {
        return new RepositoryRegister(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    @Provides
    public RepositoryProfile provideRepositoryProfile(ApiClient apiClient,
                                                      AppDatabase appDatabase,
                                                      AppExecutors appExecutors,
                                                      SharedPrefsHelper sharedPrefsHelper) {
        return new RepositoryProfile(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    @Provides
    public RepositoryOrder provideRepositoryOrder(ApiClient apiClient,
                                                  AppDatabase appDatabase,
                                                  AppExecutors appExecutors,
                                                  SharedPrefsHelper sharedPrefsHelper) {
        return new RepositoryOrder(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    @Provides
    public RepositoryNotification provideRepositoryNotification(ApiClient apiClient,
                                                                AppDatabase appDatabase,
                                                                AppExecutors appExecutors,
                                                                SharedPrefsHelper sharedPrefsHelper) {
        return new RepositoryNotification(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    @Provides
    public RepositoryPoints provideRepositoryPoints(ApiClient apiClient,
                                                    AppDatabase appDatabase,
                                                    AppExecutors appExecutors,
                                                    SharedPrefsHelper sharedPrefsHelper) {
        return new RepositoryPoints(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }
}
