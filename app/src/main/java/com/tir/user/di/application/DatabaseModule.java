package com.tir.user.di.application;

import android.app.Application;
import androidx.room.Room;

import com.tir.user.app.Const;
import com.tir.user.repository.model.room.database.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Created by khani on 04/11/2017.
 */
@Module
public class DatabaseModule {

    @Provides
    @Singleton
    public AppDatabase provideAppDatabase(Application application) {
        return Room.databaseBuilder(application, AppDatabase.class, Const.DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }
}
