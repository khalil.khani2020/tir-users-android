package com.tir.user.repository.model;

import com.google.gson.annotations.Expose;

/**
 * Created by khani on 21/02/2018.
 */

public class CarInfo {

    @Expose(serialize = false)
    public String carImageUrl = "";
    public String thirdPartyInsuranceExpirationDate = "";
    public String thirdPartyInsuranceCompany = "";
    public String bodyInsuranceExpirationDate = "";
    public String bodyInsuranceCompany = "";
    public String capotagePlate = "";
    public String plate = "";
    public int carTypeId;

    @Override
    public String toString() {
        return "CarInfo{" +
                "carImageUrl='" + carImageUrl + '\'' +
                ", thirdPartyInsuranceExpirationDate=" + thirdPartyInsuranceExpirationDate +
                ", thirdPartyInsuranceCompany='" + thirdPartyInsuranceCompany + '\'' +
                ", bodyInsuranceExpirationDate=" + bodyInsuranceExpirationDate +
                ", bodyInsuranceCompany='" + bodyInsuranceCompany + '\'' +
                ", capotagePlate='" + capotagePlate + '\'' +
                ", plate='" + plate + '\'' +
                ", carType='" + carTypeId + '\'' +
                '}';
    }
}
