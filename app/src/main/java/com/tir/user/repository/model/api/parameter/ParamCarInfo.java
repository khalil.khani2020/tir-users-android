package com.tir.user.repository.model.api.parameter;

public class ParamCarInfo {
    public String thirdPartyInsuranceExpirationDate = "";
    public String thirdPartyInsuranceCompany = "";
    public String bodyInsuranceExpirationDate = "";
    public String bodyInsuranceCompany = "";
    public String capotagePlate = "";
    public String plate = "";
    public int carTypeId;
}
