package com.tir.user.repository.model.api.response;

/**
 * Created by khani on 15/02/2018.
 */

public class ResponseImageUpload {

    public String fileUrl;

    @Override
    public String toString() {
        return "{" + "fileUrl='" + fileUrl + '\'' + '}';
    }
}
