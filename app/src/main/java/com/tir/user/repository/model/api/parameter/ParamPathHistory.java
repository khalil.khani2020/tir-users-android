package com.tir.user.repository.model.api.parameter;

public class ParamPathHistory {
    public int sourceCityId;
    public int targetCityId;
    public int shipmentTypeId;
    public int carTypeId;
}
