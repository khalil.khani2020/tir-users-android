package com.tir.user.repository.model.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by khani on 03/11/2017.
 */

@Entity()
public class RatingFactor {
    @PrimaryKey
    public int id;
    public String name;
    public boolean isPositive;
    public boolean isSelected;
    public int oppositeId;

    @Override
    public String toString() {
        return "RatingFactor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isPositive=" + isPositive +
                ", isSelected=" + isSelected +
                ", oppositeId=" + oppositeId +
                '}';
    }
}
