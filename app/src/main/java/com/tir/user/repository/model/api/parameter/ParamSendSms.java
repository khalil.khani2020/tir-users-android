package com.tir.user.repository.model.api.parameter;

/**
 * Created by khani on 15/02/2018.
 */

public class ParamSendSms {
    public String countryCode;
    public String mobile;
    public int type;
}
