package com.tir.user.repository.model.api.parameter;

/**
 * Created by khani on 15/02/2018.
 */

public class ParamSpendPoints {
    public int rewardId;

    @Override
    public String toString() {
        return "ParamSpendPoints{" +
                "rewardId=" + rewardId +
                '}';
    }
}
