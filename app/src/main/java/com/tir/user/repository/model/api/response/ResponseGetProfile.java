package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.PersonalInfo;

/**
 * Created by khani on 15/02/2018.
 */

public class ResponseGetProfile {

    public PersonalInfo personalInfo;

    @Override
    public String toString() {
        return "{" + "personalInfo=" + personalInfo +
                '}';
    }
}
