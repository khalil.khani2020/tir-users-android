package com.tir.user.repository.model;

import com.tir.user.repository.model.room.entity.CarType;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.Currency;
import com.tir.user.repository.model.room.entity.ShipmentType;
import com.tir.user.repository.model.room.entity.WeightRange;
import com.tir.user.utils.Utils;

import java.io.Serializable;

/**
 * Created by khani on 03/11/2017.
 */

public class Order implements Serializable {

    public static final int STATE_NEW = 10;
    public static final int STATE_REJECTED = 30;
    public static final int STATE_CONFIRMED = 40;
    public static final int STATE_PENDING = 50;
    public static final int STATE_ALLOCATED = 60;
    public static final int STATE_RECEIVED = 70;
    public static final int STATE_DELIVERED = 80;
    public static final int STATE_COMPLETED = 90;
    public static final int STATE_CANCELED = 100;
    public static final int STATE_DELETED = 110;


    public int id;
    public City sourceCity;
    public City targetCity;
    public CarType carType;
    public ShipmentType shipmentType;
    public WeightRange weightRange;
    public Currency currency;
    public GeoLoc sourceLocation;
    public GeoLoc targetLocation;
    public String sourceAddress = "";
    public String targetAddress = "";
    public String description = "";
    public boolean allocatedToMe;
    public boolean allocationCanceled;
    public String loadingDate;
    public int orderState;
    public String orderStateName;
    public String trackingCode;
    public String lastModificationDate;
    public String receiveDateTime;
    public String deliverDateTime;
    public String createDate;
    public String rejectReason;
    public String deleteReason;
    public String cancelReason;
    public Double forwarderPrice;
    public Double adminPrice;
    public String loadImageUrl;
    public String deliverImageUrl;
    public boolean notRated;
    public Driver driver;
    public boolean seenByCustomer;

    public Order() {
    }

    public String getFormattedOrderPrice() {
        return Utils.isNullOrEmpty(adminPrice) ? "" : Utils.formatPrice(adminPrice.doubleValue());
    }

    public String getFormattedForwarderPrice() {
        if (forwarderPrice == null)
            return "";
        return Utils.formatPrice((long) forwarderPrice.doubleValue());
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", sourceCity=" + sourceCity +
                ", targetCity=" + targetCity +
                ", carType=" + carType +
                ", shipmentType=" + shipmentType +
                ", weightRange=" + weightRange +
                ", currency=" + currency +
                ", sourceLocation=" + sourceLocation +
                ", targetLocation=" + targetLocation +
                ", sourceAddress='" + sourceAddress + '\'' +
                ", targetAddress='" + targetAddress + '\'' +
                ", description='" + description + '\'' +
                ", allocatedToMe=" + allocatedToMe +
                ", allocationCanceled=" + allocationCanceled +
                ", loadingDate='" + loadingDate + '\'' +
                ", orderState='" + orderState + '\'' +
                ", trackingCode='" + trackingCode + '\'' +
                ", lastModificationDate='" + lastModificationDate + '\'' +
                ", receiveDateTime='" + receiveDateTime + '\'' +
                ", deliverDateTime='" + deliverDateTime + '\'' +
                ", createDate='" + createDate + '\'' +
                ", deleteReason='" + deleteReason + '\'' +
                ", forwarderPrice=" + forwarderPrice +
                ", adminPrice=" + adminPrice +
                ", loadImageUrl='" + loadImageUrl + '\'' +
                ", deliverImageUrl='" + deliverImageUrl + '\'' +
                ", notRated=" + notRated +
                '}';
    }
}
