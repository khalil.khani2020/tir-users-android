package com.tir.user.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.app.AppExecutors;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseNotifications;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.remot.retrofit.ApiClient;


public class RepositoryNotification extends RepositoryBasic {

    private final MediatorLiveData<NetworkResponse<ResponseNotifications>> notificationMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<ResponseNotifications>> notificationLive;


    public RepositoryNotification(ApiClient apiClient, AppDatabase appDatabase,
                                  AppExecutors appExecutors, SharedPrefsHelper sharedPrefsHelper) {
        super(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    public MediatorLiveData<NetworkResponse<ResponseNotifications>> getNotifications() {
        if (notificationLive != null) {
            notificationMediator.removeSource(notificationLive);
        }
        notificationLive = apiClient.getNotifications();
        notificationMediator.addSource(notificationLive, notificationMediator::setValue);
        return notificationMediator;
    }
}
