package com.tir.user.repository.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by khani on 25/09/2017.
 */

public class GeoLoc implements Serializable {

    public double latitude;
    public double longitude;

    public GeoLoc() {
    }

    public GeoLoc(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public GeoLoc(LatLng latLng) {
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
    }


    @Override
    public String toString() {
        return "{" + "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
