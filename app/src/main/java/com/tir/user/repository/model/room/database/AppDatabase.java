package com.tir.user.repository.model.room.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.tir.user.repository.model.room.dao.BasicInfoDAO;
import com.tir.user.repository.model.room.dao.ChangesDAO;
import com.tir.user.repository.model.room.dao.SessionDAO;
import com.tir.user.repository.model.room.entity.CarType;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.Country;
import com.tir.user.repository.model.room.entity.Currency;
import com.tir.user.repository.model.room.entity.DBChangeInfo;
import com.tir.user.repository.model.room.entity.OrderState;
import com.tir.user.repository.model.room.entity.PathCurrency;
import com.tir.user.repository.model.room.entity.RatingFactor;
import com.tir.user.repository.model.room.entity.Session;
import com.tir.user.repository.model.room.entity.ShipmentType;
import com.tir.user.repository.model.room.entity.WeightRange;

@Database(entities = {DBChangeInfo.class, Session.class, Country.class,
        City.class, Currency.class, CarType.class, OrderState.class,
        ShipmentType.class, WeightRange.class, PathCurrency.class, RatingFactor.class}, version = 7)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ChangesDAO changesDAO();

    public abstract BasicInfoDAO basicInfoDAO();

    public abstract SessionDAO sessionDAO();
}
