package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.room.entity.CarType;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.Country;
import com.tir.user.repository.model.room.entity.Currency;
import com.tir.user.repository.model.room.entity.DBChangeInfo;
import com.tir.user.repository.model.room.entity.OrderState;
import com.tir.user.repository.model.room.entity.PathCurrency;
import com.tir.user.repository.model.room.entity.Session;
import com.tir.user.repository.model.room.entity.ShipmentType;
import com.tir.user.repository.model.room.entity.WeightRange;

import java.util.ArrayList;


public class ResponseBasicInfo{

    public DBChangeInfo dbChangeInfo;
    public ArrayList<Country> countries;
    public ArrayList<City> cities;
    public ArrayList<CarType> carTypes;
    public ArrayList<OrderState> orderStates;
    public ArrayList<ShipmentType> shipmentTypes;
    public ArrayList<Currency> currencies;
    public ArrayList<WeightRange> weightRanges;
    public ResponseRatingFactors ratingFactors;
    public ArrayList<PathCurrency> pathCurrencies;
    public Session userInfo;

    @Override
    public String toString() {
        return "ResponseBasicInfo{" +
                "dbChangeInfo=" + dbChangeInfo +
                ", countries=" + countries +
                ", cities=" + cities +
                ", carTypes=" + carTypes +
                ", orderStates=" + orderStates +
                ", shipmentTypes=" + shipmentTypes +
                ", currencies=" + currencies +
                ", weightRanges=" + weightRanges +
                ", ratingFactors=" + ratingFactors +
                ", pathCurrencies=" + pathCurrencies +
                ", userInfo=" + userInfo +
                '}';
    }
}
