package com.tir.user.repository.model;

import java.io.Serializable;

public class Driver implements Serializable {
    public String fullName;
    public String cellPhone;
    public String profileImageUrl;
    public Car car;
}
