package com.tir.user.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.app.AppExecutors;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamLogin;
import com.tir.user.repository.model.api.parameter.ParamRegister;
import com.tir.user.repository.model.api.parameter.ParamResetPassword;
import com.tir.user.repository.model.api.parameter.ParamSendSms;
import com.tir.user.repository.model.api.parameter.ParamVerification;
import com.tir.user.repository.model.api.response.ResponseVerifyPhone;
import com.tir.user.repository.model.api.response.SendSms;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.model.room.entity.Session;
import com.tir.user.repository.remot.retrofit.ApiClient;


public class RepositoryRegister extends RepositoryBasic{

    private final MediatorLiveData<NetworkResponse<Session>> loginMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<SendSms>> sendSmsMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<SendSms>> resendMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<ResponseVerifyPhone>> verifyMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Object>> resetPasswordMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Session>> registerMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<Session>> loginLive;
    private LiveData<NetworkResponse<SendSms>> sendSmsLive;
    private LiveData<NetworkResponse<SendSms>> resendLive;
    private LiveData<NetworkResponse<ResponseVerifyPhone>> verifyLive;
    private LiveData<NetworkResponse<Object>> resetPasswordLive;
    private LiveData<NetworkResponse<Session>> registerLive;

    public RepositoryRegister(ApiClient apiClient, AppDatabase appDatabase,
                             AppExecutors appExecutors, SharedPrefsHelper sharedPrefsHelper) {
        super(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    public MediatorLiveData<NetworkResponse<Session>> login(ParamLogin paramLogin) {
        if (loginLive != null) {
            loginMediator.removeSource(loginLive);
        }
        loginLive = apiClient.login(paramLogin);
        loginMediator.addSource(loginLive, loginMediator::setValue);
        return loginMediator;
    }

    public MediatorLiveData<NetworkResponse<SendSms>> sendSms(ParamSendSms paramSendSms) {
        if (sendSmsLive != null) {
            sendSmsMediator.removeSource(sendSmsLive);
        }
        sendSmsLive = apiClient.sendSms(paramSendSms);
        sendSmsMediator.addSource(sendSmsLive, sendSmsMediator::setValue);
        return sendSmsMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponseVerifyPhone>> verifyPhoneNumber(ParamVerification verificationCode) {
        if (verifyLive != null) {
            verifyMediator.removeSource(verifyLive);
        }
        verifyLive = apiClient.verifyPhoneNumber(verificationCode);
        verifyMediator.addSource(verifyLive, verifyMediator::setValue);
        return verifyMediator;
    }

    public MediatorLiveData<NetworkResponse<Object>> resetPassword(ParamResetPassword resetPassword) {
        if (resetPasswordLive != null) {
            resetPasswordMediator.removeSource(resetPasswordLive);
        }
        resetPasswordLive = apiClient.resetPassword(resetPassword);
        resetPasswordMediator.addSource(resetPasswordLive, resetPasswordMediator::setValue);
        return resetPasswordMediator;
    }

    public MediatorLiveData<NetworkResponse<Session>> register(ParamRegister paramRegister) {
        if (registerLive != null) {
            registerMediator.removeSource(registerLive);
        }
        registerLive = apiClient.register(paramRegister);
        registerMediator.addSource(registerLive, registerMediator::setValue);
        return registerMediator;
    }

    public MediatorLiveData<NetworkResponse<SendSms>> resendSMS(ParamSendSms paramSendSms) {
        if (resendLive != null) {
            resendMediator.removeSource(resendLive);
        }
        resendLive = apiClient.resendSMS(paramSendSms);
        resendMediator.addSource(resendLive, resendMediator::setValue);
        return resendMediator;
    }

}
