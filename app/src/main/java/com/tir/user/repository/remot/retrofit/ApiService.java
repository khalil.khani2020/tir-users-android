package com.tir.user.repository.remot.retrofit;


import com.tir.user.repository.model.CarInfo;
import com.tir.user.repository.model.GeoLoc;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.PersonalInfo;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.parameter.ParamFcmToken;
import com.tir.user.repository.model.api.parameter.ParamLogin;
import com.tir.user.repository.model.api.parameter.ParamNewOrder;
import com.tir.user.repository.model.api.parameter.ParamOrders;
import com.tir.user.repository.model.api.parameter.ParamPathHistory;
import com.tir.user.repository.model.api.parameter.ParamPoints;
import com.tir.user.repository.model.api.parameter.ParamPrice;
import com.tir.user.repository.model.api.parameter.ParamRateDriver;
import com.tir.user.repository.model.api.parameter.ParamRefreshToken;
import com.tir.user.repository.model.api.parameter.ParamRegister;
import com.tir.user.repository.model.api.parameter.ParamResetPassword;
import com.tir.user.repository.model.api.parameter.ParamRewards;
import com.tir.user.repository.model.api.parameter.ParamSendSms;
import com.tir.user.repository.model.api.parameter.ParamSpendPoints;
import com.tir.user.repository.model.api.parameter.ParamVerification;
import com.tir.user.repository.model.api.response.ResponseBasicInfo;
import com.tir.user.repository.model.api.response.ResponseImageUpload;
import com.tir.user.repository.model.api.response.ResponseNotifications;
import com.tir.user.repository.model.api.response.ResponseNull;
import com.tir.user.repository.model.api.response.ResponseOrders;
import com.tir.user.repository.model.api.response.ResponsePathHistory;
import com.tir.user.repository.model.api.response.ResponsePoints;
import com.tir.user.repository.model.api.response.ResponseRewards;
import com.tir.user.repository.model.api.response.ResponseUserRewards;
import com.tir.user.repository.model.api.response.ResponseVerifyPhone;
import com.tir.user.repository.model.api.response.SendSms;
import com.tir.user.repository.model.room.entity.DBChangeInfo;
import com.tir.user.repository.model.room.entity.Session;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

import static com.tir.user.app.Const.Endpoints.URL_AcceptOrder;
import static com.tir.user.app.Const.Endpoints.URL_DeleteOffer;
import static com.tir.user.app.Const.Endpoints.URL_DeleteOrder;
import static com.tir.user.app.Const.Endpoints.URL_EditOrder;
import static com.tir.user.app.Const.Endpoints.URL_GetBasicInfo;
import static com.tir.user.app.Const.Endpoints.URL_GetNotifications;
import static com.tir.user.app.Const.Endpoints.URL_GetNotifiedShipments;
import static com.tir.user.app.Const.Endpoints.URL_GetOrders;
import static com.tir.user.app.Const.Endpoints.URL_GetPathHistory;
import static com.tir.user.app.Const.Endpoints.URL_GetPointActions;
import static com.tir.user.app.Const.Endpoints.URL_GetProfile;
import static com.tir.user.app.Const.Endpoints.URL_GetRewards;
import static com.tir.user.app.Const.Endpoints.URL_GetUserRewards;
import static com.tir.user.app.Const.Endpoints.URL_Logout;
import static com.tir.user.app.Const.Endpoints.URL_PriceOffer;
import static com.tir.user.app.Const.Endpoints.URL_RateDriver;
import static com.tir.user.app.Const.Endpoints.URL_RecreateOrder;
import static com.tir.user.app.Const.Endpoints.URL_RefreshToken;
import static com.tir.user.app.Const.Endpoints.URL_Register;
import static com.tir.user.app.Const.Endpoints.URL_ResetPassword;
import static com.tir.user.app.Const.Endpoints.URL_SendSms;
import static com.tir.user.app.Const.Endpoints.URL_SetOrderStatus;
import static com.tir.user.app.Const.Endpoints.URL_SpendPoints;
import static com.tir.user.app.Const.Endpoints.URL_SubmitOrder;
import static com.tir.user.app.Const.Endpoints.URL_UpdateCar;
import static com.tir.user.app.Const.Endpoints.URL_UpdateFcmToken;
import static com.tir.user.app.Const.Endpoints.URL_UpdateLocation;
import static com.tir.user.app.Const.Endpoints.URL_UpdateProfile;
import static com.tir.user.app.Const.Endpoints.URL_VerifySms;
import static com.tir.user.app.Const.Endpoints.URL_login;
import static com.tir.user.app.Const.Endpoints.URL_uploadImage;


/**
 * Created by khani on 25/09/2017.
 */

public interface ApiService {

    @POST(URL_login)
    Call<ApiResponse<Session>> login(@Body ParamLogin paramLogin);

    @POST(URL_SendSms)
    Call<ApiResponse<SendSms>> sendSms(@Body ParamSendSms paramSendSms);

    @POST(URL_VerifySms)
    Call<ApiResponse<ResponseVerifyPhone>> verifyPhoneNumber(@Body ParamVerification verificationCode);

    @POST(URL_RefreshToken)
    Call<ApiResponse<Session>> refreshToken(@Body ParamRefreshToken refreshToken);

    @POST(URL_ResetPassword)
    Call<ApiResponse<Object>> resetPassword(@Body ParamResetPassword paramResetPassword);

    @POST(URL_Register)
    Call<ApiResponse<Session>> register(@Body ParamRegister paramRegister);

    @POST(URL_GetBasicInfo)
    Call<ApiResponse<ResponseBasicInfo>> getBasicInfo(@Body DBChangeInfo dbChangeInfo);

    @POST(URL_Logout)
    Call<ApiResponse<Object>> logout();

    @POST(URL_UpdateFcmToken)
    Call<ApiResponse<Object>> updateFcmToken(@Body ParamFcmToken paramFcmToken);

    @GET(URL_GetProfile)
    Call<ApiResponse<PersonalInfo>> getProfile();

    @GET(URL_GetNotifications)
    Call<ApiResponse<ResponseNotifications>> getNotifications();

    @PUT(URL_UpdateProfile)
    Call<ApiResponse<Object>> updateProfile(@Body PersonalInfo personalInfo);

    @POST(URL_UpdateCar)
    Call<ApiResponse<Object>> updateCarInfo(@Body CarInfo carInfo);

    @Multipart
    @POST(URL_uploadImage)
    Call<ApiResponse<ResponseImageUpload>> uploadImage(@Header("ImageType") String imageType, @Part MultipartBody.Part image);

    @POST(URL_GetOrders)
    Call<ApiResponse<ResponseOrders>> getOrders(@Body ParamOrders paramOrders);

    @POST(URL_RateDriver)
    Call<ApiResponse<Object>> rateDriver(@Body ParamRateDriver paramRateDriver);

    @GET(URL_GetOrders + "/{id}")
    Call<ApiResponse<Order>> getOrderDetail(@Path("id") int id);

    @DELETE(URL_DeleteOrder + "/{id}")
    Call<ApiResponse<Object>> deleteOrder(@Path("id") int id);

    @POST(URL_GetPathHistory)
    Call<ApiResponse<ResponsePathHistory>> getPathHistory(@Body ParamPathHistory paramPathHistory);

    @POST(URL_SubmitOrder)
    Call<ApiResponse<Order>> submitOrder(@Body ParamNewOrder paramNewOrder);

    @PUT(URL_EditOrder + "/{id}")
    Call<ApiResponse<Order>> editOrder(@Path("id") int id, @Body ParamNewOrder paramNewOrder);

    @POST(URL_RecreateOrder + "/{id}")
    Call<ApiResponse<Order>> recreateOrder(@Path("id") int id);

    @GET(URL_GetNotifiedShipments)
    Call<ApiResponse<ResponseOrders>> getOffers();

    @POST(URL_PriceOffer)
    Call<ApiResponse<Object>> submitPrice(@Body ParamPrice price);

    @POST(URL_AcceptOrder + "/{id}")
    Call<ApiResponse<Object>> acceptOrder(@Path("id") int id);

    @DELETE(URL_DeleteOffer + "/{id}")
    Call<ApiResponse<Object>> deleteOffer(@Path("id") int id);

    @Multipart
    @POST(URL_SetOrderStatus)
    Call<ApiResponse<Object>> submitOrderStatus(@Header("ImageType") String imageType,
                                                      @Header("OrderId")  int orderId,
                                                      @Header("OrderState")  int orderStatus,
                                                      @Part MultipartBody.Part image);

    @POST(URL_UpdateLocation)
    Call<ApiResponse<Object>> updateLocation(@Body GeoLoc location);


    @POST(URL_GetRewards)
    Call<ApiResponse<ResponseRewards>> getRewards(@Body ParamRewards paramRewards);

    @POST(URL_GetUserRewards)
    Call<ApiResponse<ResponseUserRewards>> getUserRewards(@Body ParamRewards paramRewards);

    @POST(URL_GetPointActions)
    Call<ApiResponse<ResponsePoints>> getPointActions(@Body ParamPoints paramPoints);

    @POST(URL_SpendPoints)
    Call<ApiResponse<ResponseNull>> spendPoints(@Body ParamSpendPoints paramSpendPoints);

    @GET
    @Streaming
    Call<ResponseBody> downloadLastVersion(@Url String url);

    @GET
    @Streaming
    Call<ResponseBody> downloadFile(@Url String url);

}
