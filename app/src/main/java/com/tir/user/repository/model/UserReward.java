package com.tir.user.repository.model;

/**
 * Created by khani on 03/11/2017.
 */

public class UserReward {

    public int id;
    public String title = "";
    public String description = "";
    public int points;
    public String imageUrl = "";
    public String instructions = "";
    public String createDate = "";
    public String expireDate = "";
    public String code = "";

    public UserReward() {
    }

}
