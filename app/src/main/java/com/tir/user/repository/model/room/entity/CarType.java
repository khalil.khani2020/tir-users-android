package com.tir.user.repository.model.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

/**
 * Created by khani on 03/11/2017.
 */

@Entity()
public class CarType implements Serializable {
    @PrimaryKey
    public int id;
    public String name;

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
