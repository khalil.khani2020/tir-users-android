package com.tir.user.repository.model.api.response;

public class ResponseVerifyPhone {
    public String verificationToken;
    public String countryCode;
    public String mobile;
    public String identityToken;

    @Override
    public String toString() {
        return "ResponseVerifyPhone{" +
                "verificationToken='" + verificationToken + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", mobile='" + mobile + '\'' +
                ", identityToken='" + identityToken + '\'' +
                '}';
    }
}
