package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.Blog;

import java.util.ArrayList;


public class ResponseNotifications {

    public int totalItems;

    public ArrayList<Blog> items;

    @Override
    public String toString() {
        return "ResponseNotifications{" +
                "totalItems=" + totalItems +
                ", items=" + items +
                '}';
    }
}
