package com.tir.user.repository.model.api.response;

import android.os.Parcel;

import com.tir.user.repository.model.Order;


public class ResponseUnRatedOrders {

    public int count;
    public Order lastOne;

    protected ResponseUnRatedOrders(Parcel in) {
        count = in.readInt();
    }

    @Override
    public String toString() {
        return "ResponseUnRatedOrders{" +
                "count=" + count +
                ", lastOne=" + lastOne +
                '}';
    }
}
