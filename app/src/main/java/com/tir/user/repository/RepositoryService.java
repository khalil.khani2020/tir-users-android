package com.tir.user.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.app.AppExecutors;
import com.tir.user.repository.model.GeoLoc;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.remot.retrofit.ApiClient;

import java.io.IOException;

import okhttp3.ResponseBody;


public class RepositoryService extends RepositoryBasic{

    private LiveData<NetworkResponse<Object>> updateLocationLive;
    private final MediatorLiveData<NetworkResponse<Object>> updateLocationMediator = new MediatorLiveData<>();

    public RepositoryService(ApiClient apiClient, AppDatabase appDatabase,
                             AppExecutors appExecutors, SharedPrefsHelper sharedPrefsHelper) {
        super(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    public MediatorLiveData<NetworkResponse<Object>> updateLocation(GeoLoc location) {
        if (updateLocationLive != null) {
            updateLocationMediator.removeSource(updateLocationLive);
        }
        updateLocationLive = apiClient.updateLocation(location);
        updateLocationMediator.addSource(updateLocationLive, updateLocationMediator::setValue);
        return updateLocationMediator;
    }

    public ResponseBody downloadAPK(String url) throws IOException {
        return apiClient.downloadAPK(url);
    }





}
