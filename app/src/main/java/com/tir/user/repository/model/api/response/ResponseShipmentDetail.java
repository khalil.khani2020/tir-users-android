package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.Order;


public class ResponseShipmentDetail {

    public Order order;

    @Override
    public String toString() {
        return "{" + "order=" + order + '}';
    }
}
