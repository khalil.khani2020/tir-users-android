package com.tir.user.repository.model;

import com.tir.user.repository.model.room.entity.CarType;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.Currency;
import com.tir.user.repository.model.room.entity.ShipmentType;
import com.tir.user.repository.model.room.entity.WeightRange;
import com.tir.user.utils.Utils;

/**
 * Created by khani on 03/11/2017.
 */

public class PathHistory {

    public int id;
    public City sourceCity;
    public City targetCity;
    public CarType carType;
    public ShipmentType shipmentType;
    public WeightRange weightRange;
    public Currency currency;
    public Double price;
    public String date;

    public PathHistory() {
    }

    public String getFormattedPrice() {
        if (price == null)
            return "";
        return Utils.formatPrice((long) price.doubleValue());
    }

    @Override
    public String toString() {
        return "PathHistory{" +
                "id=" + id +
                ", sourceCity=" + sourceCity +
                ", targetCity=" + targetCity +
                ", carType=" + carType +
                ", shipmentType=" + shipmentType +
                ", weightRange=" + weightRange +
                ", currency=" + currency +
                ", price=" + price +
                ", date='" + date + '\'' +
                '}';
    }
}
