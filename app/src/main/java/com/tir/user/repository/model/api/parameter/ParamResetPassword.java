package com.tir.user.repository.model.api.parameter;

public class ParamResetPassword {
    public String identityToken;
    public String verificationToken;
    public String password;
    public String confirmPassword;
}
