package com.tir.user.repository.model.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by khani on 15/02/2018.
 */
@Entity
public class DBChangeInfo {
    @PrimaryKey
    @SerializedName("id")
    @Expose(serialize = false)
    public int id = 1;
    public int carTypeChangeId = 0;
    public int shipmentTypeChangeId = 0;
    public int currencyChangeId = 0;
    public int cityChangeId = 0;
    public int weightRangeChangeId = 0;
    public int ratingFactorChangeId = 0;
    public int pathCurrencyChangeId = 0;
    public int orderStateChangeId = 0;
    @Expose(serialize = false)
    public String supportPhone = "";
    @Expose(serialize = false)
    public int locationInterval = 1200000;
    @Expose(serialize = false)
    public int fastestLocationInterval = 900000;
    public int appVersion = 1;
    @Expose(serialize = false)
    public boolean forceUpdate;
    @Expose(serialize = false)
    public String directUrl = "";
    @Expose(serialize = false)
    public String googlePlayUrl = "";
    @Expose(serialize = false)
    public String cafeBazarUrl = "";
    @Expose(serialize = false)
    public String playServicesUpdateUrl = "";
    public String playServicesVersion = "";

    @Override
    public String toString() {
        return "DBChangeInfo{" +
                "id=" + id +
                ", carTypeChangeId=" + carTypeChangeId +
                ", shipmentTypeChangeId=" + shipmentTypeChangeId +
                ", currencyChangeId=" + currencyChangeId +
                ", cityChangeId=" + cityChangeId +
                ", weightRangeChangeId=" + weightRangeChangeId +
                ", ratingFactorChangeId=" + ratingFactorChangeId +
                ", pathCurrencyChangeId=" + pathCurrencyChangeId +
                ", orderStateChangeId=" + orderStateChangeId +
                ", SupportPhone='" + supportPhone + '\'' +
                ", locationInterval=" + locationInterval +
                ", fastestLocationInterval=" + fastestLocationInterval +
                ", appVersion=" + appVersion +
                ", forceUpdate=" + forceUpdate +
                ", directUrl='" + directUrl + '\'' +
                ", googlePlayUrl='" + googlePlayUrl + '\'' +
                ", cafeBazarUrl='" + cafeBazarUrl + '\'' +
                ", playServicesUpdateUrl='" + playServicesUpdateUrl + '\'' +
                ", playServicesVersion='" + playServicesVersion + '\'' +
                '}';
    }
}
