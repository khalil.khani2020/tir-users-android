package com.tir.user.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tir.user.app.StaticVariables;


public class SharedPrefsHelper {

    //SharedPreferences Key:
    private static final String PREFS_KEY_SESSION_ID = "SessionId";
    private static final String PREFS_KEY_CULTURE = "Culture";
    private static final String PREFS_KEY_IS_LOGGED_IN = "IsLoggedIn";
    private static final String PREFS_KEY_CALENDAR_TYPE = "CalendarType";
    private static final String PREFS_KEY_UPDATE_FCM_TOKEN = "setFcmTokenUpdateStatus";
    private static final String PREFS_KEY_DELETE_FCM_TOKEN = "setFcmTokenDeleteStatus";


    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    public SharedPrefsHelper(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        getPrefs();
    }

    public void saveSessionId(String sessionId) {
        StaticVariables.SESSION.sessionId = sessionId;
        editor.putString(PREFS_KEY_SESSION_ID, sessionId);
        editor.commit();
    }

    public void saveCulture(String culture) {
        StaticVariables.CULTURE = culture;
        editor.putString(PREFS_KEY_CULTURE, culture);
        editor.commit();
    }

    public void saveLoginStatus(boolean isLoggedIn) {
        StaticVariables.IS_LOGGED_IN = isLoggedIn;
        editor.putBoolean(PREFS_KEY_IS_LOGGED_IN, isLoggedIn);
        editor.commit();
    }

    public void saveCalendarType(String calendarType) {
        StaticVariables.CALENDAR_TYPE = calendarType;
        editor.putString(PREFS_KEY_CALENDAR_TYPE, calendarType);
        editor.commit();
    }

    public void getPrefs() {
//        StaticVariables.SESSION.sessionId = sharedPreferences.getString(PREFS_KEY_SESSION_ID, "");
        StaticVariables.CULTURE = sharedPreferences.getString(PREFS_KEY_CULTURE, "");
        StaticVariables.IS_LOGGED_IN = sharedPreferences.getBoolean(PREFS_KEY_IS_LOGGED_IN, false);
        StaticVariables.CALENDAR_TYPE = sharedPreferences.getString(PREFS_KEY_CALENDAR_TYPE, "");
    }

    public void clearPrefs() {
        editor.remove(PREFS_KEY_SESSION_ID);
        editor.remove(PREFS_KEY_CULTURE);
        editor.remove(PREFS_KEY_IS_LOGGED_IN);
        editor.remove(PREFS_KEY_CALENDAR_TYPE);
        editor.remove(PREFS_KEY_UPDATE_FCM_TOKEN);
        editor.remove(PREFS_KEY_DELETE_FCM_TOKEN);
        editor.commit();
    }

    public boolean isUpdateFcmTokenNeeded() {
        return sharedPreferences.getBoolean(PREFS_KEY_UPDATE_FCM_TOKEN, true);
    }

    public boolean isDeleteFcmTokenNeeded() {
        return sharedPreferences.getBoolean(PREFS_KEY_DELETE_FCM_TOKEN, false);
    }

    public void setFcmTokenUpdateStatus(boolean updatedNeeded) {
        editor.putBoolean(PREFS_KEY_UPDATE_FCM_TOKEN, updatedNeeded);
        editor.commit();
    }

    public void setFcmTokenDeleteStatus(boolean deleteNeeded) {
        editor.putBoolean(PREFS_KEY_DELETE_FCM_TOKEN, deleteNeeded);
        editor.commit();
    }

}
