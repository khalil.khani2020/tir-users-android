package com.tir.user.repository.model.api.response;


import com.tir.user.repository.model.PointAction;

import java.util.ArrayList;


public class ResponsePointActions {

    public int totalItems;

    public ArrayList<PointAction> items;

    @Override
    public String toString() {
        return "ResponsePointActions{" +
                "totalItems=" + totalItems +
                ", items=" + items +
                '}';
    }
}
