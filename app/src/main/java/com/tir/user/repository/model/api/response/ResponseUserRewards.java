package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.UserReward;

import java.util.ArrayList;


public class ResponseUserRewards {

    public int totalItems;

    public ArrayList<UserReward> items;

    @Override
    public String toString() {
        return "ResponseRewards{" +
                "totalItems=" + totalItems +
                ", items=" + items +
                '}';
    }
}
