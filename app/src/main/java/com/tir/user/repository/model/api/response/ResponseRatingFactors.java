package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.room.entity.RatingFactor;

import java.util.ArrayList;


public class ResponseRatingFactors {

    public ArrayList<RatingFactor> positives;
    public ArrayList<RatingFactor> negatives;

    @Override
    public String toString() {
        return "ResponseRatingFactors{" +
                "positives=" + positives +
                ", negatives=" + negatives +
                '}';
    }
}
