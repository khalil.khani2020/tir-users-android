package com.tir.user.repository.model;

public class CustomImage {
    public String url;
    public String description;

    public CustomImage(String url, String description) {
        this.url = url;
        this.description = description;
    }
}
