package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.PathHistory;
import com.tir.user.repository.model.room.entity.Currency;

import java.util.ArrayList;


public class ResponsePathHistory {

    public int totalItems;
    public Currency pathCurrency;
    public ArrayList<PathHistory> pathHistory;

    @Override
    public String toString() {
        return "ResponsePathHistory{" +
                "totalItems=" + totalItems +
                ", pathCurrency=" + pathCurrency +
                ", pathHistory=" + pathHistory +
                '}';
    }
}
