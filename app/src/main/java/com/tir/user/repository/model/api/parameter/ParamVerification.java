package com.tir.user.repository.model.api.parameter;

/**
 * Created by khani on 15/02/2018.
 */

public class ParamVerification {

    public String countryCode;
    public String mobile;
    public String verifyCode;
    public int type;
}
