package com.tir.user.repository.model.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by khani on 03/11/2017.
 */

@Entity()
public class Country{
    @PrimaryKey
    public int id;
    public String countryCode;
    public String name;
    public boolean registerAllowed;

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", countryCode='" + countryCode + '\'' +
                ", name='" + name + '\'' +
                ", registerAllowed=" + registerAllowed +
                '}';
    }
}