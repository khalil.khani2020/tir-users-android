package com.tir.user.repository.model.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by khani on 03/11/2017.
 */

@Entity()
public class OrderState {
    @PrimaryKey
    public int code;
    public String name;

    @Override
    public String toString() {
        return "{" +
                "id=" + code +
                ", name='" + name + '\'' +
                '}';
    }
}
