package com.tir.user.repository.model.api;

public class ApiResponse<T> {

    // TODO: 2018/09/06 check this with server codes

    // Common codes:
    public static final int UNAUTHORIZED = 401;
    public static final int COMMON_SUCCESSFUL = 2000;
    //    public static final int COMMON_SUCCESSFUL = 100;
    public static final int COMMON_FAILURE = 1000;
    //    public static final int COMMON_FAILURE = 101;
    public static final int COMMON_BODY_PARAMS_NOT_SUPPLIED = 102;
    public static final int COMMON_HEADER_PARAMS_NOT_SUPPLIED = 103;
    public static final int COMMON_UNAUTHORIZED = 104;
    public static final int COMMON_SESSION_NOT_FOUND = 105;
    public static final int COMMON_SESSION_IS_INACTIVE = 106;
    public static final int COMMON_LOGIN_NEEDED = 120;

    // Registration API codes:
    public static final int REGISTRATION_NUMBER_EXISTS = 210;
    public static final int REGISTRATION_INVALID_NUMBER = 211;
    public static final int REGISTRATION_CAN_NOT_SEND_SMS = 212;

    // Verification API codes: TODO : improve name and toast message of this statuses:
    public static final int VERIFICATION_MOBILE_SEND_SMS_CONSTRAINT = 1004;
    public static final int VERIFICATION_IP_SEND_SMS_CONSTRAINT = 1005;
    public static final int VERIFICATION_SMS_EXPIRED_OR_NOT_VALID = 1006;

    // UploadImage API codes:
    public static final int UPLOAD_INVALID_FILE_FORMAT = 232;


    public ResponseStatus status;
    public T data;

    @Override
    public String toString() {
        return "{" +
                "state=" + status +
                ", data=" + data +
                '}';
    }
}
