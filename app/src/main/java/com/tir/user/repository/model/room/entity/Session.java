package com.tir.user.repository.model.room.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import com.tir.user.repository.model.api.response.ResponseUnRatedOrders;

@Entity()
public class Session {
    @PrimaryKey
    public int id = 1;
    public String userId = "";
    @SerializedName("userName")
    public String userName = "";
    public String firstName = "";
    public String lastName = "";
    public String sessionId = "";
    public String accessToken = "";
    public String refreshToken = "";
    public String carTypeId = "";
    public String profileImageUrl = "";
    public String carImageUrl = "";
    public float rating = 5.0f;
    public boolean isProfileCompleted;
    public String roles;
    @Ignore
    public ResponseUnRatedOrders unRatedOrders;

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", carTypeId='" + carTypeId + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                ", carImageUrl='" + carImageUrl + '\'' +
                ", rating=" + rating +
                ", isProfileCompleted=" + isProfileCompleted +
                ", roles='" + roles + '\'' +
                ", unRatedOrders=" + unRatedOrders +
                '}';
    }
}
