package com.tir.user.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.app.AppExecutors;
import com.tir.user.app.MyApp;
import com.tir.user.app.StaticVariables;
import com.tir.user.repository.model.Status;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamFcmToken;
import com.tir.user.repository.model.api.response.ResponseBasicInfo;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.model.room.entity.CarType;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.Country;
import com.tir.user.repository.model.room.entity.DBChangeInfo;
import com.tir.user.repository.model.room.entity.OrderState;
import com.tir.user.repository.model.room.entity.PathCurrency;
import com.tir.user.repository.model.room.entity.RatingFactor;
import com.tir.user.repository.model.room.entity.Session;
import com.tir.user.repository.model.room.entity.ShipmentType;
import com.tir.user.repository.model.room.entity.WeightRange;
import com.tir.user.repository.remot.retrofit.ApiClient;

import java.util.List;


public class RepositoryBasic {

    protected final String TAG = this.getClass().getSimpleName();

    final ApiClient apiClient;
    private final AppDatabase appDatabase;
    public final AppExecutors appExecutors;
    private final SharedPrefsHelper prefs;

    private LiveData<NetworkResponse<ResponseBasicInfo>> basicInfoLive;
    private final MediatorLiveData<NetworkResponse<ResponseBasicInfo>> basicInfoMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<Object>> fcmTokenLive;
    private final MediatorLiveData<NetworkResponse<Object>> fcmTokenMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Object>> logoutMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<Object>> logoutLive;


    public RepositoryBasic(ApiClient apiClient, AppDatabase appDatabase,
                           AppExecutors appExecutors, SharedPrefsHelper prefs) {
        this.apiClient = apiClient;
        this.appDatabase = appDatabase;
        this.appExecutors = appExecutors;
        this.prefs = prefs;
        getSession();
    }

    public MediatorLiveData<NetworkResponse<ResponseBasicInfo>> getBasicInfo() {
        DBChangeInfo dbChangeInfo = appDatabase.changesDAO().getDBChange();
        if (dbChangeInfo == null) {
            dbChangeInfo = new DBChangeInfo();
        }
        dbChangeInfo.appVersion = MyApp.getInstance().getVersionCode();
        dbChangeInfo.playServicesVersion = MyApp.getInstance().getPlayServicesVersion();

        if (basicInfoLive != null) {
            basicInfoMediator.removeSource(basicInfoLive);
        }

        basicInfoLive = apiClient.fetchBasicInfo(dbChangeInfo);
        basicInfoMediator.addSource(basicInfoLive, networkResponse -> {
            if (networkResponse.getStatus() == Status.SUCCESS) {
                if (networkResponse.getApiResponse().status.code == ApiResponse.COMMON_SUCCESSFUL) {
                    persistBasicInfo(networkResponse.getApiResponse().data);
                }
            }
            basicInfoMediator.setValue(networkResponse);
        });
        return basicInfoMediator;
    }

    private void persistBasicInfo(ResponseBasicInfo basicInfo) {
        appExecutors.diskIO().execute(() -> {
            if (basicInfo.dbChangeInfo != null) {
                appDatabase.changesDAO().insertDBChange(basicInfo.dbChangeInfo);

                StaticVariables.SupportPhone = basicInfo.dbChangeInfo.supportPhone;
                int locationInterval = basicInfo.dbChangeInfo.locationInterval;
                int fastestLocationInterval = basicInfo.dbChangeInfo.fastestLocationInterval;
                if (locationInterval > 0)
                    StaticVariables.LocationInterval = locationInterval;
                if (fastestLocationInterval > 0)
                    StaticVariables.FastestLocationInterval = fastestLocationInterval;
            }
            if (basicInfo.countries != null) {
                appDatabase.basicInfoDAO().deleteCities();
                appDatabase.basicInfoDAO().deleteCountries();
                appDatabase.basicInfoDAO().insertCountries(basicInfo.countries);
            }
            if (basicInfo.cities != null) {
                appDatabase.basicInfoDAO().deleteCities();
                appDatabase.basicInfoDAO().insertCities(basicInfo.cities);
            }
            if (basicInfo.currencies != null) {
                appDatabase.basicInfoDAO().deleteCurrencies();
                appDatabase.basicInfoDAO().insertCurrencies(basicInfo.currencies);
            }
            if (basicInfo.carTypes != null) {
                appDatabase.basicInfoDAO().deleteCarTypes();
                appDatabase.basicInfoDAO().insertCarTypes(basicInfo.carTypes);
            }
            if (basicInfo.orderStates != null) {
                appDatabase.basicInfoDAO().deleteOrderStates();
                appDatabase.basicInfoDAO().insertOrderStates(basicInfo.orderStates);
            }
            if (basicInfo.shipmentTypes != null) {
                appDatabase.basicInfoDAO().deleteShipmentTypes();
                appDatabase.basicInfoDAO().insertShipmentTypes(basicInfo.shipmentTypes);
            }
            if (basicInfo.weightRanges != null) {
                appDatabase.basicInfoDAO().deleteWeights();
                appDatabase.basicInfoDAO().insertWeights(basicInfo.weightRanges);
            }
            if (basicInfo.ratingFactors != null) {
                appDatabase.basicInfoDAO().deleteRatingFactors();
                appDatabase.basicInfoDAO().insertRatingFactors(basicInfo.ratingFactors.positives);
                appDatabase.basicInfoDAO().insertRatingFactors(basicInfo.ratingFactors.negatives);
            }

            if (basicInfo.pathCurrencies != null) {
                appDatabase.basicInfoDAO().deletePathCurrencies();
                appDatabase.basicInfoDAO().insertPathCurrencies(basicInfo.pathCurrencies);
            }
            if (basicInfo.userInfo != null) {
                appDatabase.sessionDAO().updateProfileInfo(
                        basicInfo.userInfo.userId,
                        basicInfo.userInfo.firstName,
                        basicInfo.userInfo.lastName,
                        basicInfo.userInfo.userName,
                        basicInfo.userInfo.profileImageUrl,
                        basicInfo.userInfo.carImageUrl,
                        basicInfo.userInfo.rating,
                        basicInfo.userInfo.isProfileCompleted,
                        basicInfo.userInfo.roles);
                getSession();
            }
        });
    }

    public MediatorLiveData<NetworkResponse<Object>> updateFcmToken(ParamFcmToken paramFcmToken) {
        if (fcmTokenLive != null) {
            fcmTokenMediator.removeSource(fcmTokenLive);
        }

        fcmTokenLive = apiClient.updateFcmToken(paramFcmToken);
        fcmTokenMediator.addSource(fcmTokenLive, networkResponse -> {
            fcmTokenMediator.postValue(networkResponse);
        });
        return fcmTokenMediator;
    }

    public MediatorLiveData<NetworkResponse<Object>> logout() {
        if (logoutLive != null) {
            logoutMediator.removeSource(logoutLive);
        }

        logoutLive = apiClient.logout();
        logoutMediator.addSource(logoutLive, networkResponse -> {
            logoutMediator.postValue(networkResponse);
        });
        return logoutMediator;
    }

    private void getSession() {
        StaticVariables.SESSION = appDatabase.sessionDAO().getSession();
        if (StaticVariables.SESSION == null) {
            StaticVariables.SESSION = new Session();
        }
    }

    public List<CarType> getCarTypes() {
        return appDatabase.basicInfoDAO().getCarTypes();
    }

    public List<OrderState> getOrderStates() {
        return appDatabase.basicInfoDAO().getOrderStates();
    }

    public List<OrderState> getOrderStatesGreaterThan(int orderState) {
        return appDatabase.basicInfoDAO().getOrderStates(orderState);
    }

    public List<ShipmentType> getShipmentTypes() {
        return appDatabase.basicInfoDAO().getShipmentTypes();
    }

    public List<WeightRange> getWeights() {
        return appDatabase.basicInfoDAO().getWeightRanges();
    }

    public List<RatingFactor> getRatingFactors(boolean isPositive) {
        return appDatabase.basicInfoDAO().getRatingFactors(isPositive);
    }

    public List<Country> getCountries() {
        return appDatabase.basicInfoDAO().getCountries();
    }

    public List<PathCurrency> getPathCurrencies(int sourceCountryId, int targetCountryId){
        return appDatabase.basicInfoDAO().getPathCurrencies(sourceCountryId, targetCountryId);
    }

    public void saveSession(Session session) {
        appDatabase.sessionDAO().insertSession(session);
    }

    public void saveProfileImageUrl(String url) {
        StaticVariables.SESSION.profileImageUrl = url;
        appDatabase.sessionDAO().updateProfileImageUrl(url);
    }

    public void saveCarImageUrl(String url) {
        StaticVariables.SESSION.carImageUrl = url;
        appDatabase.sessionDAO().updateCarImageUrl(url);
    }

    public List<City> searchCity(String phrase) {
        return appDatabase.basicInfoDAO().searchCity(phrase);
    }

    public City getCity(int cityId) {
        return appDatabase.basicInfoDAO().getCity(cityId);
    }

    public CarType getCarType(int carTypeId) {
        return appDatabase.basicInfoDAO().getCarType(carTypeId);
    }

    public ShipmentType getShipmentType(int shipmentTypeId) {
        return appDatabase.basicInfoDAO().getShipmentType(shipmentTypeId);
    }

    public SharedPrefsHelper getPrefs() {
        return prefs;
    }

    public void clearSession() {
        appDatabase.sessionDAO().deleteSession();
        StaticVariables.SESSION = new Session();
    }

    public void clearPrefs() {
        prefs.clearPrefs();
    }

    public void saveLoginStatus(boolean isLoggedIn) {
        prefs.saveLoginStatus(isLoggedIn);
    }
}
