package com.tir.user.repository.model.api.parameter;

public class ParamRegister {

    public static final String TRADER = "trader";
    public static final String FORWARDER = "forwarder";

    public String mobileVerificationToken;
    public String firstName;
    public String lastName;
    public String mobile;
    public String password;
    public String confirmPassword;
    public String countryCode;
    public String userType;
}
