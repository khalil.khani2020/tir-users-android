package com.tir.user.repository.model.api.response;

public class ResponsePoints {

    public int userPoints;

    public ResponsePointActions pointActions;

    @Override
    public String toString() {
        return "ResponsePoints{" +
                "userPoints=" + userPoints +
                ", pointActions=" + pointActions +
                '}';
    }
}
