package com.tir.user.repository.model.api.parameter;

public class SortBy {
    public String by;
    public Boolean isDesc;

    public SortBy(String by, Boolean isDesc) {
        this.by = by;
        this.isDesc = isDesc;
    }
}
