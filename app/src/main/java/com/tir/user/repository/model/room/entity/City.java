package com.tir.user.repository.model.room.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(foreignKeys = @ForeignKey(
        entity = Country.class,
        parentColumns = "id",
        childColumns = "countryId",
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE),
        indices = @Index(value = "countryId"))
public class City implements Serializable {

    @PrimaryKey
    public int id;
    public int countryId;
    public double latitude;
    public double longitude;
    public String name;

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", countryId=" + countryId +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", name='" + name + '\'' +
                '}';
    }

}
