package com.tir.user.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import android.net.Uri;

import com.tir.user.app.AppExecutors;
import com.tir.user.repository.model.PersonalInfo;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseImageUpload;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.remot.retrofit.ApiClient;


public class RepositoryProfile extends RepositoryBasic{

    private LiveData<NetworkResponse<PersonalInfo>> getProfileLive;
    private final MediatorLiveData<NetworkResponse<PersonalInfo>> getProfileMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<Object>> updateProfileLive;
    private final MediatorLiveData<NetworkResponse<Object>> updateProfileMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<ResponseImageUpload>> uploadImageLive;
    private final MediatorLiveData<NetworkResponse<ResponseImageUpload>> uploadImageMediator = new MediatorLiveData<>();


    public RepositoryProfile(ApiClient apiClient, AppDatabase appDatabase,
                           AppExecutors appExecutors, SharedPrefsHelper sharedPrefsHelper) {
        super(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    public MediatorLiveData<NetworkResponse<PersonalInfo>> getProfile() {
        if (getProfileLive != null) {
            getProfileMediator.removeSource(getProfileLive);
        }
        getProfileLive = apiClient.getProfile();
        getProfileMediator.addSource(getProfileLive, getProfileMediator::setValue);
        return getProfileMediator;
    }

    public MediatorLiveData<NetworkResponse<Object>> updateProfile(PersonalInfo personalInfo) {
        if (updateProfileLive != null) {
            updateProfileMediator.removeSource(updateProfileLive);
        }
        updateProfileLive = apiClient.updateProfile(personalInfo);
        updateProfileMediator.addSource(updateProfileLive, updateProfileMediator::setValue);
        return updateProfileMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponseImageUpload>> uploadImage(String imageType, Uri imageUri) {
        if (uploadImageLive != null) {
            uploadImageMediator.removeSource(uploadImageLive);
        }
        uploadImageLive = apiClient.uploadImage(imageType, imageUri);
        uploadImageMediator.addSource(uploadImageLive, uploadImageMediator::setValue);
        return uploadImageMediator;
    }
}
