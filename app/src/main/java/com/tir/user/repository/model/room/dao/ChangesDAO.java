package com.tir.user.repository.model.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.tir.user.repository.model.room.entity.DBChangeInfo;

/**
 * Created by khani on 03/11/2017.
 */

@Dao
public interface ChangesDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDBChange(DBChangeInfo dbChange);

    @Query("SELECT * FROM DBChangeInfo WHERE id = 1")
    DBChangeInfo getDBChange();
}
