package com.tir.user.repository.model;

/**
 * Created by khani on 07/03/2018.
 */

public class OrderFilter {


    public Integer sourceCityId;
    public Integer targetCityId;
    public String loadingDate;
    public Integer shipmentTypeId;
    public Integer weightRangeId;
    public Integer carTypeId;
    public Integer orderState;


    @Override
    public String toString() {
        return "OrderFilter{" +
                "sourceCityId=" + sourceCityId +
                ", targetCityId=" + targetCityId +
                ", loadingDate='" + loadingDate + '\'' +
                ", shipmentTypeId=" + shipmentTypeId +
                ", weightRangeId=" + weightRangeId +
                ", carTypeId=" + carTypeId +
                ", orderState=" + orderState +
                '}';
    }
}
