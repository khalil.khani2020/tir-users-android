package com.tir.user.repository.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Blog implements Parcelable {
    public int id;
    public String url;
    public String title;
    public static final Creator<Blog> CREATOR = new Creator<Blog>() {
        @Override
        public Blog createFromParcel(Parcel in) {
            return new Blog(in);
        }

        @Override
        public Blog[] newArray(int size) {
            return new Blog[size];
        }
    };
    public String previewImageUrl;
    public String body;
    public String dateTime;


    protected Blog(Parcel in) {
        id = in.readInt();
        url = in.readString();
        title = in.readString();
        body = in.readString();
        previewImageUrl = in.readString();
        dateTime = in.readString();
    }
    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", previewImageUrl='" + previewImageUrl + '\'' +
                ", dateTime='" + dateTime + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(url);
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(previewImageUrl);
        parcel.writeString(dateTime);
    }
}
