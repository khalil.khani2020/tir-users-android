package com.tir.user.repository.remot.retrofit;

public class ApiServiceHolder {

    ApiService apiService = null;

    public ApiService getApiService() {
        return apiService;
    }

    public void setApiService(ApiService apiService) {
        this.apiService = apiService;
    }
}
