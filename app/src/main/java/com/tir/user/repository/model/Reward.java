package com.tir.user.repository.model;

/**
 * Created by khani on 03/11/2017.
 */

public class Reward {

    public int id;
    public String title = "";
    public String description = "";
    public int points;
    public String imageUrl = "";

    public Reward() {
    }

}
