package com.tir.user.repository.model.api.parameter;

import java.util.List;

public class ParamRateDriver {
    public int orderId;
    public int rating;
    public List<Integer> ratingFactors;
    public String description;
}
