package com.tir.user.repository.model.api.parameter;

import com.google.gson.annotations.Expose;
import com.tir.user.repository.model.GeoLoc;
import com.tir.user.repository.model.OrderFilter;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by khani on 15/02/2018.
 */

public class ParamOrders {

    public static final String Newest = "createDate";
    public static final String LastModified = "modifyDate";
    public static final String SourceCity = "sourceCity";
    public static final String TargetCity = "targetCity";
    public static final String CarType = "carType";
    public static final String ShipmentType = "shipmentType";
    public static final String WeightRange = "weightRange";
    public static final String LoadingDate = "loadingDate";
    public static final String OrderState = "orderState";
    public ArrayList<SortBy> sortBy = new ArrayList<>(Arrays.asList(new SortBy(LastModified, true)));

    public static final String Current = "current";
    public static final String Previous = "previous";
    public String category;

    public Integer page;
    public Integer pageSize = 20;

    public Integer sourceCityId;
    public Integer targetCityId;
    public Integer carTypeId;
    public Integer shipmentTypeId;
    public Integer weightRangeId;
    public String loadingDate;
    public GeoLoc location;
    public Integer orderState;

    // TODO: 28/11/2018 clear unused fields
    public Boolean MyOrders;
    public Boolean PendingOffers;
    public Boolean InProgress;
    public Boolean Completed;
    @Expose(serialize = false)
    private OrderFilter filterBy = new OrderFilter();

    public OrderFilter getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(OrderFilter filter) {
        this.filterBy = filter;
        this.sourceCityId = filter.sourceCityId;
        this.targetCityId = filter.targetCityId;
        this.carTypeId = filter.carTypeId;
        this.shipmentTypeId = filter.shipmentTypeId;
        this.weightRangeId = filter.weightRangeId;
        this.loadingDate = filter.loadingDate;
        this.orderState = filter.orderState;
    }
}
