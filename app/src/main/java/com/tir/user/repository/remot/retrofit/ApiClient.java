package com.tir.user.repository.remot.retrofit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tir.user.app.MyApp;
import com.tir.user.app.StaticVariables;
import com.tir.user.repository.SharedPrefsHelper;
import com.tir.user.repository.model.CarInfo;
import com.tir.user.repository.model.GeoLoc;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.PersonalInfo;
import com.tir.user.repository.model.Status;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamFcmToken;
import com.tir.user.repository.model.api.parameter.ParamLogin;
import com.tir.user.repository.model.api.parameter.ParamNewOrder;
import com.tir.user.repository.model.api.parameter.ParamOrderState;
import com.tir.user.repository.model.api.parameter.ParamOrders;
import com.tir.user.repository.model.api.parameter.ParamPathHistory;
import com.tir.user.repository.model.api.parameter.ParamPoints;
import com.tir.user.repository.model.api.parameter.ParamPrice;
import com.tir.user.repository.model.api.parameter.ParamRateDriver;
import com.tir.user.repository.model.api.parameter.ParamRegister;
import com.tir.user.repository.model.api.parameter.ParamResetPassword;
import com.tir.user.repository.model.api.parameter.ParamRewards;
import com.tir.user.repository.model.api.parameter.ParamSendSms;
import com.tir.user.repository.model.api.parameter.ParamSpendPoints;
import com.tir.user.repository.model.api.parameter.ParamVerification;
import com.tir.user.repository.model.api.response.ResponseBasicInfo;
import com.tir.user.repository.model.api.response.ResponseImageUpload;
import com.tir.user.repository.model.api.response.ResponseNotifications;
import com.tir.user.repository.model.api.response.ResponseNull;
import com.tir.user.repository.model.api.response.ResponseOrders;
import com.tir.user.repository.model.api.response.ResponsePathHistory;
import com.tir.user.repository.model.api.response.ResponsePoints;
import com.tir.user.repository.model.api.response.ResponseRewards;
import com.tir.user.repository.model.api.response.ResponseUserRewards;
import com.tir.user.repository.model.api.response.ResponseVerifyPhone;
import com.tir.user.repository.model.api.response.SendSms;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.model.room.entity.DBChangeInfo;
import com.tir.user.repository.model.room.entity.Session;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tir.user.utils.Utils.getBytes;

public class ApiClient {

    private final Context context;
    private final ApiService apiService;
    private final AppDatabase appDatabase;
    private final SharedPrefsHelper prefsHelper;

    public ApiClient(Context context, ApiService apiService, AppDatabase appDatabase, SharedPrefsHelper prefsHelper) {
        this.apiService = apiService;
        this.context = context;
        this.appDatabase = appDatabase;
        this.prefsHelper = prefsHelper;
    }


    public MutableLiveData<NetworkResponse<ResponseBasicInfo>> fetchBasicInfo(DBChangeInfo dbChangeInfo) {
        MutableLiveData<NetworkResponse<ResponseBasicInfo>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponseBasicInfo>> call = apiService.getBasicInfo(dbChangeInfo);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> updateFcmToken(ParamFcmToken paramFcmToken) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.postValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.updateFcmToken(paramFcmToken);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> logout() {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.postValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.logout();
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Session>> login(ParamLogin paramLogin) {
        MutableLiveData<NetworkResponse<Session>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Session>> call = apiService.login(paramLogin);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<SendSms>> sendSms(ParamSendSms paramSendSms) {
        MutableLiveData<NetworkResponse<SendSms>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<SendSms>> call = apiService.sendSms(paramSendSms);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<SendSms>> resendSMS(ParamSendSms paramSendSms) {
        MutableLiveData<NetworkResponse<SendSms>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<SendSms>> call = apiService.sendSms(paramSendSms);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<ResponseVerifyPhone>> verifyPhoneNumber(ParamVerification verificationCode) {
        MutableLiveData<NetworkResponse<ResponseVerifyPhone>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponseVerifyPhone>> call = apiService.verifyPhoneNumber(verificationCode);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> resetPassword(ParamResetPassword resetPassword) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.resetPassword(resetPassword);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Session>> register(ParamRegister paramRegister) {
        MutableLiveData<NetworkResponse<Session>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Session>> call = apiService.register(paramRegister);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<PersonalInfo>> getProfile() {
        MutableLiveData<NetworkResponse<PersonalInfo>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<PersonalInfo>> call = apiService.getProfile();
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<ResponseNotifications>> getNotifications() {
        MutableLiveData<NetworkResponse<ResponseNotifications>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponseNotifications>> call = apiService.getNotifications();
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<ResponseImageUpload>> uploadImage(String imageType, Uri imageUri) {
        MutableLiveData<NetworkResponse<ResponseImageUpload>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        try {
            InputStream is = context.getContentResolver().openInputStream(imageUri);
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), getBytes(is));
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);
            Call<ApiResponse<ResponseImageUpload>> call = apiService.uploadImage(imageType, body);
            callApi(networkResponse, call);
            return networkResponse;
        } catch (Exception e) {
            e.printStackTrace();
            networkResponse.setValue(new NetworkResponse<>(Status.FAILURE, null));
            return networkResponse;
        }
    }

    public MutableLiveData<NetworkResponse<Object>> updateProfile(PersonalInfo personalInfo) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.updateProfile(personalInfo);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> updateCarInfo(CarInfo carInfo) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.updateCarInfo(carInfo);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> rateDriver(ParamRateDriver paramRateDriver) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.rateDriver(paramRateDriver);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<ResponseOrders>> getOrders(ParamOrders paramOrders) {
        MutableLiveData<NetworkResponse<ResponseOrders>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponseOrders>> call = apiService.getOrders(paramOrders);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Order>> getOrderDetail(int orderId) {
        MutableLiveData<NetworkResponse<Order>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Order>> call = apiService.getOrderDetail(orderId);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> deleteOrder(int orderId) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.deleteOrder(orderId);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<ResponsePathHistory>> getPathHistory(ParamPathHistory paramPathHistory) {
        MutableLiveData<NetworkResponse<ResponsePathHistory>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponsePathHistory>> call = apiService.getPathHistory(paramPathHistory);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Order>> submitOrder(ParamNewOrder paramNewOrder) {
        MutableLiveData<NetworkResponse<Order>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Order>> call = apiService.submitOrder(paramNewOrder);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Order>> editOrder(int id, ParamNewOrder paramNewOrder) {
        MutableLiveData<NetworkResponse<Order>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Order>> call = apiService.editOrder(id, paramNewOrder);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Order>> recreateOrder(int id) {
        MutableLiveData<NetworkResponse<Order>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Order>> call = apiService.recreateOrder(id);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<ResponseOrders>> getOffers() {
        MutableLiveData<NetworkResponse<ResponseOrders>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponseOrders>> call = apiService.getOffers();
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> submitPrice(ParamPrice price) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.submitPrice(price);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> acceptOrder(int id) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.acceptOrder(id);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> deleteOffer(int id) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.deleteOffer(id);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<Object>> submitOrderStatus(String imageType, Bitmap bitmap, ParamOrderState orderStatus) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), byteArray);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);
            Call<ApiResponse<Object>> call = apiService.submitOrderStatus(imageType, orderStatus.orderId, orderStatus.state, body);
            callApi(networkResponse, call);
            return networkResponse;
        } catch (Exception e) {
            e.printStackTrace();
            networkResponse.setValue(new NetworkResponse<>(Status.FAILURE, null));
            return networkResponse;
        }
    }

    public MutableLiveData<NetworkResponse<Object>> updateLocation(GeoLoc location) {
        MutableLiveData<NetworkResponse<Object>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<Object>> call = apiService.updateLocation(location);
        callApi(networkResponse, call);
        return networkResponse;
    }


    public MutableLiveData<NetworkResponse<ResponseRewards>> getRewards(ParamRewards paramRewards) {
        MutableLiveData<NetworkResponse<ResponseRewards>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponseRewards>> call = apiService.getRewards(paramRewards);
        callApi(networkResponse, call);
        return networkResponse;
    }


    public MutableLiveData<NetworkResponse<ResponseNull>> spendPoints(ParamSpendPoints paramSpendPoints) {
        MutableLiveData<NetworkResponse<ResponseNull>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponseNull>> call = apiService.spendPoints(paramSpendPoints);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<ResponseUserRewards>> getUserRewards(ParamRewards paramRewards) {
        MutableLiveData<NetworkResponse<ResponseUserRewards>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponseUserRewards>> call = apiService.getUserRewards(paramRewards);
        callApi(networkResponse, call);
        return networkResponse;
    }

    public MutableLiveData<NetworkResponse<ResponsePoints>> getPointActions(ParamPoints paramPoints) {
        MutableLiveData<NetworkResponse<ResponsePoints>> networkResponse = new MutableLiveData<>();
        networkResponse.setValue(new NetworkResponse<>(Status.LOADING, null));
        Call<ApiResponse<ResponsePoints>> call = apiService.getPointActions(paramPoints);
        callApi(networkResponse, call);
        return networkResponse;
    }


    private <T> void callApi(MutableLiveData<NetworkResponse<T>> resourceMutableLiveData, Call call) {
        call.enqueue(new Callback<ApiResponse<T>>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse<T>> call, @NonNull Response<ApiResponse<T>> response) {
                if (response.isSuccessful()) {
                    resourceMutableLiveData.setValue(new NetworkResponse<>(Status.SUCCESS, response.body()));
                } else {
                    if (response.code() == ApiResponse.UNAUTHORIZED)
                        resetUser();
                    else
                        resourceMutableLiveData.setValue(new NetworkResponse<>(Status.FAILURE, null));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse<T>> call, @NonNull Throwable t) {
                t.printStackTrace();
                resourceMutableLiveData.setValue(new NetworkResponse<>(Status.FAILURE, null));
            }
        });
    }

    public ResponseBody downloadAPK(String url) throws IOException {
        Call<ResponseBody> request = apiService.downloadLastVersion(url);
        return request.execute().body();
    }

    private void resetUser() {
        prefsHelper.clearPrefs();
        appDatabase.sessionDAO().deleteSession();
        StaticVariables.SESSION = new Session();
        // TODO: 2018/09/22 inject appExecutors
        new Thread(() -> {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        Intent intent = context.getPackageManager().
                getLaunchIntentForPackage(context.getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MyApp.getInstance().finishAllActivities();
        context.startActivity(intent);
    }
}
