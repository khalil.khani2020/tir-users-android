package com.tir.user.repository.model;

/**
 * Created by khani on 26/09/2017.
 */

public enum Status {
    SUCCESS,
    FAILURE,
    LOADING
}
