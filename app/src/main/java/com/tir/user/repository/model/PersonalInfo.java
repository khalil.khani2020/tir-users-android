package com.tir.user.repository.model;

import com.google.gson.annotations.Expose;


public class PersonalInfo {

    public String firstName = "";
    public String lastName = "";
    public String company = "";
    public String email = "";
    public String phoneNumber = "";
    @Expose(serialize = false)
    public String profileImageUrl = "";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonalInfo)) return false;

        PersonalInfo that = (PersonalInfo) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null)
            return false;
        if (lastName != null ? lastName.equals(that.lastName) : that.lastName == null)
            return false;
        if(company != null ? company.equals(that.company) : that.company == null)
            return false;
        if(company != null ? email.equals(that.email) : that.email == null)
            return false;
        if(company != null ? phoneNumber.equals(that.phoneNumber) : that.phoneNumber == null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (company != null ? company.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PersonalInfo{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", company='" + company + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                '}';
    }
}
