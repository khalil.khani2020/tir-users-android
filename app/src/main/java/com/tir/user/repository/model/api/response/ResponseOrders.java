package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.Order;

import java.util.ArrayList;


public class ResponseOrders {

    public int totalItems;

    public ArrayList<Order> items;

    @Override
    public String toString() {
        return "ResponseOrders{" +
                "totalItems=" + totalItems +
                ", items=" + items +
                '}';
    }
}
