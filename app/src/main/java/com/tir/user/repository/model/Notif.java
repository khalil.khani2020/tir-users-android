package com.tir.user.repository.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class Notif implements Parcelable {

    public static class Keys {
        public static final String ORDER_ID = "orderId";
        public static final String TYPE = "type";
        public static final String Data = "data";
    }

    public static class Types {

        public static final int OrderRejected = 10;
        public static final int OrderConfirmed = 20;
        public static final int OrderAllocated = 30;
        public static final int OrderLoaded = 40;
        public static final int OrderDelivered = 50;
        public static final int OrderClosed = 60;
        public static final int OrderCancelled = 70;
        public static final int OrderDeleted = 80;
        public static final int Blog = 1000;
    }


    public static class ClickActions {
        public static final String OfferDetail = "offerDetail";
        public static final String OrderDetail = "orderDetail";
        public static final String Blog = "blog";
    }

    public String data;
    public int type;
    public String title;
    public int channelId;
    public String body;
    public String clickAction;

    public Notif() {

    }

    public int extractOrderId() {
        try {
            JSONObject jsonObject = new JSONObject(data);
            return jsonObject.getInt(Notif.Keys.ORDER_ID);
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }


    public Blog extractBlog() {
        try {
            Gson gson = new Gson();
            gson.fromJson(data, Blog.class);
            return gson.fromJson(data, Blog.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int extractBlogId() {
        Blog blog = extractBlog();
        if (blog != null)
            return blog.id;
        else
            return 0;
    }

    protected Notif(Parcel in) {
        data = in.readString();
        type = in.readInt();
        title = in.readString();
        channelId = in.readInt();
        body = in.readString();
        clickAction = in.readString();
    }

    public static final Creator<Notif> CREATOR = new Creator<Notif>() {
        @Override
        public Notif createFromParcel(Parcel in) {
            return new Notif(in);
        }

        @Override
        public Notif[] newArray(int size) {
            return new Notif[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(data);
        parcel.writeInt(type);
        parcel.writeString(title);
        parcel.writeInt(channelId);
        parcel.writeString(body);
        parcel.writeString(clickAction);
    }

}
