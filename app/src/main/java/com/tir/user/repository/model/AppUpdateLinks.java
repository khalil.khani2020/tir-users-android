package com.tir.user.repository.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AppUpdateLinks implements Parcelable {

    public boolean isUpdateAvailable;
    public boolean isForceUpdate;
    public String playLink = "";
    public String bazarLink = "";
    public String directLink = "";
    public boolean isPlayServicesUpdateNeeded;
    public String playServicesUpdateUrl = "";

    public AppUpdateLinks() {
    }


    protected AppUpdateLinks(Parcel in) {
        isUpdateAvailable = in.readByte() != 0;
        isForceUpdate = in.readByte() != 0;
        playLink = in.readString();
        bazarLink = in.readString();
        directLink = in.readString();
        isPlayServicesUpdateNeeded = in.readByte() != 0;
        playServicesUpdateUrl = in.readString();
    }

    public static final Creator<AppUpdateLinks> CREATOR = new Creator<AppUpdateLinks>() {
        @Override
        public AppUpdateLinks createFromParcel(Parcel in) {
            return new AppUpdateLinks(in);
        }

        @Override
        public AppUpdateLinks[] newArray(int size) {
            return new AppUpdateLinks[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isUpdateAvailable ? 1 : 0));
        dest.writeByte((byte) (isForceUpdate ? 1 : 0));
        dest.writeString(playLink);
        dest.writeString(bazarLink);
        dest.writeString(directLink);
        dest.writeByte((byte) (isPlayServicesUpdateNeeded ? 1 : 0));
        dest.writeString(playServicesUpdateUrl);
    }
}
