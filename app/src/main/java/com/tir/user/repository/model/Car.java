package com.tir.user.repository.model;

public class Car {
    public String title;
    public String color;
    public String carImageUrl;
    public String capotagePlate;
    public String plate;
}
