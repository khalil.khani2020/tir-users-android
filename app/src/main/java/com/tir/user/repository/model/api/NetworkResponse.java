package com.tir.user.repository.model.api;

import com.tir.user.repository.model.Status;

/**
 * Created by khani on 26/09/2017.
 */

public class NetworkResponse<T> {
    private final ApiResponse<T> apiResponse;
    private final Status status;

    public NetworkResponse(Status status, ApiResponse<T> apiResponse) {
        this.status = status;
        this.apiResponse = apiResponse;
    }

    public ApiResponse<T> getApiResponse() {
        return apiResponse;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "NetworkResponse{" +
                "state=" + status +
                ", apiResponse=" + apiResponse +
                '}';
    }
}
