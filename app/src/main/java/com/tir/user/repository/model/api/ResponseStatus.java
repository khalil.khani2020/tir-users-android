package com.tir.user.repository.model.api;

/**
 * Created by khani on 17/11/2017.
 */

public class ResponseStatus {

    public int code;
    public String message;

    @Override
    public String toString() {
        return "{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
