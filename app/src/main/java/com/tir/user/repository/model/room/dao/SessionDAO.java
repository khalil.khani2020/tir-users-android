package com.tir.user.repository.model.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.tir.user.repository.model.room.entity.Session;

@Dao
public interface SessionDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSession(Session session);

    @Query("SELECT * FROM Session WHERE id=1")
    Session getSession();

    @Query("DELETE FROM Session WHERE id=1")
    void deleteSession();

    @Query("UPDATE Session SET userId = :userId WHERE id=1")
    int updateUserId(int userId);

    @Query("UPDATE Session SET sessionId = :sessionId WHERE id=1")
    int updateSessionId(String sessionId);

    @Query("UPDATE Session SET accessToken = :accessToken WHERE id=1")
    int updateAccessToken(String accessToken);

    @Query("UPDATE Session SET firstName = :firstName, lastName = :lastName WHERE id=1")
    int updateName(String firstName, String lastName);

    @Query("UPDATE Session SET " +
            "userId = :userId, firstName = :firstName, lastName = :lastName, " +
            "userName = :mobilePhone, profileImageUrl = :profileImageUrl, " +
            "carImageUrl = :carImageUrl, rating = :rating, isProfileCompleted = :isProfileCompleted, roles = :roles WHERE id=1")
    void updateProfileInfo(String userId, String firstName, String lastName, String mobilePhone,
                           String profileImageUrl, String carImageUrl, float rating, boolean isProfileCompleted, String roles);

    @Query("UPDATE Session SET profileImageUrl = :url WHERE id=1")
    void updateProfileImageUrl(String url);

    @Query("UPDATE Session SET  carImageUrl = :url WHERE id=1")
    void updateCarImageUrl(String url);
}
