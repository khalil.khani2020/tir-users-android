package com.tir.user.repository.model.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.tir.user.repository.model.room.entity.CarType;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.Country;
import com.tir.user.repository.model.room.entity.Currency;
import com.tir.user.repository.model.room.entity.OrderState;
import com.tir.user.repository.model.room.entity.PathCurrency;
import com.tir.user.repository.model.room.entity.RatingFactor;
import com.tir.user.repository.model.room.entity.ShipmentType;
import com.tir.user.repository.model.room.entity.WeightRange;

import java.util.List;

/**
 * Created by khani on 03/11/2017.
 */

@Dao
public interface BasicInfoDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCountries(List<Country> countries);

    @Query("DELETE FROM Country")
    void deleteCountries();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCities(List<City> cities);

    @Query("DELETE FROM City")
    void deleteCities();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCurrencies(List<Currency> currencies);

    @Query("DELETE FROM Currency")
    void deleteCurrencies();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCarTypes(List<CarType> carTypes);

    @Query("DELETE FROM CarType")
    void deleteCarTypes();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrderStates(List<OrderState> orderStates);

    @Query("DELETE FROM OrderState")
    void deleteOrderStates();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertShipmentTypes(List<ShipmentType> shipmentTypes);

    @Query("DELETE FROM ShipmentType")
    void deleteShipmentTypes();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertWeights(List<WeightRange> weightRanges);

    @Query("DELETE FROM WeightRange")
    void deleteWeights();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPathCurrencies(List<PathCurrency> PathCurrencies);

    @Query("DELETE FROM PathCurrency")
    void deletePathCurrencies();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRatingFactors(List<RatingFactor> ratingFactors);

    @Query("DELETE FROM RatingFactor")
    void deleteRatingFactors();

    @Query("SELECT * FROM Country WHERE registerAllowed Order By name")
    List<Country> getCountries();

    @Query("SELECT * FROM Country WHERE id = :id")
    Country getCountry(int id);

    @Query("SELECT * FROM City")
    List<City> getCities();

    @Query("SELECT * FROM City WHERE id = :id")
    City getCity(int id);

    @Query("SELECT * FROM City WHERE countryId = :id")
    List<City> getCities(int id);

    @Query("SELECT * FROM City WHERE name LIKE '%' || :phrase  || '%' " + "\n" +
            "ORDER BY (CASE WHEN name = :phrase THEN 1 WHEN name LIKE :phrase  || '%' THEN 2 ELSE 3 END),name LIMIT 10")
    List<City> searchCity(String phrase);

    @Query("SELECT * FROM Currency")
    List<Currency> getCurrencies();

    @Query("SELECT * FROM Currency WHERE id = :id")
    Currency getCurrency(int id);

    @Query("SELECT * FROM CarType")
    List<CarType> getCarTypes();

    @Query("SELECT * FROM CarType WHERE id = :id")
    CarType getCarType(int id);

    @Query("SELECT * FROM OrderState")
    List<OrderState> getOrderStates();

    @Query("SELECT * FROM OrderState WHERE code >= :orderState")
    List<OrderState> getOrderStates(int orderState);

    @Query("SELECT * FROM OrderState WHERE code = :id")
    OrderState getOrderState(int id);

    @Query("SELECT * FROM ShipmentType")
    List<ShipmentType> getShipmentTypes();

    @Query("SELECT * FROM ShipmentType WHERE id = :id")
    ShipmentType getShipmentType(int id);

    @Query("SELECT * FROM WeightRange")
    List<WeightRange> getWeightRanges();

    @Query("SELECT * FROM WeightRange WHERE id = :id")
    WeightRange getWeightRange(int id);

    @Query("SELECT * FROM RatingFactor WHERE isPositive = :isPositive")
    List<RatingFactor> getRatingFactors(boolean isPositive);

    @Query("SELECT * FROM RatingFactor WHERE id = :id")
    RatingFactor getRatingFactor(int id);

    @Query("SELECT * FROM PathCurrency")
    List<PathCurrency> getPathCurrencies();

    @Query("SELECT * FROM PathCurrency WHERE sourceCountryId = :sourceCountryId AND targetCountryId = :targetCountryId" )
    List<PathCurrency> getPathCurrencies(int sourceCountryId, int targetCountryId);
}
