package com.tir.user.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.app.AppExecutors;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamPoints;
import com.tir.user.repository.model.api.parameter.ParamRewards;
import com.tir.user.repository.model.api.parameter.ParamSpendPoints;
import com.tir.user.repository.model.api.response.ResponseNull;
import com.tir.user.repository.model.api.response.ResponseOrders;
import com.tir.user.repository.model.api.response.ResponsePoints;
import com.tir.user.repository.model.api.response.ResponseRewards;
import com.tir.user.repository.model.api.response.ResponseUserRewards;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.remot.retrofit.ApiClient;


public class RepositoryPoints extends RepositoryBasic {

    private final MediatorLiveData<NetworkResponse<ResponseOrders>> shipmentsMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Order>> detailsMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<ResponseOrders>> offersMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<ResponseNull>> priceMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<ResponseNull>> acceptMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<ResponseNull>> cancelMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<ResponseNull>> orderStatusMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<ResponseOrders>> shipmentsLive;
    private LiveData<NetworkResponse<Order>> detailsLive;
    private LiveData<NetworkResponse<ResponseOrders>> offersLive;
    private LiveData<NetworkResponse<ResponseNull>> priceLive;
    private LiveData<NetworkResponse<ResponseNull>> acceptLive;
    private LiveData<NetworkResponse<ResponseNull>> cancelLive;
    private LiveData<NetworkResponse<ResponseNull>> orderStatusLive;

    public RepositoryPoints(ApiClient apiClient, AppDatabase appDatabase,
                            AppExecutors appExecutors, SharedPrefsHelper sharedPrefsHelper) {
        super(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    //    public MediatorLiveData<NetworkResponse<ResponseOrders>> getRewards(ParamOrders paramOrders) {
//        if (shipmentsLive != null) {
//            shipmentsMediator.removeSource(shipmentsLive);
//        }
//        shipmentsLive = apiClient.getRewards(paramOrders);
//        shipmentsMediator.addSource(shipmentsLive, shipmentsMediator::setValue);
//        return shipmentsMediator;
//    }
    public LiveData<NetworkResponse<ResponseRewards>> getRewards(ParamRewards paramRewards) {
        return apiClient.getRewards(paramRewards);
    }

    public LiveData<NetworkResponse<ResponseNull>> spendPoints(ParamSpendPoints paramSpendPoints) {
        return apiClient.spendPoints(paramSpendPoints);
    }

    public LiveData<NetworkResponse<ResponseUserRewards>> getUserRewards(ParamRewards paramRewards) {
        return apiClient.getUserRewards(paramRewards);
    }

    public LiveData<NetworkResponse<ResponsePoints>> getPoints(ParamPoints paramPoints) {
        return apiClient.getPointActions(paramPoints);
    }

    public MediatorLiveData<NetworkResponse<Order>> getOrderDetail(int orderId) {
        if (detailsLive != null) {
            detailsMediator.removeSource(detailsLive);
        }
        detailsLive = apiClient.getOrderDetail(orderId);
        detailsMediator.addSource(detailsLive, detailsMediator::setValue);
        return detailsMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponseOrders>> getOffers() {
        if (offersLive != null) {
            offersMediator.removeSource(offersLive);
        }
        offersLive = apiClient.getOffers();
        offersMediator.addSource(offersLive, offersMediator::setValue);
        return offersMediator;
    }
}
