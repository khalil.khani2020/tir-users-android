package com.tir.user.repository.model.room.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by khani on 03/11/2017.
 */

@Entity()
public class PathCurrency {
    @PrimaryKey
    public int id;
    public int sourceCountryId;
    public int targetCountryId;
    public int currencyId;
    public String currencyName;

    @Override
    public String toString() {
        return "PathCurrency{" +
                "id=" + id +
                ", sourceCountryId=" + sourceCountryId +
                ", targetCountryId=" + targetCountryId +
                ", currencyId=" + currencyId +
                ", currencyName='" + currencyName + '\'' +
                '}';
    }
}
