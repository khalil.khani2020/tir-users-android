package com.tir.user.repository.model.api.parameter;

import com.tir.user.repository.model.GeoLoc;

/**
 * Created by khani on 15/02/2018.
 */

public class ParamNewOrder {
    public Integer predecessorId;
    public Integer sourceCityId;
    public Integer targetCityId;
    public Integer carTypeId;
    public Integer shipmentTypeId;
    public Integer weightRangeId;
    public Integer currencyId;
    public String sourceAddress;
    public String targetAddress;
    public String loadingDate;
    public String description;
    public GeoLoc sourceLocation;
    public GeoLoc targetLocation;
    public double forwarderPrice;
}
