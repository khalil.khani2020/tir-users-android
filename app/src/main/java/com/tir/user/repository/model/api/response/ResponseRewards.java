package com.tir.user.repository.model.api.response;

import com.tir.user.repository.model.Reward;

import java.util.ArrayList;


public class ResponseRewards {

    public int totalItems;

    public ArrayList<Reward> items;

    @Override
    public String toString() {
        return "ResponseRewards{" +
                "totalItems=" + totalItems +
                ", items=" + items +
                '}';
    }
}
