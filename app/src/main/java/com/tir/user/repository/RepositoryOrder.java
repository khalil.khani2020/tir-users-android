package com.tir.user.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import android.graphics.Bitmap;

import com.tir.user.app.AppExecutors;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamNewOrder;
import com.tir.user.repository.model.api.parameter.ParamOrderState;
import com.tir.user.repository.model.api.parameter.ParamOrders;
import com.tir.user.repository.model.api.parameter.ParamPathHistory;
import com.tir.user.repository.model.api.parameter.ParamPrice;
import com.tir.user.repository.model.api.parameter.ParamRateDriver;
import com.tir.user.repository.model.api.response.ResponseOrders;
import com.tir.user.repository.model.api.response.ResponsePathHistory;
import com.tir.user.repository.model.room.database.AppDatabase;
import com.tir.user.repository.remot.retrofit.ApiClient;


public class RepositoryOrder extends RepositoryBasic {

    private LiveData<NetworkResponse<ResponseOrders>> shipmentsLive;
    private final MediatorLiveData<NetworkResponse<ResponseOrders>> shipmentsMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<ResponsePathHistory>> pathHistoryMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Order>> submitOrderMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Order>> editOrderMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Order>> recreateOrderMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Order>> detailsMediator = new MediatorLiveData<>();
    private final MediatorLiveData<NetworkResponse<Object>> deleteMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<ResponsePathHistory>> pathHistoryLive;
    private LiveData<NetworkResponse<Order>> submitOrderLive;
    private LiveData<NetworkResponse<Order>> editOrderLive;
    private LiveData<NetworkResponse<Order>> recreateOrderLive;
    private LiveData<NetworkResponse<Order>> detailsLive;
    private LiveData<NetworkResponse<Object>> deleteLive;
    private LiveData<NetworkResponse<ResponseOrders>> offersLive;
    private final MediatorLiveData<NetworkResponse<ResponseOrders>> offersMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<Object>> priceLive;
    private final MediatorLiveData<NetworkResponse<Object>> priceMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<Object>> acceptLive;
    private final MediatorLiveData<NetworkResponse<Object>> acceptMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<Object>> cancelLive;
    private final MediatorLiveData<NetworkResponse<Object>> cancelMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<Object>> orderStatusLive;
    private final MediatorLiveData<NetworkResponse<Object>> orderStatusMediator = new MediatorLiveData<>();

    public RepositoryOrder(ApiClient apiClient, AppDatabase appDatabase,
                           AppExecutors appExecutors, SharedPrefsHelper sharedPrefsHelper) {
        super(apiClient, appDatabase, appExecutors, sharedPrefsHelper);
    }

    //    public MediatorLiveData<NetworkResponse<ResponseOrders>> getOrders(ParamOrders paramOrders) {
//        if (shipmentsLive != null) {
//            shipmentsMediator.removeSource(shipmentsLive);
//        }
//        shipmentsLive = apiClient.getOrders(paramOrders);
//        shipmentsMediator.addSource(shipmentsLive, shipmentsMediator::setValue);
//        return shipmentsMediator;
//    }
    public LiveData<NetworkResponse<ResponseOrders>> getOrders(ParamOrders paramOrders) {
        return apiClient.getOrders(paramOrders);
    }

    public LiveData<NetworkResponse<Object>> rateDriver(ParamRateDriver paramRateDriver) {
        return apiClient.rateDriver(paramRateDriver);
    }


    public MediatorLiveData<NetworkResponse<Order>> getOrderDetail(int orderId) {
        if (detailsLive != null) {
            detailsMediator.removeSource(detailsLive);
        }
        detailsLive = apiClient.getOrderDetail(orderId);
        detailsMediator.addSource(detailsLive, detailsMediator::setValue);
        return detailsMediator;
    }


    public MediatorLiveData<NetworkResponse<Object>> deleteOrder(int orderId) {
        if (deleteLive != null) {
            deleteMediator.removeSource(deleteLive);
        }
        deleteLive = apiClient.deleteOrder(orderId);
        deleteMediator.addSource(deleteLive, deleteMediator::setValue);
        return deleteMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponsePathHistory>> getPathHistory(ParamPathHistory paramPathHistory) {
        if (pathHistoryLive != null) {
            pathHistoryMediator.removeSource(pathHistoryLive);
        }
        pathHistoryLive = apiClient.getPathHistory(paramPathHistory);
        pathHistoryMediator.addSource(pathHistoryLive, pathHistoryMediator::setValue);
        return pathHistoryMediator;
    }

    public MediatorLiveData<NetworkResponse<Order>> submitOrder(ParamNewOrder paramNewOrder) {
        if (submitOrderLive != null) {
            submitOrderMediator.removeSource(submitOrderLive);
        }
        submitOrderLive = apiClient.submitOrder(paramNewOrder);
        submitOrderMediator.addSource(submitOrderLive, submitOrderMediator::setValue);
        return submitOrderMediator;
    }

    public MediatorLiveData<NetworkResponse<Order>> editOrder(int id, ParamNewOrder paramNewOrder) {
        if (editOrderLive != null) {
            editOrderMediator.removeSource(editOrderLive);
        }
        editOrderLive = apiClient.editOrder(id, paramNewOrder);
        editOrderMediator.addSource(editOrderLive, editOrderMediator::setValue);
        return editOrderMediator;
    }

    public MediatorLiveData<NetworkResponse<Order>> recreateOrder(int id) {
        if (recreateOrderLive != null) {
            recreateOrderMediator.removeSource(recreateOrderLive);
        }
        recreateOrderLive = apiClient.recreateOrder(id);
        recreateOrderMediator.addSource(recreateOrderLive, recreateOrderMediator::setValue);
        return recreateOrderMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponseOrders>> getOffers() {
        if (offersLive != null) {
            offersMediator.removeSource(offersLive);
        }
        offersLive = apiClient.getOffers();
        offersMediator.addSource(offersLive, offersMediator::setValue);
        return offersMediator;
    }

    public MediatorLiveData<NetworkResponse<Object>> submitPrice(ParamPrice price) {
        if (priceLive != null) {
            priceMediator.removeSource(priceLive);
        }
        priceLive = apiClient.submitPrice(price);
        priceMediator.addSource(priceLive, priceMediator::setValue);
        return priceMediator;
    }

    public MediatorLiveData<NetworkResponse<Object>> acceptOrder(int id) {
        if (acceptLive != null) {
            acceptMediator.removeSource(acceptLive);
        }
        acceptLive = apiClient.acceptOrder(id);
        acceptMediator.addSource(acceptLive, acceptMediator::setValue);
        return acceptMediator;
    }

    public MediatorLiveData<NetworkResponse<Object>> deleteOffer(int id) {
        if (cancelLive != null) {
            cancelMediator.removeSource(cancelLive);
        }
        cancelLive = apiClient.deleteOffer(id);
        cancelMediator.addSource(cancelLive, cancelMediator::setValue);
        return cancelMediator;
    }

    public MediatorLiveData<NetworkResponse<Object>> submitOrderStatus(String imageType, Bitmap bitmap, ParamOrderState OrderStatus) {
        if (orderStatusLive != null) {
            orderStatusMediator.removeSource(orderStatusLive);
        }
        orderStatusLive = apiClient.submitOrderStatus(imageType, bitmap, OrderStatus);
        orderStatusMediator.addSource(orderStatusLive, orderStatusMediator::setValue);
        return orderStatusMediator;
    }

}
