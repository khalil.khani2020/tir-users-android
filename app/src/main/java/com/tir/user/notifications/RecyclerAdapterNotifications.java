package com.tir.user.notifications;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tir.user.R;
import com.tir.user.databinding.RecyclerItemNotificationBinding;
import com.tir.user.repository.model.Blog;
import com.tir.user.utils.RecyclerViewAnimator;
import com.tir.user.utils.Utils;

import java.util.List;

/**
 * Created by khani on 08/12/2017.
 */

public class RecyclerAdapterNotifications extends RecyclerView.Adapter<RecyclerAdapterNotifications.ViewHolder> {

    private final List<Blog> list;
    private Context mContext;
    private int layout;
    private OnAdapterInteractionsListener mListener;
    private RecyclerViewAnimator mAnimator;
    private boolean animateItems;

    public RecyclerAdapterNotifications(Context context, List<Blog> list, int layout,
                                        OnAdapterInteractionsListener listener, boolean animateItems) {
        this.mContext = context;
        this.list = list;
        this.layout = layout;
        this.mListener = listener;
        this.animateItems = animateItems;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (animateItems)
            mAnimator = new RecyclerViewAnimator(recyclerView, RecyclerViewAnimator.VERTICAL);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemNotificationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layout, parent, false);
        if (animateItems)
            mAnimator.onCreateViewHolder(binding.getRoot());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Blog blog = list.get(position);
        holder.bind(blog);
        if (!Utils.isNullOrEmpty(blog.previewImageUrl)) {
            holder.getBinding().imgPreview.setVisibility(View.VISIBLE);
            Utils.glideImage(mContext, holder.getBinding().imgPreview, blog.previewImageUrl, R.drawable.ic_image_placeholder);
        } else {
            holder.getBinding().imgPreview.setVisibility(View.GONE);
        }
        holder.getBinding().txtDateTime.setText(makeDateTimeString(blog.dateTime));
        if (animateItems)
            mAnimator.onBindViewHolder(holder.getBinding().getRoot(), position);
    }

    private String makeDateTimeString(String date) {
        String[] s = date.split(" - ");
        return "ارسال شده در" + " " + s[0] + " " + "ساعت" + " " + s[1];
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnAdapterInteractionsListener {
        void onRecyclerItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemNotificationBinding binding;

        ViewHolder(RecyclerItemNotificationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.txtSeeBlog.setOnClickListener(v -> mListener.onRecyclerItemClick(getAdapterPosition()));
        }

        void bind(Blog blog) {
            binding.setBlog(blog);
            binding.executePendingBindings();
        }

        RecyclerItemNotificationBinding getBinding() {
            return binding;
        }
    }
}
