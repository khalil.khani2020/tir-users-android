package com.tir.user.notifications;

import android.app.Application;

import com.tir.user.base.BaseViewModel;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryNotification;
import com.tir.user.repository.model.Blog;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseNotifications;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.MediatorLiveData;


public class ViewModelNotification extends BaseViewModel {

    public List<Blog> notifications = new ArrayList<>();
    @Inject
    RepositoryNotification repository;


    public ViewModelNotification(Application application) {
        super(application);
        getViewModelComponent().inject(this);
    }

    @Override
    public RepositoryBasic getBaseRepository() {
        return repository;
    }

    public MediatorLiveData<NetworkResponse<ResponseNotifications>> getNotifications() {
        return repository.getNotifications();
    }

}
