package com.tir.user.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.tir.user.R;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityNotificationsBinding;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseNotifications;
import com.tir.user.webview.ActivityWebView;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

public class ActivityNotification extends BaseActivity
        implements RecyclerAdapterNotifications.OnAdapterInteractionsListener {

    MediatorLiveData<NetworkResponse<ResponseNotifications>> notificationsMediator;
    private ActivityNotificationsBinding binding;
    private ViewModelNotification viewModel;
    private RecyclerAdapterNotifications adapterNotifications;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_notifications, contentFrame, true);
        setSupportActionBar(binding.toolbar);
//        initToolbarAndNavigationDrawer(binding.toolbar, binding.toolbarShadow);
        initToolbar(binding.toolbar, binding.toolbarShadow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ////////////////////////////////////////////////////////////////////////////////////////////
        viewModel = ViewModelProviders.of(this).get(ViewModelNotification.class);
        checkFcmToken(viewModel);
        setupRecycler();
        getNotifications();
        setupSwipeRefreshLayout();
    }

    private void setupRecycler() {
        adapterNotifications = new RecyclerAdapterNotifications(getContext(), viewModel.notifications, R.layout.recycler_item_notification, this, true);
        binding.content.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        binding.content.recyclerView.setAdapter(adapterNotifications);
    }

    private void setupSwipeRefreshLayout() {
        binding.content.swipeRefresh.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorAccent));
        binding.content.swipeRefresh.setOnRefreshListener(
                () -> getNotifications()
        );
    }

    private void getNotifications() {
        if (notificationsMediator == null) {
            notificationsMediator = viewModel.getNotifications();
            notificationsMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.notifications.clear();
                                viewModel.notifications.addAll(networkResponse.getApiResponse().data.items);
                                adapterNotifications.notifyDataSetChanged();
                                showForm();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        } else {
            viewModel.getNotifications();
        }
    }

    private void showProgress() {
        if (binding.content.swipeRefresh.isRefreshing()) {
            binding.content.progressBar.setVisibility(View.GONE);
            binding.content.recyclerView.setVisibility(View.GONE);
        } else {
            binding.content.swipeRefresh.setEnabled(false);
            binding.content.progressBar.setVisibility(View.VISIBLE);
            binding.content.errorLayout.root.setVisibility(View.GONE);
            binding.content.recyclerView.setVisibility(View.GONE);
            binding.content.txtNoOrder.setVisibility(View.GONE);
        }
    }

    private void showForm() {
        binding.content.swipeRefresh.setRefreshing(false);
        binding.content.swipeRefresh.setEnabled(true);
        binding.content.progressBar.setVisibility(View.GONE);
        binding.content.errorLayout.root.setVisibility(View.GONE);
        binding.content.recyclerView.setVisibility(View.VISIBLE);
        if (viewModel.notifications.size() > 0) {
            binding.content.txtNoOrder.setVisibility(View.GONE);
        } else {
            binding.content.txtNoOrder.setVisibility(View.VISIBLE);
        }
    }

    private void showError() {
        binding.content.swipeRefresh.setRefreshing(false);
        binding.content.swipeRefresh.setEnabled(false);
        binding.content.progressBar.setVisibility(View.GONE);
        binding.content.errorLayout.root.setVisibility(View.VISIBLE);
        binding.content.recyclerView.setVisibility(View.GONE);
        binding.content.txtNoOrder.setVisibility(View.GONE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.content.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.content.errorLayout.txtRetry.setOnClickListener(view -> viewModel.getNotifications());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        return this.viewModel;
    }

    @Override
    public void onRecyclerItemClick(int position) {
        Intent intent = new Intent(getContext(), ActivityWebView.class);
        intent.putExtra(ActivityWebView.BLOG, viewModel.notifications.get(position));
        startActivity(intent);
    }
}
