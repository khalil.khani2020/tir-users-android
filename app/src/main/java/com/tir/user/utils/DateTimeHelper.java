package com.tir.user.utils;


import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.chrono.PersianChronology;
import org.joda.time.chrono.PersianChronologyKhayyam;

import java.util.Locale;
//import com.ibm.icu.util.ULocale;

/**
 * Created by khani on 13/02/2018.
 */

public class DateTimeHelper {

//    private static Calendar persianCalendar;
//    private static Calendar calendar;


//    public static void printDate() {
//        int[] date = UtcToDate(System.currentTimeMillis());
//        System.out.println(date[0] + "-" + date[1] + "-" + date[2] + " " + date[3] + ":" + date[4] + ":" + date[5]);
//    }
//
//    public static long persianDateToUtc(int year, int month, int day){
//        if (persianCalendar == null) {
//            persianCalendar = Calendar.getInstance(new ULocale("@calendar=persian"));
//        }
//        persianCalendar.clear();
//        persianCalendar.set(year, month-1, day);
//        return persianCalendar.getTimeInMillis();
//    }


//    public static long persianDateToUtc(int year, int month, int day, int hour, int minute, int second){
//        if (persianCalendar == null) {
//            persianCalendar = Calendar.getInstance(new ULocale("@calendar=persian"));
//        }
//        persianCalendar.clear();
//        persianCalendar.set(year, month-1, day, hour, minute, second);
//        return persianCalendar.getTimeInMillis();
//    }
//
//    public static int[] UtcToPersianDate(Long timeInMills) {
//        if (persianCalendar == null) {
//            persianCalendar = Calendar.getInstance(new ULocale("@calendar=persian"));
//        }
//        persianCalendar.clear();
//        persianCalendar.setTimeInMillis(timeInMills);
//        return new int[]{persianCalendar.get(Calendar.YEAR),
//                persianCalendar.get(Calendar.MONTH) + 1,
//                persianCalendar.get(Calendar.DAY_OF_MONTH),
//                persianCalendar.get(Calendar.HOUR_OF_DAY),
//                persianCalendar.get(Calendar.MINUTE),
//                persianCalendar.get(Calendar.SECOND)};
//    }

    public static int[] parseDateToInt(String dateTime) {
//        String s = dateTime.split("T")[0];
        String s[] = dateTime.split("/");
        return new int[]{Integer.valueOf(s[0]), Integer.valueOf(s[1]), Integer.valueOf(s[2])};
    }

    public static int[] getToday() {
//        int[] date = parseDate(dateTime);
        PersianChronology perChr = null;
        if (Locale.getDefault().getLanguage().equals("fa")) {
            perChr = PersianChronologyKhayyam.getInstance();
        }

        DateTime now;
        if (perChr != null) {
            now = new DateTime(perChr);
            return parseDate(now.toString());
        } else {
            now = new DateTime();
            return parseDate(now.toString());
        }
    }

    public static boolean isValid(int year, int month, int day) {
//        int[] date = parseDate(dateTime);
        PersianChronology perChr = null;
        if (Locale.getDefault().getLanguage().equals("fa")) {
            perChr = PersianChronologyKhayyam.getInstance();
        }
        DateTime now;
        if (perChr != null) {
            now = new DateTime(perChr);
        } else {
            now = new DateTime();
        }
        try {
            long millis = now.withYear(year).withMonthOfYear(month).withDayOfMonth(day).getMillis();
            return true;
        } catch (IllegalFieldValueException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isExpired(String date) {
        int[] d = DateTimeHelper.parseDateToInt(date);
        int[] now = getToday();
        if (d[0] < now[0])
            return true;
        else if (d[0] == now[0] && d[1] < now[1])
            return true;
        else if (d[0] == now[0] && d[1] == now[1] && d[2] < now[2])
            return true;
        return false;
    }


    private static int[] parseDate(String dateTime) {
        String s = dateTime.split("T")[0];
        String s1[] = s.split("-");
        return new int[]{Integer.valueOf(s1[0]), Integer.valueOf(s1[1]), Integer.valueOf(s1[2])};
    }


//    public static int[] UtcToDate(long timeInMills) {
//        if (calendar == null) {
//            calendar = Calendar.getInstance();
//        }
//        System.out.println(java.util.TimeZone.getDefault().getID());
//        System.out.println(Locale.getDefault());
//        calendar.clear();
//        calendar.setTimeInMillis(timeInMills - calendar.getTimeZone().getRawOffset());
//        return new int[]{calendar.get(Calendar.YEAR),
//                Locale.getDefault().getLanguage().equals("fa") ? calendar.get(Calendar.MONTH) + 1 : calendar.get(Calendar.MONTH),
//                calendar.get(Calendar.DAY_OF_MONTH),
//                calendar.get(Calendar.HOUR_OF_DAY),
//                calendar.get(Calendar.MINUTE),
//                calendar.get(Calendar.SECOND)};
//    }
//
//    public static long DateToUtc(int year, int month, int day) {
//        if (calendar == null) {
//            calendar = Calendar.getInstance();
//        }
//        calendar.clear();
//        if (Locale.getDefault().getLanguage().equals("fa")) {
//            month = month - 1;
//        }
//        calendar.set(year, month, day);
//        calendar.setTimeInMillis(calendar.getTimeInMillis() + calendar.getTimeZone().getRawOffset());
//        System.out.println(calendar.getTimeZone().getRawOffset());
////        System.out.println(UtcToDate(calendar.getTimeInMillis())[0]);
////        System.out.println(UtcToDate(calendar.getTimeInMillis())[1]);
////        System.out.println(UtcToDate(calendar.getTimeInMillis())[2]);
//        return calendar.getTimeInMillis();
//    }
//
//    public static String getFormattedDate(long timeStamp) {
//        int[] date = UtcToDate(timeStamp);
//        return date[0] + "/" + date[1] + "/" + date[2];
//    }
//
//    public static void persianDate() {
//
//        PersianChronology perChr = PersianChronologyKhayyam.getInstance();
//        DateTime now;
//        try {
////            now = new DateTime(1396, 8, 31, 2, 40, perChr);
//            now = new DateTime(2018, 8, 30, 2, 40);
//            System.out.println("Calendar: " + now);
//            System.out.println("Calendar: " + now.toDateTimeISO());
//        } catch (IllegalFieldValueException e) {
//            System.out.println("Calendar: Invalid getDate");
//            e.printStackTrace();
//        }
//
////        DateTime now1 = new DateTime(DateTimeZone.UTC);
//        DateTime now1 = new DateTime();
//
//        System.out.println("Calendar: " + now1);
//        System.out.println("Calendar: " + now1.toDateTime(perChr));
//        System.out.println("Calendar: " + now1.toLocalDateTime());
//
//
////        DateTimeFormatter dateFormat = DateTimeFormat
////                .forPattern("G,C,Y,x,w,e,E,Y,D,M,d,a,K,h,H,k,m,s,S,z,Z");
////
////        String dob = "2002-01-15";
////        LocalTime localTime = new LocalTime();
////        LocalDate localDate = new LocalDate();
////        DateTime dateTime = new DateTime();
////        LocalDateTime localDateTime = new LocalDateTime();
////        DateTimeZone dateTimeZone = DateTimeZone.getDefault();
////
////        System.out.println("Calendar dateFormatr : " + dateFormat.print(localDateTime));
////        System.out.println("Calendar LocalTime : " + localTime.toString());
////        System.out.println("Calendar localDate : " + localDate.toString());
////        System.out.println("Calendar dateTime : " + dateTime.toString());
////        System.out.println("Calendar localDateTime : " + localDateTime.toString());
////        System.out.println("Calendar DateTimeZone : " + dateTimeZone.toString());
////        System.out.println("Calendar Year Difference : " + Years.yearsBetween(DateTime.parse(dob), dateTime).getYears());
////        System.out.println("Calendar Month Difference : " + Months.monthsBetween(DateTime.parse(dob), dateTime).getMonths());
//    }
//
//    public static String getDate(String dateTime) {
//        int[] date = parseDate(dateTime);
//        PersianChronology perChr = null;
//        if (Locale.getDefault().getLanguage().equals("fa")) {
//            perChr = PersianChronologyKhayyam.getInstance();
//        }
//        DateTime now;
//        try {
//            now = new DateTime(date[0], date[1], date[2], 1, 0);
//            if (perChr != null) {
//                return getFormattedDate(now.toDateTime(perChr).toString());
//            } else {
//                return getFormattedDate(now.toDateTimeISO().toString());
//            }
//        } catch (IllegalFieldValueException e) {
//            System.out.println("Calendar: Invalid getDate");
//            e.printStackTrace();
//            return "0000/00/00";
//        } catch (Exception e) {
//            System.out.println("calendar: " + dateTime);
//            System.out.println("calendar: " + date[0] + " " + date[1] + " " + date[2]);
//            e.printStackTrace();
//            return "0000/00/00";
//        }
//    }
//
//    public static int[] getIntDate(String dateTime) {
//        int[] date = parseDate(dateTime);
//        PersianChronology perChr = null;
//        if (Locale.getDefault().getLanguage().equals("fa")) {
//            perChr = PersianChronologyKhayyam.getInstance();
//        }
//        DateTime now;
//        try {
//            now = new DateTime(date[0], date[1], date[2], 0, 0);
//            if (perChr != null) {
//                return parseDate(getFormattedDate(now.toDateTime(perChr).toString()));
//            } else {
//                return parseDate(getFormattedDate(now.toDateTimeISO().toString()));
//            }
//        } catch (IllegalFieldValueException e) {
//            System.out.println("Calendar: Invalid getDate");
//            e.printStackTrace();
//            return new int[]{0};
//        }
//    }
//
//    public static String setDate(int year, int month, int day) {
//
////        getFormattedDate("2018-03-07 00:00:00");
//
//        PersianChronology perChr = null;
//        if (Locale.getDefault().getLanguage().equals("fa")) {
//            perChr = PersianChronologyKhayyam.getInstance();
//        }
//        DateTime now;
//        try {
//            if (perChr != null) {
//                now = new DateTime(year, month, day, 0, 0, perChr);
//            } else {
//                now = new DateTime(year, month, day, 0, 0);
//            }
//            return getFormattedDate(now.toDateTimeISO().toString());
//        } catch (IllegalFieldValueException e) {
//            System.out.println("Calendar: Invalid getDate");
//            e.printStackTrace();
//            return "0000-00-00";
//        }
//
////        DateTime now1 = new DateTime(DateTimeZone.UTC);
////        DateTime now1 = new DateTime();
////
////        System.out.println("Calendar: " + now1);
////        if (perChr != null) {
////            System.out.println("Calendar: " + now1.toDateTime(perChr));
////        } else {
////            System.out.println("Calendar: " + now1.toLocalDateTime());
////        }
//
////        DateTimeFormatter dateFormat = DateTimeFormat
////                .forPattern("G,C,Y,x,w,e,E,Y,D,M,d,a,K,h,H,k,m,s,S,z,Z");
////
////        String dob = "2002-01-15";
////        LocalTime localTime = new LocalTime();
////        LocalDate localDate = new LocalDate();
////        DateTime dateTime = new DateTime();
////        LocalDateTime localDateTime = new LocalDateTime();
////        DateTimeZone dateTimeZone = DateTimeZone.getDefault();
////
////        System.out.println("Calendar dateFormatr : " + dateFormat.print(localDateTime));
////        System.out.println("Calendar LocalTime : " + localTime.toString());
////        System.out.println("Calendar localDate : " + localDate.toString());
////        System.out.println("Calendar dateTime : " + dateTime.toString());
////        System.out.println("Calendar localDateTime : " + localDateTime.toString());
////        System.out.println("Calendar DateTimeZone : " + dateTimeZone.toString());
////        System.out.println("Calendar Year Difference : " + Years.yearsBetween(DateTime.parse(dob), dateTime).getYears());
////        System.out.println("Calendar Month Difference : " + Months.monthsBetween(DateTime.parse(dob), dateTime).getMonths());
//    }
//

}
