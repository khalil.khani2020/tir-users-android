package com.tir.user.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.model.LatLng;
import com.tir.user.R;
import com.tir.user.app.MyApp;
import com.tir.user.repository.model.GeoLoc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;


public class Utils {


    private static RequestOptions requestOptionsRounded;
    private static RequestOptions requestOptionsCircular;
    private static RequestOptions requestOptionsWithPlaceHolder;
    private static RequestOptions requestOptions;
    private static final String[] pr = {"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};
    private static final String[] en = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};


    public Utils() {
    }

    public static String fixString(String s) {
        return s.replace("\\r\\n", " ")
                .replace("  ", " ")
                .replace("&nbsp;", " ")
                .replace("  ", " ")
                .replace("&zwnj;", " ")
                .replace("  ", " ");
    }

    public static String getMD5Hash(String s) throws Exception {

        // Create MD5 Hash
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update(s.getBytes(StandardCharsets.UTF_8));
        byte[] messageDigest = digest.digest();
        StringBuilder MD5Hash = new StringBuilder();
        for (byte aMessageDigest : messageDigest) {
            StringBuilder h = new StringBuilder(Integer.toHexString(0xFF & aMessageDigest));
            while (h.length() < 2) h.insert(0, "0");
            MD5Hash.append(h);
        }
        return MD5Hash.toString();
    }

    public static String formatPrice(long price) {
        return String.format("%,d", price).replace(',', ' ');
    }

    public static String formatPrice(double price) {

        return String.format("%,d", (long) price).replace(',', ' ');
    }

    public static String extractDigits(String s) {
        return s.replaceAll("[^\\d]", "");
    }

    public static String toEnglishDigits(String number) {
        for (int i = 0; i < 10; i++) {
            number = number.replace(pr[i], en[i]);
        }
        return number;
    }

    public static void glideCircularImage(Context context, ImageView imageView, String url, int placeholder) {
        if (requestOptionsCircular == null) {
            requestOptionsCircular = new RequestOptions()
                    .fitCenter()
                    .transform(new CircleTransform());
        }
        requestOptionsCircular.placeholder(placeholder);
        Glide.with(context)
                .load(url)
                .transition(withCrossFade(400))
                .apply(requestOptionsCircular)
                .into(imageView);
    }

    public static void glideImage(Context context, ImageView imageView, String url, int placeholder) {
        if (requestOptionsWithPlaceHolder == null) {
            requestOptionsWithPlaceHolder = new RequestOptions()
                    .fitCenter()
                    .dontAnimate();
        }
//        requestOptionsWithPlaceHolder.placeholder(placeholder);
        Glide.with(context)
                .load(url)
                .transition(withCrossFade(400))
                .apply(requestOptionsWithPlaceHolder)
                .into(imageView);
    }

    public static void glideImage(Context context, ImageView imageView, String url) {
        if (requestOptions == null) {
            requestOptions = new RequestOptions()
                    .fitCenter()
                    .dontAnimate();
        }
        Glide.with(context)
                .load(url)
                .transition(withCrossFade(400))
                .apply(requestOptions)
                .into(imageView);
    }

    public static void glideRoundedCornerImage(Context context, ImageView imageView, String url, int placeholder) {
        if (requestOptionsRounded == null) {
            requestOptionsRounded = new RequestOptions()
                    .transforms(new CenterCrop(), new RoundedCorners(8));
        }
//        requestOptionsRounded.placeholder(placeholder);
        Glide.with(context)
                .load(url)
                .transition(withCrossFade(400))
                .apply(requestOptionsRounded)
                .into(imageView);
    }

    public static byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();
        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }
        return byteBuff.toByteArray();
    }

    public static String getDeviceInfo() {
        return "OS:Android" +
                "##OS_Version:" + Build.VERSION.RELEASE +
                "##Brand:" + Build.BRAND +
                "##Model:" + Build.MODEL +
                "##Serial:" + Build.SERIAL;
    }

    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static boolean isGPSEnabled() {
        LocationManager lm = (LocationManager) MyApp.getInstance().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        // TODO: 4/27/2018 check this snippet again
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return !(!gps_enabled && !network_enabled);
    }

    public static void hideKeyboard(Activity activity) {
        try {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFileNameFromUrl(String url) {
        try {
            url = url.replace("http://", "");
            url = url.replace("//", "/");
            String[] s = url.split("/");
            return s[s.length - 1];
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static boolean isNullOrEmpty(Double d) {
        return d == null || d == 0;
    }

    public static boolean isValidLocation(GeoLoc loc) {
        return loc != null && loc.latitude != 0f && loc.longitude != 0f;
    }

    public static boolean isValidLocation(LatLng loc) {
        return loc != null && loc.latitude != 0f && loc.longitude != 0f;
    }

    public static void enableEnableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            for (int idx = 0; idx < group.getChildCount(); idx++)
                enableEnableView(group.getChildAt(idx), enabled);
        }
    }

    public static void fadeView(Context mContext, final View view, final Boolean fadeOut) {
        Animation anim;
        view.setVisibility(View.VISIBLE);
        if (fadeOut) {
            //fadeOut
            anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
        } else {
            //fadeIn
            anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        }
        view.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                //Functionality here
                if (fadeOut) {
                    view.setVisibility(View.GONE);
                } else {
                    view.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

}
