package com.tir.user.utils;

import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by khani on 18/11/2017.
 */

public class Validations {

    //    private static UrlValidator urlValidator;

    private static final String persian = "[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤآإأءًٌٍَُِّ\\s]+$";
    private static final String noneSpecial = "^[^~!@#$%^&|*()_\\-+=1234567890،؟۰۱۲۳۴۵۶۷۸۹?,:;'\"/\\\\<>{}\\[\\]]+$";
    private static final String mobilePhone = "^09(0[1-3]|1[0-9]|3[0-9]|2[0-2]|9[0])-?[0-9]{3}-?[0-9]{4}$";
    private static final String phone = "";
    private static final String instagramUser = "@[A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\\.(?!\\.))){0,28}(?:[A-Za-z0-9_]))?";
    private static final String telegramUser = "@([a-z0-9_]{5,32})/?";
    private static final String url = "^(http://www\\.|https://www\\.|http://|https://)?[a-z0-9]+([\\-.][a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$";


    public static boolean isValidPersian(String input) {
        Pattern p = Pattern.compile(persian);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public static boolean isValidName(String input) {
        Pattern p = Pattern.compile(noneSpecial);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public static boolean isValidEmail(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidUrl(String input) {
//        if (urlValidator == null) {
//            urlValidator = new UrlValidator();
//        }
//        return urlValidator.isValid(input);
        Pattern p = Pattern.compile(url);
        Matcher m = p.matcher(input);
        return m.matches();
//        return Patterns.WEB_URL.matcher(input).matches() && URLUtil.isValidUrl(input);
    }

    public static boolean isValidMobilePhone(String input) {
        // TODO: add phone regex and validate
        Pattern p = Pattern.compile(mobilePhone);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public static boolean isValidPhone(String input) {
        Pattern p = Pattern.compile(phone);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public static boolean isValidInstagramUser(String input) {
        Pattern p = Pattern.compile(instagramUser);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    public static boolean isValidTelegramUser(String input) {
        Pattern p = Pattern.compile(telegramUser);
        Matcher m = p.matcher(input);
        return m.matches();
    }


}
