package com.tir.user.utils;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;

 public class RecyclerViewAnimator {

    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;

    /**
     * Initial delay before to show pathHistory - in ms
     */
    private static final int INIT_DELAY = 700;

    /**
     * Initial entrance tension parameter.
     * See https://facebook.github.io/rebound/
     */
    private static final int INIT_TENSION = 300;
    /**
     * Initial entrance friction parameter.
     */
    private static final int INIT_FRICTION = 28;

    /**
     * Scroll entrance animation tension parameter.
     */
    private static final int SCROLL_TENSION = 300;
    /**
     * Scroll entrance animation friction parameter.
     */
    private static final int SCROLL_FRICTION = 28;


     private final int mMeasure;
     private final RecyclerView mRecyclerView;
     private final SpringSystem mSpringSystem;

    private boolean mFirstViewInit = true;
    private int mLastPosition = -1;
    private int mStartDelay;
    private boolean isVertical;


    public RecyclerViewAnimator(RecyclerView recyclerView, int orientation) {
        mRecyclerView = recyclerView;
        mSpringSystem = SpringSystem.create();
        // Use height of RecyclerView to slide-in pathHistory from bottom.
        if (orientation == LinearLayoutManager.VERTICAL) {
            isVertical = true;
            mMeasure = mRecyclerView.getResources().getDisplayMetrics().heightPixels;
        } else {
            mMeasure = mRecyclerView.getResources().getDisplayMetrics().widthPixels;
        }
        mStartDelay = INIT_DELAY;
    }


    public void onCreateViewHolder(View item) {
        if (mFirstViewInit) {
            slideInBottom(item, mStartDelay, INIT_TENSION, INIT_FRICTION);
            mStartDelay += 100;
        }
    }


    public void onBindViewHolder(View item, int position) {
        /*
          After init, animate once item by item when user scroll down.
         */
        if (!mFirstViewInit && position > mLastPosition) {
            slideInBottom(item, 70, SCROLL_TENSION, SCROLL_FRICTION);
            mLastPosition = position;
        }
    }


    private void slideInBottom(final View item,
                               final int delay,
                               final int tension,
                               final int friction) {
        // Move item far outside the RecyclerView
        if (isVertical) {
            item.setTranslationY(mMeasure);
        } else {
            item.setTranslationX(mMeasure);
        }
        Runnable startAnimation = () -> {
            SpringConfig config = new SpringConfig(tension, friction);
            Spring spring = mSpringSystem.createSpring();
            spring.setSpringConfig(config);
            spring.addListener(new SimpleSpringListener() {
                @Override
                public void onSpringUpdate(Spring spring) {
                    /*
                      Decrease translationY until 0.
                     */
                    if (isVertical) {
                        float val = (float) (mMeasure - spring.getCurrentValue());
                        item.setTranslationY(val);
                    } else {
                        float val = (float) (spring.getCurrentValue() - mMeasure);
                        item.setTranslationX(val);
                    }
                }


                @Override
                public void onSpringEndStateChange(Spring spring) {
                    mFirstViewInit = false;
                }
            });
            // Set the spring in motion; moving from 0 to height
            spring.setEndValue(mMeasure);
        };
        mRecyclerView.postDelayed(startAnimation, delay);
    }
 }
