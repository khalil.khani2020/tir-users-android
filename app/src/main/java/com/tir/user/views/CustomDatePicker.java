package com.tir.user.views;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.Toast;

import com.tir.user.R;
import com.tir.user.databinding.DialogDatePickerBinding;
import com.tir.user.utils.DateTimeHelper;

import static com.tir.user.utils.Utils.isNullOrEmpty;

/**
 * Created by khani on 03/03/2018.
 */

public class CustomDatePicker {

    private final Context mContext;
    private DialogDatePickerBinding binding;
    private Dialog mDialog;
    private final OnDateTimeSetListener mListener;
    private int requestCode;
    private int minYearDifference = 5;
    private int maxYearDifference = 5;


    public CustomDatePicker(Context context, OnDateTimeSetListener listener) {
        this.mContext = context;
        this.mListener = listener;
        initDialog();
    }

    private void initDialog() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(mContext),
                R.layout.dialog_date_picker, null, false);
        mDialog = new Dialog(mContext);
//        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(binding.getRoot());
        binding.txtSubmit.setOnClickListener(v -> {
//            long timeStamp = getTimeStamp();
            if (isValidDate()) {
                String formattedDate = getNPDate();
                mListener.onDateSet(formattedDate, 0, requestCode);
                mDialog.dismiss();
            } else {
                Toast.makeText(mContext, R.string.invalid_date, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showDatePicker(String dateTime, int requestCode) {
        this.requestCode = requestCode;
        initDate(dateTime);
        mDialog.show();
    }

    public void setMinMaxYears(int min, int max) {
        this.minYearDifference = min;
        this.minYearDifference = max;
    }

    private void initDate(String dateTime) {
        int[] today = DateTimeHelper.getToday();
        binding.npDay.setMinValue(1);
        binding.npDay.setMaxValue(31);
        binding.npMonth.setMinValue(1);
        binding.npMonth.setMaxValue(12);
        binding.npYear.setMinValue(today[0] - minYearDifference);
        binding.npYear.setMaxValue(today[0] + maxYearDifference);

        if (!isNullOrEmpty(dateTime)) {
            int[] date = DateTimeHelper.parseDateToInt(dateTime);
            binding.npYear.setValue(date[0]);
            binding.npMonth.setValue(date[1]);
            binding.npDay.setValue(date[2]);
        } else {
            binding.npYear.setValue(today[0]);
            binding.npMonth.setValue(today[1]);
            binding.npDay.setValue(today[2]);
        }
    }

//    private long getTimeStamp() {
//        return DateTimeHelper.DateToUtc(
//                binding.npYear.getValue(),
//                binding.npMonth.getValue(),
//                binding.npDay.getValue());
//    }

    private boolean isValidDate() {
        return DateTimeHelper.isValid(
                binding.npYear.getValue(),
                binding.npMonth.getValue(),
                binding.npDay.getValue());
    }

    private String getNPDate() {
        String date =
                String.valueOf(binding.npYear.getValue()) + '/' +
                String.valueOf(binding.npMonth.getValue()) + '/' +
                        String.valueOf(binding.npDay.getValue());
        return date;
    }

    public interface OnDateTimeSetListener{
        void onDateSet(String formattedDate, long timeStamp, int requestCode);
    }
}
