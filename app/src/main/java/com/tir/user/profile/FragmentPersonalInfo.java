package com.tir.user.profile;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tir.user.R;
import com.tir.user.app.MyApp;
import com.tir.user.app.StaticVariables;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentPersonalInfoBinding;
import com.tir.user.repository.model.PersonalInfo;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseImageUpload;
import com.tir.user.utils.Utils;

import static com.tir.user.utils.Utils.isNullOrEmpty;


public class FragmentPersonalInfo extends BaseFragment {

    private static final String IMAGE_TYPE_PROFILE = "Profile";

    private FragmentPersonalInfoBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelProfile viewModel;
    private FormValidatorPersonalInfo formValidator;
    private Uri profileImageUri;
    MediatorLiveData<NetworkResponse<PersonalInfo>> profileMediator;
    MutableLiveData<NetworkResponse<ResponseImageUpload>> uploadImageMediator;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_personal_info, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelProfile.class);

        setupForm();
        getProfileInfo();
        return binding.getRoot();
    }

    private void setupForm() {
//        binding.scrollView.post(() -> binding.scrollView.scrollTo(0, 0));
        formValidator = new FormValidatorPersonalInfo(getContext(), binding);
        setupListeners();
    }

    private void getProfileInfo() {
        if (profileMediator == null) {
            profileMediator = viewModel.getProfile();
            profileMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.setPersonalInfo(networkResponse.getApiResponse().data);
                                bindData(viewModel.getPersonalInfo());
                                showForm();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        } else {
            viewModel.getProfile();
        }
    }

    private void setupListeners() {
        binding.imgProfile.setOnClickListener(view -> {
            CropImage.startPickImageActivity(getContext(), getFragment());
//            chooseImageSource();
        });
        binding.imgRetry.setOnClickListener(view -> {
            viewModel.uploadImage(IMAGE_TYPE_PROFILE, profileImageUri);
        });
        binding.imgRetry.setEnabled(false);
    }

    private void bindData(PersonalInfo personalInfo) {
        binding.setPersonalInfo(personalInfo);
        if (!isNullOrEmpty(personalInfo.profileImageUrl)) {
            Utils.glideCircularImage(getContext(), binding.imgProfile,
                    personalInfo.profileImageUrl, R.drawable.ic_placeholder_profile);
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setAllowFlipping(false)
                .setCropMenuCropButtonIcon(R.drawable.ic_check_white)
                .setCropMenuCropButtonTitle("")
                .setScaleType(CropImageView.ScaleType.FIT_CENTER)
                .setFixAspectRatio(true)
                .setOutputCompressQuality(70)
                .setRequestedSize(640, 640)
                .start(getContext(), getFragment());
    }

    private PersonalInfo collectFormData() {
        PersonalInfo p = new PersonalInfo();
        p.firstName = binding.etxtFirstName.getText().toString().trim();
        p.lastName = binding.etxtLastName.getText().toString().trim();
        p.company = binding.etxtCompany.getText().toString().trim();
        p.email = binding.etxtEmail.getText().toString().trim();
        p.phoneNumber = binding.etxtPhone.getText().toString().trim();

        if (p.equals(viewModel.getPersonalInfo())) {
            return null;
        } else {
            viewModel.setPersonalInfo(p);
            return viewModel.getPersonalInfo();
        }
    }

    private void showProgress() {
        mListener.showSubmitBtn(false);
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.constraintMain.setVisibility(View.GONE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showForm() {
        mListener.showSubmitBtn(true);
        binding.constraintMain.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
        binding.errorLayout.root.setVisibility(View.GONE);
    }

    private void showError() {
        mListener.showSubmitBtn(false);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
        binding.constraintMain.setVisibility(View.GONE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> {
                if (viewModel.getPersonalInfo() == null) {
                    viewModel.getProfile();
                } else {
                    viewModel.updateProfile();
                }
            });
        }
    }

    public void confirmForm() {
        if (validate()) {
            if (collectFormData() != null) {
                viewModel.updateProfile().observe(this, networkResponse -> {
                    switch (networkResponse.getStatus()) {
                        case LOADING:
                            showProgress();
                            break;
                        case FAILURE:
                            showError();
                            break;
                        case SUCCESS:
                            switch (networkResponse.getApiResponse().status.code) {
                                case ApiResponse.COMMON_SUCCESSFUL:
                                    StaticVariables.SESSION.isProfileCompleted = true;
                                    StaticVariables.SESSION.lastName = viewModel.getPersonalInfo().lastName;
                                    StaticVariables.SESSION.firstName = viewModel.getPersonalInfo().firstName;
                                    mListener.onContinueToApp();
                                    break;
                                case ApiResponse.COMMON_FAILURE:
                                    showError();
                                    break;
                                default:
                                    showMessage(networkResponse.getApiResponse().status.message);
                                    showForm();
                                    break;
                            }
                            break;
                        default:
                            showError();
                            break;
                    }
                });
            } else {
                mListener.onContinueToApp();
            }
        }
    }

    private boolean validate() {
//        if (isNullOrEmpty(viewModel.getPersonalInfo().profileImageUrl)) {
//            Toast.makeText(getContext(), R.string.choose_profile_image, Toast.LENGTH_SHORT).show();
//            return false;
//        }
        return formValidator.validate();
    }


//    public boolean isChanged() {
//        return collectFormData() != null;
//    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyApp.getInstance().setLanguage();
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);
            if (CropImage.isReadExternalStoragePermissionsRequired(getContext(), imageUri)) {
                // TODO: check and test this segment;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (result != null) {
                profileImageUri = result.getUri();
                Utils.glideCircularImage(getContext(), binding.imgProfile,
                        result.getUri().toString(), R.drawable.ic_placeholder_profile);
                uploadImage();
            }
        }
    }

    private void uploadImage() {
        if (uploadImageMediator != null) {
            uploadImageMediator.removeObservers(this);
            uploadImageMediator = null;
        }
        uploadImageMediator = viewModel.uploadImage(IMAGE_TYPE_PROFILE, profileImageUri);
        uploadImageMediator.observe(this, networkResponse -> {
            switch (networkResponse.getStatus()) {
                case LOADING:
                    showUploadProgress();
                    break;
                case FAILURE:
                    showRetryImageUpload();
                    Toast.makeText(getContext(), R.string.upload_failed, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS:
                    switch (networkResponse.getApiResponse().status.code) {
                        case ApiResponse.COMMON_SUCCESSFUL:
                            showImage();
                                viewModel.saveProfileUrl(networkResponse.getApiResponse().data.fileUrl);
                            break;
                        case ApiResponse.COMMON_FAILURE:
                            showRetryImageUpload();
                            break;
                        default:
                            showRetryImageUpload();
                            break;
                    }
                    break;
                default:
                    showRetryImageUpload();
                    break;
            }
        });
    }

    private void showUploadProgress() {
            showProfileImageUploadProgress();
    }

    private void showImage() {
            showProfileImage();
    }

    private void showRetryImageUpload() {
            showRetryProfileImageUpload();
    }

    private void showProfileImageUploadProgress() {
        binding.imgBlur.setVisibility(View.VISIBLE);
        binding.imgRetry.setVisibility(View.INVISIBLE);
        binding.dotsProgressBar.show();
        binding.imgProfile.setEnabled(false);
        binding.imgRetry.setEnabled(false);
    }

    private void showProfileImage() {
        binding.imgBlur.setVisibility(View.INVISIBLE);
        binding.dotsProgressBar.hide();
        binding.imgRetry.setVisibility(View.INVISIBLE);
        binding.imgProfile.setEnabled(true);
    }

    private void showRetryProfileImageUpload() {
        binding.imgBlur.setVisibility(View.VISIBLE);
        binding.dotsProgressBar.hide();
        binding.imgRetry.setVisibility(View.VISIBLE);
        binding.imgRetry.setEnabled(true);
        binding.imgProfile.setEnabled(false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void showSubmitBtn(boolean show);
        void onContinueToApp();
    }

}
