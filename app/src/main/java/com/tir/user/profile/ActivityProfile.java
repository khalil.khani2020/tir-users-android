package com.tir.user.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.tir.user.R;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityProfileBinding;
import com.tir.user.home.ActivityHome;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

public class ActivityProfile extends BaseActivity
        implements FragmentPersonalInfo.OnFragmentInteractionListener {

    private MenuItem menuItem;
    private ViewModelProfile viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityProfileBinding binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_profile, contentFrame, true);
        setSupportActionBar(binding.toolbar);
//        initToolbarAndNavigationDrawer(binding.toolbar, binding.toolbarShadow);
        initToolbar(binding.toolbar, binding.toolbarShadow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ////////////////////////////////////////////////////////////////////////////////////////////
        viewModel = ViewModelProviders.of(this).get(ViewModelProfile.class);
        checkFcmToken(viewModel);
        setupMainFrag();
    }

    private void setupMainFrag() {
        FragmentPersonalInfo mainFrag = new FragmentPersonalInfo();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mainFrag, mainFrag.getClass().getSimpleName())
                .addToBackStack(mainFrag.getClass().getSimpleName())
                .commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private String getLastFragName() {
        return getSupportFragmentManager()
                .getBackStackEntryAt(getSupportFragmentManager()
                        .getBackStackEntryCount() - 1).getName();
    }

    @Override
    public void showSubmitBtn(boolean show) {
        if (menuItem != null) {
            menuItem.setVisible(show);
        }
    }

    @Override
    public void onContinueToApp() {
        startActivity(new Intent(getContext(), ActivityHome.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_profile, menu);
        menuItem = menu.findItem(R.id.confirm);
        menuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String tag = getLastFragName();
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.confirm:
                if (tag.equals(FragmentPersonalInfo.class.getSimpleName())) {
                    ((FragmentPersonalInfo) getSupportFragmentManager().findFragmentByTag(tag)).confirmForm();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        return this.viewModel;
    }

    @Override
    public void onBackPressed() {
        if (closeDrawer()) {
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }
}
