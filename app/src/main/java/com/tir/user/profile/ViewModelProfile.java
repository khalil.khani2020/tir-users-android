package com.tir.user.profile;

import android.app.Application;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import android.net.Uri;

import com.tir.user.base.BaseViewModel;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryProfile;
import com.tir.user.repository.model.PersonalInfo;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseImageUpload;

import javax.inject.Inject;


public class ViewModelProfile extends BaseViewModel {

    @Inject
    RepositoryProfile repository;
    private PersonalInfo personalInfo;


    public ViewModelProfile(Application application) {
        super(application);
        getViewModelComponent().inject(this);
    }

    @Override
    public RepositoryBasic getBaseRepository() {
        return repository;
    }

    public MediatorLiveData<NetworkResponse<PersonalInfo>> getProfile() {
        return repository.getProfile();
    }

    public MediatorLiveData<NetworkResponse<Object>> updateProfile() {
        return repository.updateProfile(personalInfo);
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public MutableLiveData<NetworkResponse<ResponseImageUpload>> uploadImage(String imageType, Uri imageUri) {
        return repository.uploadImage(imageType, imageUri);
    }

    public void saveProfileUrl(String url) {
        personalInfo.profileImageUrl = url;
        repository.saveProfileImageUrl(url);
    }
}
