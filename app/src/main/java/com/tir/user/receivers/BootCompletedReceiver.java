package com.tir.user.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            // TODO: 5/6/2018 check login information and enable running service on device boot
//            Intent serviceIntent = new Intent(context, ServiceUpdateLocation.class);
//            context.startService(serviceIntent);
        }
    }

}
