package com.tir.user.neworder.detail;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;
import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentLocationBinding;
import com.tir.user.home.ActivityHome;
import com.tir.user.neworder.ViewModelOrder;
import com.tir.user.neworder.history.FragmentHistory;
import com.tir.user.repository.model.GeoLoc;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.utils.Utils;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;

import static com.tir.user.utils.Utils.isValidLocation;


public class FragmentLocation extends BaseFragment {

    private static final int overview = 0;
    private final int zoomLevel = 14;
    protected FragmentLocationBinding binding;
    protected GoogleMap googleMap;
    //    protected LatLng sourceLatLng;
//    protected LatLng targetLatLng;
    protected CameraPosition sourceCameraPosition;
    protected CameraPosition targetCameraPosition;
    protected boolean isMapReady;
    protected Bundle mSavedInstanceState;
    private ViewModelOrder viewModel;
    private OnFragmentInteractionListener mListener;
    private Marker sourceMarker;
    private Marker targetMarker;
    private Marker currentMarker;
    private BitmapDescriptor originIcon;
    private BitmapDescriptor originIconSelected;
    private BitmapDescriptor targetIcon;
    private BitmapDescriptor targetIconSelected;
    protected Handler handler = new Handler();
    private boolean isSourceSelected;
    private boolean isTargetSelected;
    private MediatorLiveData<NetworkResponse<Order>> createOrderMediator;
    private MediatorLiveData<NetworkResponse<Order>> editOrderMediator;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_location, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelOrder.class);

        if (viewModel.order != null) {
            if (!(viewModel.order.sourceCity.id == viewModel.paramNewOrder.sourceCityId
                    && isValidLocation(viewModel.order.sourceLocation))) {
                viewModel.order.sourceLocation = viewModel.paramNewOrder.sourceLocation;
            }

            if (!(viewModel.order.targetCity.id == viewModel.paramNewOrder.targetCityId
                    && isValidLocation(viewModel.order.targetLocation))) {
                viewModel.order.targetLocation = viewModel.paramNewOrder.targetLocation;
            }
        }

        initMap();
        return binding.getRoot();
    }

    public void reload() {
        if (googleMap == null) {
            initMap();
        } else {
            googleMap.clear();
        }
    }

    protected void initMap() {
        binding.mapView.onCreate(mSavedInstanceState);
        binding.mapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        binding.mapView.getMapAsync(mMap -> {
            googleMap = mMap;
            isMapReady = true;
            googleMap.getUiSettings().setCompassEnabled(false);
            googleMap.getUiSettings().setRotateGesturesEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
//            googleMap.getUiSettings().setAllGesturesEnabled(false);

            googleMap.setOnMarkerClickListener(marker -> {
                if (isTargetSelected)
                    return false;
                if (marker.equals(sourceMarker)) {
                    marker.setIcon(originIconSelected);
                    isSourceSelected = true;
                    isTargetSelected = false;
                    marker.setVisible(true);
                    goToTarget();
                } else if (marker.equals(targetMarker)) {
                    marker.setIcon(targetIconSelected);
                    isTargetSelected = true;
                    currentMarker = null;
                    fitToBounds();
                    marker.setVisible(true);
                }
                return false;
            });

            googleMap.setOnCameraMoveStartedListener(i -> {
                if (isTargetSelected)
                    return;
                binding.imgFakeMarker.setVisibility(View.VISIBLE);
                if (currentMarker != null) {
                    currentMarker.setVisible(false);
                }
            });

            googleMap.setOnCameraIdleListener(() -> {
                if (isTargetSelected)
                    return;
                LatLng center = googleMap.getCameraPosition().target;
                new Handler().postDelayed(() -> {
                    binding.imgFakeMarker.setVisibility(View.INVISIBLE);
                }, 30);
                if (currentMarker != null) {
                    currentMarker.setPosition(center);
                    currentMarker.setVisible(true);
                }
            });
            showMarkers();
        });
    }

    private void setSourceAsCurrentMarker() {
        currentMarker = sourceMarker;
        binding.imgFakeMarker.setImageResource(R.drawable.ic_map_marker_source_rised);
    }

    private void setTargetAsCurrentMarker() {
        currentMarker = targetMarker;
        binding.imgFakeMarker.setImageResource(R.drawable.ic_map_marker_target_rised);
    }

    protected void showMarkers() {
        LatLng sourceLatLng = getSourceLatLng();
        LatLng targetLatLng = getSourceLatLng();
        if (!(isValidLocation(sourceLatLng) && isValidLocation(targetLatLng))) {
            return;
        }
        addMarkersToMap();
    }

    private LatLng getSourceLatLng() {
        if (viewModel.order != null) {
            return new LatLng(viewModel.order.sourceLocation.latitude, viewModel.order.sourceLocation.longitude);
        } else {
            return new LatLng(viewModel.paramNewOrder.sourceLocation.latitude, viewModel.paramNewOrder.sourceLocation.longitude);
        }
    }

    private LatLng getTargetLatLng() {
        if (viewModel.order != null) {
            return new LatLng(viewModel.order.targetLocation.latitude, viewModel.order.targetLocation.longitude);
        } else {
            return new LatLng(viewModel.paramNewOrder.targetLocation.latitude, viewModel.paramNewOrder.targetLocation.longitude);
        }
    }

    private void addMarkersToMap() {
        originIcon = BitmapDescriptorFactory.fromBitmap(
                Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_marker_source));
        originIconSelected = BitmapDescriptorFactory.fromBitmap(
                Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_marker_source_selected));

        targetIcon = BitmapDescriptorFactory.fromBitmap(
                Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_marker_target));

        targetIconSelected = BitmapDescriptorFactory.fromBitmap(
                Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_marker_target_selected));

        targetMarker = googleMap.addMarker(new MarkerOptions()
                .position(getTargetLatLng())
                .icon(targetIcon));

        sourceMarker = googleMap.addMarker(new MarkerOptions()
                .position(getSourceLatLng())
                .icon(originIcon));

        setSourceAsCurrentMarker();
        goToOrigin();
    }

    private void fitToBounds() {
        binding.mapView.post(() -> {
            sourceMarker.setVisible(true);
            targetMarker.setVisible(true);
            binding.imgFakeMarker.setVisibility(View.INVISIBLE);
            int markerSize = (int) getResources().getDimension(R.dimen.marker_size);
            int width = binding.mapView.getMeasuredWidth() - (int) (markerSize * 0.75);
            int height = binding.mapView.getMeasuredHeight() - (int) (markerSize * 1.75);
            LatLngBounds bounds = new LatLngBounds.Builder()
                    .include(sourceMarker.getPosition())
                    .include(targetMarker.getPosition())
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, 16));
        });
    }

    private void goToOrigin() {
            sourceCameraPosition = new CameraPosition.Builder().target(sourceMarker.getPosition()).zoom(zoomLevel).build();
            binding.mapView.post(() -> {
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(sourceCameraPosition));
                binding.imgFakeMarker.setVisibility(View.INVISIBLE);
                setSourceAsCurrentMarker();
            });
    }

    private void goToTarget() {
            targetCameraPosition = new CameraPosition.Builder().target(targetMarker.getPosition()).zoom(zoomLevel).build();
            binding.mapView.post(() -> {
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(targetCameraPosition));
                binding.imgFakeMarker.setVisibility(View.INVISIBLE);
                setTargetAsCurrentMarker();
            });
    }

    protected void setupMapListener() {
//        if (!binding.imgOrigin.hasOnClickListeners()) {
//            binding.imgOrigin.setOnClickListener(v -> {
//                if (isMapReady) {
//                    goToOrigin();
//                }
//            });
//        }
//        if (!binding.imgDestination.hasOnClickListeners()) {
//            binding.imgDestination.setOnClickListener(v -> {
//                if (isMapReady) {
//                    goToTarget();
//                }
//            });
//        }
    }

    private void positionCamera(DirectionsRoute route, GoogleMap mMap) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.legs[overview].startLocation.lat,
                route.legs[overview].startLocation.lng), 12));
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        if (results.routes.length > 0) {
            List<LatLng> decodedPath = PolyUtil.decode(results.routes[overview].overviewPolyline.getEncodedPath());
            mMap.addPolyline(new PolylineOptions()
                    .addAll(decodedPath)
                    .startCap(new RoundCap())
                    .endCap(new RoundCap())
                    .width(15)
                    .geodesic(true)
                    .color(Color.CYAN));
        }
    }

    private DirectionsResult getDirectionsDetails(LatLng origin, LatLng target, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(new com.google.maps.model.LatLng(origin.latitude, origin.longitude))
                    .destination(new com.google.maps.model.LatLng(target.latitude, target.longitude))
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private GeoApiContext getGeoContext() {
        return new GeoApiContext.Builder()
                .queryRateLimit(3)
                .apiKey(getString(R.string.google_maps_key))
                .connectTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .build();
    }

    private void showProgress(boolean show) {
        mListener.showProgress(show);
    }

    private void createOrder() {
        if (createOrderMediator == null) {
            createOrderMediator = viewModel.submitOrder();
            createOrderMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress(true);
                        break;
                    case FAILURE:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                Toast.makeText(getContext(), R.string.registered, Toast.LENGTH_SHORT).show();
                                ActivityHome.REFRESH_NEED = true;
                                ActivityHome.ORDER_ID = networkResponse.getApiResponse().data.id;
                                mListener.onContinueToApp();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showConnectionErrorMessage();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                break;
                        }
                        showProgress(false);
                        break;
                    default:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                }
            });
        } else {
            viewModel.submitOrder();
        }
    }

    private void editOrder() {
        if (editOrderMediator == null) {
            editOrderMediator = viewModel.editOrder();
            editOrderMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress(true);
                        break;
                    case FAILURE:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                Toast.makeText(getContext(), R.string.registered, Toast.LENGTH_SHORT).show();
                                ActivityHome.REFRESH_NEED = true;
                                ActivityHome.ORDER_ID = networkResponse.getApiResponse().data.id;
                                mListener.onContinueToApp();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showConnectionErrorMessage();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                break;
                        }
                        showProgress(false);
                        break;
                    default:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                }
            });
        } else {
            viewModel.editOrder();
        }
    }

    private void showConnectionErrorMessage() {
        showMessage(getString(R.string.connection_error));
    }

    public void confirmForm() {
        if (isTargetSelected && isSourceSelected) {
            viewModel.paramNewOrder.sourceLocation =
                    new GeoLoc(sourceMarker.getPosition().latitude, sourceMarker.getPosition().longitude);
            viewModel.paramNewOrder.targetLocation =
                    new GeoLoc(targetMarker.getPosition().latitude, targetMarker.getPosition().longitude);
            if (viewModel.orderId == 0 || viewModel.recreate) {
                if (viewModel.orderId > 0)
                    viewModel.paramNewOrder.predecessorId = viewModel.orderId;
                createOrder();
            } else {
                editOrder();
            }
        } else {
            Toast.makeText(getContext(), getString(R.string.enter_exact_locations), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean onBackPressed() {
        if (isTargetSelected) {
            isTargetSelected = false;
            targetMarker.setIcon(targetIcon);
            goToTarget();
            return false;
        } else if (isSourceSelected) {
            isSourceSelected = false;
            sourceMarker.setIcon(originIcon);
            goToOrigin();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (binding.mapView != null) {
            binding.mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (binding.mapView != null) {
            binding.mapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (binding.mapView != null) {
            binding.mapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (binding.mapView != null) {
            binding.mapView.onLowMemory();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentHistory.OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void showSubmitBtn(boolean show);

        void onContinueToApp();

        void showProgress(boolean show);
    }
}
