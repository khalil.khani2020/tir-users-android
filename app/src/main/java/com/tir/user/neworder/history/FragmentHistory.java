package com.tir.user.neworder.history;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;

import com.tir.user.R;
import com.tir.user.app.Const;
import com.tir.user.app.StaticVariables;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentHistoryBinding;
import com.tir.user.home.list.ArrayAdapterCities;
import com.tir.user.neworder.ViewModelOrder;
import com.tir.user.repository.model.GeoLoc;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponsePathHistory;
import com.tir.user.repository.model.room.entity.CarType;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.PathCurrency;
import com.tir.user.repository.model.room.entity.ShipmentType;
import com.tir.user.repository.model.room.entity.WeightRange;
import com.tir.user.utils.DateTimeHelper;
import com.tir.user.utils.Utils;
import com.tir.user.views.CustomDatePicker;

import java.util.ArrayList;
import java.util.List;

import static com.tir.user.utils.Utils.extractDigits;
import static com.tir.user.utils.Utils.formatPrice;
import static com.tir.user.utils.Utils.isNullOrEmpty;


public class FragmentHistory extends BaseFragment implements
        RecyclerAdapterHistory.OnAdapterInteractionsListener,
        CustomDatePicker.OnDateTimeSetListener,
        ArrayAdapterCities.OnAdapterInteractions {

    private FragmentHistoryBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelOrder viewModel;
    private FormValidatorHistory formValidator;
    private MediatorLiveData<NetworkResponse<ResponsePathHistory>> pathHistoryMediator;
    private MediatorLiveData<NetworkResponse<Order>> orderDetailMediator;
    private final List<City> sourceCitiesArray = new ArrayList<>();
    private final List<City> destinationCitiesArray = new ArrayList<>();
    private List<ShipmentType> shipmentTypes = new ArrayList<>();
    private List<WeightRange> weightRanges = new ArrayList<>();
    private List<CarType> carTypes = new ArrayList<>();
    private List<PathCurrency> pathCurrencies = new ArrayList<>();
    private RecyclerAdapterHistory adapterOrders;
    private CustomDatePicker datePicker;
    private String loadingDate = "";
    private boolean isChecked;
    private ArrayAdapterCities sourceCityAdapter;
    private ArrayAdapterCities targetCityAdapter;
    private ArrayAdapter<String> currencyAdapter;
    private boolean NotCheckHistoryInputs;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelOrder.class);

        setupForm();
        if (viewModel.orderId > 0) {
            getOrderDetails();
            binding.getRoot().requestFocus();
        } else {
            binding.aTxtSourceCity.requestFocus();
        }
        return binding.getRoot();
    }

    private void setupForm() {
//        binding.scrollView.post(() -> binding.scrollView.scrollTo(0, 0));
//        formValidator = new FormValidatorHistory(getContext(), binding);
        setListeners();
        initAdapters();
        setupRecycler();
    }

    private void getOrderDetails() {
        if (orderDetailMediator == null) {
            orderDetailMediator = viewModel.getOrderDetails();
            orderDetailMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress(true);
                        break;
                    case FAILURE:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                setOrder(networkResponse.getApiResponse().data);
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showConnectionErrorMessage();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                break;
                        }
                        showProgress(false);
                        break;
                    default:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                }
            });
        } else {
            viewModel.getOrderDetails();
        }
    }

    private void getPathHistory() {
        if (pathHistoryMediator == null) {
            pathHistoryMediator = viewModel.getPathHistory();
            pathHistoryMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress(true);
                        break;
                    case FAILURE:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.pathHistories.clear();
                                viewModel.pathHistories.addAll(networkResponse.getApiResponse().data.pathHistory);
                                if (viewModel.pathHistories.size() > 0) {
                                    adapterOrders.notifyDataSetChanged();
                                    showPathHistory(true);
                                } else {
                                    showPathHistory(false);
                                }
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showConnectionErrorMessage();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                break;
                        }
                        showProgress(false);
                        break;
                    default:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                }
            });
        } else {
            viewModel.getPathHistory();
        }
    }

    private void showPathHistory(boolean show) {
        if (show) {
            binding.recyclerView.setVisibility(View.VISIBLE);
            binding.expandCollapse.setVisibility(View.VISIBLE);
            binding.txtNoOrder.setVisibility(View.GONE);
            changeArrowToExpanded();
        } else {
            binding.recyclerView.setVisibility(View.GONE);
            binding.expandCollapse.setVisibility(View.GONE);
            binding.txtNoOrder.setVisibility(View.VISIBLE);
        }
        TransitionManager.beginDelayedTransition(binding.cvOrderInfo);
    }

    private void showProgress(boolean show) {
        mListener.showProgress(show);
    }

    private void showConnectionErrorMessage() {
        showMessage(getString(R.string.connection_error));
    }

    protected void setOrder(Order order) {
        viewModel.order = order;
        binding.txtPathHistory.setVisibility(View.VISIBLE);
        binding.setOrder(order);
        NotCheckHistoryInputs = true;
        setCities();
        setShipmentTypesItem();
        setCarTypesItem();
        setWeightRangesItem();
        setCurrencyItem();
        setDate();
        // TODO: 24/12/2018 improve this:
        new Handler().postDelayed(() -> {
            binding.txtPathHistory.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(binding.cvOrderInfo);
            NotCheckHistoryInputs = false;

        }, 1000);
    }

    private void setCities() {
        sourceCityAdapter.setCityById(viewModel.order.sourceCity.id);
        targetCityAdapter.setCityById(viewModel.order.targetCity.id);
    }

    private void setShipmentTypesItem() {
        int position = 0;
        for (ShipmentType shipmentType : shipmentTypes)
            if (shipmentType.id == viewModel.order.shipmentType.id) {
                position = shipmentTypes.indexOf(shipmentType) + 1;
                break;
            }
        binding.spinnerShipmentType.setSelection(position);
    }

    private void setCarTypesItem() {
        int position = 0;
        for (CarType carType : carTypes)
            if (carType.id == viewModel.order.carType.id) {
                position = carTypes.indexOf(carType) + 1;
                break;
            }
        binding.spinnerCarType.setSelection(position);
    }

    private void setWeightRangesItem() {
        int position = 0;
        for (WeightRange weightRange : weightRanges)
            if (weightRange.id == viewModel.order.weightRange.id) {
                position = weightRanges.indexOf(weightRange) + 1;
                break;
            }
        binding.spinnerWeightRange.setSelection(position);
    }

    private void setCurrencyItem() {
        if (viewModel.order.currency != null) {
            int position = 0;
            for (PathCurrency pathCurrency : pathCurrencies)
                if (pathCurrency.currencyId == viewModel.order.currency.id) {
                    position = pathCurrencies.indexOf(pathCurrency) + 1;
                    break;
                }
            binding.spinnerCurrency.setSelection(position);
            if (position == 0) {
                Toast.makeText(getContext(), getString(R.string.this_currency_is_not_valid_for_this_path), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setDate() {
        if (!isNullOrEmpty(viewModel.order.loadingDate)) {
            binding.txtLoadingDate.setText(viewModel.order.loadingDate);
            binding.imgClearDate.setVisibility(View.VISIBLE);
            loadingDate = viewModel.order.loadingDate;
        } else {
            // TODO: 08/12/2018 check this
        }
    }

    private void initAdapters() {
        sourceCityAdapter = new ArrayAdapterCities(getContext(),
                sourceCitiesArray, viewModel.getBaseRepository(), binding.aTxtSourceCity, this);
        binding.aTxtSourceCity.setAdapter(sourceCityAdapter);
//        binding.skill.setThreshold(1);

        targetCityAdapter = new ArrayAdapterCities(getContext(),
                destinationCitiesArray, viewModel.getBaseRepository(), binding.aTxtDestinationCity, this);
        binding.aTxtDestinationCity.setAdapter(targetCityAdapter);
//        binding.skill.setThreshold(1);

        shipmentTypes = viewModel.getBaseRepository().getShipmentTypes();
        carTypes = viewModel.getBaseRepository().getCarTypes();
        weightRanges = viewModel.getBaseRepository().getWeights();

        ArrayAdapter<String> shipmentTypesAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_simple);
        shipmentTypesAdapter.add(getContext().getString(R.string.shipment_type));
        for (ShipmentType s : shipmentTypes) {
            shipmentTypesAdapter.add(s.name);
        }
        binding.spinnerShipmentType.setAdapter(shipmentTypesAdapter);
        binding.spinnerShipmentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    Utils.hideKeyboard(getActivity());
                    isHistoryItemsEntered();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> carTypesAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_simple);
        carTypesAdapter.add(getContext().getString(R.string.car_type));
        for (CarType c : carTypes) {
            carTypesAdapter.add(c.name);
        }
        binding.spinnerCarType.setAdapter(carTypesAdapter);
        binding.spinnerCarType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    Utils.hideKeyboard(getActivity());
                    isHistoryItemsEntered();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> weightsAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_simple);
        weightsAdapter.add(getContext().getString(R.string.order_weight));
        for (WeightRange w : weightRanges) {
            weightsAdapter.add(w.name);
        }
        binding.spinnerWeightRange.setAdapter(weightsAdapter);

        if (StaticVariables.SESSION.roles.equals(Const.UserRoles.FORWARDER)) {
            if (viewModel.order != null) {
//                initCurrencyAdapter(viewModel.order.sourceCity.countryId, viewModel.order.targetCity.countryId);
            }
            currencyAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_simple);
            currencyAdapter.add(getContext().getString(R.string.choose_currency));
            binding.spinnerCurrency.setAdapter(currencyAdapter);
        }
    }

    private void initCurrencyAdapter(int sourceCountryId, int targetCountryId) {
        pathCurrencies = viewModel.getPathCurrencies(sourceCountryId, targetCountryId);
        if (pathCurrencies.size() == 0) {
            Toast.makeText(getContext(), R.string.path_not_defined, Toast.LENGTH_LONG).show();
            return;
        }
        if (StaticVariables.SESSION.roles.equals(Const.UserRoles.FORWARDER)) {
            currencyAdapter.clear();
            currencyAdapter.add(getContext().getString(R.string.choose_currency));

            for (PathCurrency pc : pathCurrencies) {
                currencyAdapter.add(pc.currencyName);
            }
            currencyAdapter.notifyDataSetChanged();
            if(currencyAdapter.getCount() == 2)
                binding.spinnerCurrency.setSelection(1);
        }
    }

    private void setupRecycler() {
        adapterOrders = new RecyclerAdapterHistory(viewModel.pathHistories,
                R.layout.recycler_item_path_history, this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false));

//        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), VERTICAL);
//        binding.recyclerView.addItemDecoration(decoration);
        binding.recyclerView.setAdapter(adapterOrders);
    }

    private void setListeners() {
        binding.txtPathHistory.setOnClickListener(v -> {
            if (validatePathHistoryInputs()) return;
            Utils.hideKeyboard(getActivity());
            getPathHistory();
        });

        binding.imgPickDate.setOnClickListener(v -> {
            if (datePicker == null) {
                datePicker = new CustomDatePicker(getContext(), this);
            }
            datePicker.showDatePicker(loadingDate, 0);
        });

        binding.imgClearDate.setOnClickListener(v -> {
            binding.txtLoadingDate.setText("");
            binding.imgClearDate.setVisibility(View.GONE);
            loadingDate = "";
        });

        binding.expandCollapse.setOnClickListener(view -> {
            changeArrowDirection();
            if (binding.recyclerView.getVisibility() == View.VISIBLE) {
                binding.recyclerView.setVisibility(View.GONE);
            } else {
                binding.recyclerView.setVisibility(View.VISIBLE);
            }
            TransitionManager.beginDelayedTransition(binding.cvOrderInfo);
        });

        binding.etxtForwarderPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = extractDigits(editable.toString());
                if (!s.equals("")) {
                    binding.etxtForwarderPrice.removeTextChangedListener(this);
                    binding.etxtForwarderPrice.setText(formatPrice(Long.valueOf(s)));
                    binding.etxtForwarderPrice.setSelection(binding.etxtForwarderPrice.getText().length());
                    binding.etxtForwarderPrice.addTextChangedListener(this);
                }
            }
        });
    }

    private boolean isHistoryItemsEntered() {
        if (NotCheckHistoryInputs) {
            return true;
        }
        if (sourceCityAdapter.getCity() != null
                && targetCityAdapter.getCity() != null
                && binding.spinnerShipmentType.getSelectedItemPosition() > 0
                && binding.spinnerCarType.getSelectedItemPosition() > 0) {
            binding.txtPathHistory.setVisibility(View.VISIBLE);
            if (isPathHistoryParamsChanged()) {
                binding.recyclerView.setVisibility(View.GONE);
                binding.expandCollapse.setVisibility(View.GONE);
                binding.txtNoOrder.setVisibility(View.GONE);
            }
            TransitionManager.beginDelayedTransition(binding.cvOrderInfo);
            return true;
        }
        return false;

    }

    private boolean isPathHistoryParamsChanged() {
        return viewModel.paramPathHistory.sourceCityId != sourceCityAdapter.getCity().id
                || viewModel.paramPathHistory.targetCityId != targetCityAdapter.getCity().id
                || viewModel.paramPathHistory.shipmentTypeId != shipmentTypes.get(binding.spinnerShipmentType.getSelectedItemPosition() - 1).id
                || viewModel.paramPathHistory.carTypeId != carTypes.get(binding.spinnerCarType.getSelectedItemPosition() - 1).id;
    }

    private void changeArrowDirection() {
        isChecked = !isChecked;
        final int[] stateSet = {android.R.attr.state_checked * (isChecked ? 1 : -1)};
        binding.expandCollapse.setImageState(stateSet, true);
    }

    private void changeArrowToExpanded() {
        isChecked = true;
        final int[] stateSet = {android.R.attr.state_checked * (isChecked ? 1 : -1)};
        binding.expandCollapse.setImageState(stateSet, true);
    }

    private boolean validatePathHistoryInputs() {
//        String source = binding.aTxtSourceCity.getText().toString().trim();
//        if (!source.isEmpty()) {
//            if (sourceCitiesArray.size() == 0 ||
//                    (sourceCitiesArray.size() == 1 && !sourceCitiesArray.get(0).name.equals(source))) {
//                binding.aTxtSourceCity.setError(getContext().getString(R.string.city_not_found));
//                binding.aTxtSourceCity.requestFocus();
//                sourceCity = null;
//                return true;
//            }
//        } else {
//            sourceCity = null;
//            return true;
//        }

        if (sourceCityAdapter.getCity() == null) {
            binding.aTxtSourceCity.setError(getContext().getString(R.string.enter_source_city));
            binding.aTxtSourceCity.requestFocus();
            return true;
        }
        viewModel.paramPathHistory.sourceCityId = sourceCityAdapter.getCity().id;

        if (targetCityAdapter.getCity() == null) {
            binding.aTxtDestinationCity.setError(getContext().getString(R.string.enter_target_city));
            binding.aTxtDestinationCity.requestFocus();
            return true;
        }
        viewModel.paramPathHistory.targetCityId = targetCityAdapter.getCity().id;

        int shipmentTypePosition = binding.spinnerShipmentType.getSelectedItemPosition();
        viewModel.paramPathHistory.shipmentTypeId = shipmentTypePosition == 0 ? null : shipmentTypes.get(shipmentTypePosition - 1).id;
        if (binding.spinnerShipmentType.getSelectedItemPosition() == 0) {
            Toast.makeText(getContext(), getString(R.string.choose_shipment_type), Toast.LENGTH_LONG).show();
            return true;
        }

        int carTypePosition = binding.spinnerCarType.getSelectedItemPosition();
        if (binding.spinnerCarType.getSelectedItemPosition() == 0) {
            Toast.makeText(getContext(), getString(R.string.choose_car_type), Toast.LENGTH_LONG).show();
            return true;
        }
        viewModel.paramPathHistory.carTypeId = carTypePosition == 0 ? null : carTypes.get(carTypePosition - 1).id;

        return false;
    }

    private boolean validateNewOrderInputs() {

        if (sourceCityAdapter.getCity() == null) {
            binding.aTxtSourceCity.setError(getContext().getString(R.string.enter_source_city));
            binding.aTxtSourceCity.requestFocus();
            return true;
        }
        viewModel.paramNewOrder.sourceCityId = sourceCityAdapter.getCity().id;
        viewModel.paramNewOrder.sourceLocation = new GeoLoc(sourceCityAdapter.getCity().latitude, sourceCityAdapter.getCity().longitude);

        if (targetCityAdapter.getCity() == null) {
            binding.aTxtDestinationCity.setError(getContext().getString(R.string.enter_target_city));
            binding.aTxtDestinationCity.requestFocus();
            return true;
        }
        viewModel.paramNewOrder.targetCityId = targetCityAdapter.getCity().id;
        viewModel.paramNewOrder.targetLocation = new GeoLoc(targetCityAdapter.getCity().latitude, targetCityAdapter.getCity().longitude);

        if (pathCurrencies.size() == 0) {
            Toast.makeText(getContext(), getString(R.string.path_not_defined), Toast.LENGTH_LONG).show();
            return true;
        }

        int shipmentTypePosition = binding.spinnerShipmentType.getSelectedItemPosition();
        if (shipmentTypePosition == 0) {
            Toast.makeText(getContext(), getString(R.string.choose_shipment_type), Toast.LENGTH_LONG).show();
            return true;
        }
        viewModel.paramNewOrder.shipmentTypeId = shipmentTypes.get(shipmentTypePosition - 1).id;


        int carTypePosition = binding.spinnerCarType.getSelectedItemPosition();
        if (carTypePosition == 0) {
            Toast.makeText(getContext(), getString(R.string.choose_car_type), Toast.LENGTH_LONG).show();
            return true;
        }
        viewModel.paramNewOrder.carTypeId = carTypes.get(carTypePosition - 1).id;

        int weightRangePosition = binding.spinnerWeightRange.getSelectedItemPosition();
        if (weightRangePosition == 0) {
            Toast.makeText(getContext(), getString(R.string.choose_weight_range), Toast.LENGTH_LONG).show();
            return true;
        }
        viewModel.paramNewOrder.weightRangeId = weightRanges.get(weightRangePosition - 1).id;

        if (isNullOrEmpty(loadingDate)) {
            Toast.makeText(getContext(), getString(R.string.choose_loading_date), Toast.LENGTH_LONG).show();
            return true;
        } else if (DateTimeHelper.isExpired(loadingDate)) {
            Toast.makeText(getContext(), getString(R.string.loading_date_must_be_greater_than_current_date), Toast.LENGTH_LONG).show();
            return true;
        }


        viewModel.paramNewOrder.loadingDate = loadingDate;

        if (StaticVariables.SESSION.roles.equals(Const.UserRoles.FORWARDER)) {
            String forwarderPrice = binding.etxtForwarderPrice.getText().toString().replaceAll("[^\\d]", "").trim();
            if (!isNullOrEmpty(forwarderPrice)) {
                int currencyPosition = binding.spinnerCurrency.getSelectedItemPosition();
                if (currencyPosition == 0) {
                        Toast.makeText(getContext(), getString(R.string.enter_currency), Toast.LENGTH_LONG).show();
                    return true;
                }
                viewModel.paramNewOrder.forwarderPrice = Double.valueOf(Utils.toEnglishDigits(forwarderPrice));
                viewModel.paramNewOrder.currencyId = pathCurrencies.get(currencyPosition - 1).currencyId;
            }
        }
        String sourceAddress = binding.etxtSourceAddress.getText().toString().trim();
        if (isNullOrEmpty(sourceAddress)) {
            binding.etxtSourceAddress.setError(getString(R.string.enter_source_address), null);
            binding.etxtSourceAddress.requestFocus();
            return true;
        }
        viewModel.paramNewOrder.sourceAddress = sourceAddress;

        String destinationAddress = binding.etxtDestinationAddress.getText().toString().trim();
        if (isNullOrEmpty(destinationAddress)) {
            binding.etxtDestinationAddress.setError(getString(R.string.enter_destination_address), null);
            binding.etxtDestinationAddress.requestFocus();
            return true;
        }
        viewModel.paramNewOrder.targetAddress = destinationAddress;
        viewModel.paramNewOrder.description = binding.etxtDescription.getText().toString().trim();

        return false;
    }

    public void confirmForm() {
        if (!validateNewOrderInputs())
            mListener.onContinueToLocationFrag();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRecyclerItemClick(int position) {

    }

    @Override
    public void onDateSet(String formattedDate, long timeStamp, int requestCode) {
        binding.txtLoadingDate.setText(formattedDate);
        binding.imgClearDate.setVisibility(View.VISIBLE);
        loadingDate = formattedDate;
    }

    @Override
    public void onCitySelected(City city) {
        if (sourceCityAdapter.getCity() != null && targetCityAdapter.getCity() != null)
            initCurrencyAdapter(sourceCityAdapter.getCity().countryId, targetCityAdapter.getCity().countryId);
        isHistoryItemsEntered();
    }

    public interface OnFragmentInteractionListener {
        void showSubmitBtn(boolean show);

        void showProgress(boolean show);

        void onContinueToLocationFrag();
    }
}
