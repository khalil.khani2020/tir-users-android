package com.tir.user.neworder.history;

import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tir.user.databinding.RecyclerItemPathHistoryBinding;
import com.tir.user.repository.model.PathHistory;

import java.util.List;


public class RecyclerAdapterHistory extends RecyclerView.Adapter<RecyclerAdapterHistory.ViewHolder> {

    private final List<PathHistory> list;
    private int layout;
    private OnAdapterInteractionsListener mListener;
    int colorEven;
    int colorOdd;

    public RecyclerAdapterHistory(List<PathHistory> list, int layout,
                                  OnAdapterInteractionsListener listener) {
        this.list = list;
        this.layout = layout;
        this.mListener = listener;
        this.colorOdd = Color.parseColor("#ECEFF1");
        this.colorEven = Color.parseColor("#FFFFFF");
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemPathHistoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position %2 == 1)
            holder.itemView.setBackgroundColor(colorEven);
        else
            holder.itemView.setBackgroundColor(colorOdd);
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemPathHistoryBinding binding;

        ViewHolder(RecyclerItemPathHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(v -> mListener.onRecyclerItemClick(getAdapterPosition()));
        }

        void bind(PathHistory pathHistory) {
            binding.setPathHistory(pathHistory);
            binding.executePendingBindings();
        }

        RecyclerItemPathHistoryBinding getBinding() {
            return binding;
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnAdapterInteractionsListener {
        void onRecyclerItemClick(int position);
    }
}
