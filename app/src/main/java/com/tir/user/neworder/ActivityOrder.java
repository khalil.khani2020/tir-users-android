package com.tir.user.neworder;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tir.user.R;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityNewOrderBinding;
import com.tir.user.neworder.detail.FragmentLocation;
import com.tir.user.neworder.history.FragmentHistory;
import com.tir.user.utils.Utils;

public class ActivityOrder extends BaseActivity
        implements FragmentHistory.OnFragmentInteractionListener,
        FragmentLocation.OnFragmentInteractionListener {
    public static final String ORDER_ID = "orderId";
    public static final String RECREATE = "recreate";
    ActivityNewOrderBinding binding;
    private MenuItem menuItem;
    private ViewModelOrder viewModel;
    private FragmentHistory fragmentHistory;
    private FragmentLocation fragmentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_new_order, contentFrame, true);
        setSupportActionBar(binding.toolbar);
        initToolbarAndNavigationDrawer(binding.toolbar, binding.toolbarShadow);
        ////////////////////////////////////////////////////////////////////////////////////////////
        viewModel = ViewModelProviders.of(this).get(ViewModelOrder.class);
        checkFcmToken(viewModel);
//        getUiComponent().inject(this);

        if (getIntent().hasExtra(ORDER_ID)) {
            viewModel.orderId = getIntent().getIntExtra(ORDER_ID, 0);
            viewModel.recreate = getIntent().hasExtra(RECREATE);
        }
        setupMainFrag();
    }

    private void setupMainFrag() {
        fragmentHistory = new FragmentHistory();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragmentHistory, fragmentHistory.getClass().getSimpleName())
                .addToBackStack(fragmentHistory.getClass().getSimpleName())
                .commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void launchFragHorizontally(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left)
                .add(R.id.fragment_container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    private String getLastFragName() {
        return getSupportFragmentManager()
                .getBackStackEntryAt(getSupportFragmentManager()
                        .getBackStackEntryCount() - 1).getName();
    }

    @Override
    public void showSubmitBtn(boolean show) {
        if (menuItem != null) {
            menuItem.setVisible(show);
        }
    }

    @Override
    public void onContinueToApp() {
//        startActivity(new Intent(getContext(), ActivityHome.class));
        finish();
    }

    @Override
    public void showProgress(boolean show) {
        disableTouches = show;
        if (show)
            binding.content.progressBar.setVisibility(View.VISIBLE);
        else
            binding.content.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onContinueToLocationFrag() {
        if (fragmentLocation == null) {
            fragmentLocation = new FragmentLocation();
        }
        launchFragHorizontally(fragmentLocation);
        menuItem.setVisible(true);
        Utils.hideKeyboard(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_profile, menu);
        menuItem = menu.findItem(R.id.confirm);
//        menuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                int i = getSupportFragmentManager().getBackStackEntryCount();
                if (i == 1) {
                    fragmentHistory.confirmForm();
                } else if (i == 2) {
                    fragmentLocation.confirmForm();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        return this.viewModel;
    }

    @Override
    public void onBackPressed() {
        if (closeDrawer()) {
            return;
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            if (fragmentLocation.onBackPressed()) {
                super.onBackPressed();
                fragmentLocation = null;
            }
        } else {
            finish();
        }
    }
}
