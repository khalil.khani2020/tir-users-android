package com.tir.user.neworder.history;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.tir.user.R;
import com.tir.user.databinding.FragmentPersonalInfoBinding;

import static com.tir.user.utils.Validations.isValidName;


class FormValidatorHistory {
    private final FragmentPersonalInfoBinding binding;
    private final Context mContext;

    public FormValidatorHistory(Context context, FragmentPersonalInfoBinding binding) {
        this.mContext = context;
        this.binding = binding;
        setupTextWatchers();
    }

    public boolean validate() {
        if (validateFirstName()) return false;
        return !validateLastName();
    }

    private void setupTextWatchers() {
        binding.etxtFirstName.addTextChangedListener(new MyTextWatcher(binding.etxtFirstName));
        binding.etxtLastName.addTextChangedListener(new MyTextWatcher(binding.etxtLastName));
    }

    private boolean validateFirstName() {
        String firstName = binding.etxtFirstName.getText().toString().trim();
        if (TextUtils.isEmpty(firstName)) {
            binding.etxtFirstName.setError(mContext.getString(R.string.field_is_required), null);
            binding.etxtFirstName.requestFocus();
            return true;
        }
        else if (!isValidName(firstName)) {
            binding.etxtFirstName.setError(mContext.getString(R.string.field_contains_special_characters), null);
            binding.etxtFirstName.requestFocus();
            return true;
        }
        return false;
    }

    private boolean validateLastName() {
        String lastName = binding.etxtLastName.getText().toString().trim();
        if (TextUtils.isEmpty(lastName)) {
            binding.etxtLastName.setError(mContext.getString(R.string.field_is_required), null);
            binding.etxtLastName.requestFocus();
            return true;
        } else if (!isValidName(lastName)) {
            binding.etxtLastName.setError(mContext.getString(R.string.field_contains_special_characters), null);
            binding.etxtLastName.requestFocus();
            return true;
        }
        return false;
    }

//    private boolean validateLicenseNum() {
//        String licenseNum = binding.etxtLicenseNumber.getText().toString().trim();
//        if (TextUtils.isEmpty(licenseNum)) {
//            binding.etxtLicenseNumber.setError("این فیلد اجباری است", null);
//            binding.etxtLicenseNumber.requestFocus();
//            return true;
//        }
////        else if (licenseNum.length() < 10) {
////            binding.etxtLicenseNumber.setError("شماره ملی 10 رقمی است", null);
////            binding.etxtLicenseNumber.requestFocus();
////            return true;
////        }
//        return false;
//    }

    private Context getContext() {
        return mContext;
    }

    private void hideKeyboard() {
        View view = ((Activity) getContext()).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

//    private void scrollTo(View view) {
//        binding.scrollView.post(() -> {
//            binding.scrollView.scrollTo(0, view.getTop());
//            view.requestFocus();
//        });
//    }

    private class MyTextWatcher implements TextWatcher {
        private final View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!editable.toString().trim().equals("")) {
                switch (view.getId()) {
                    case R.id.etxt_firstName:
                        validateFirstName();
                        break;
                    case R.id.etxt_lastName:
                        validateLastName();
                        break;
                }
            }
        }
    }
}
