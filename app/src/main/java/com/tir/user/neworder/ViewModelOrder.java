package com.tir.user.neworder;

import android.app.Application;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.base.BaseViewModel;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryOrder;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.PathHistory;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamNewOrder;
import com.tir.user.repository.model.api.parameter.ParamPathHistory;
import com.tir.user.repository.model.api.response.ResponsePathHistory;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.PathCurrency;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class ViewModelOrder extends BaseViewModel {

    @Inject
    RepositoryOrder repository;
    public ParamPathHistory paramPathHistory = new ParamPathHistory();
    public ParamNewOrder paramNewOrder = new ParamNewOrder();
    public List<PathHistory> pathHistories = new ArrayList<>();
    public Order order;
    public int orderId = 0;
    public boolean recreate;
//    public GeoLoc sourceLocation;
//    public GeoLoc targetLocation;


    public ViewModelOrder(Application application) {
        super(application);
        getViewModelComponent().inject(this);
    }

    @Override
    public RepositoryBasic getBaseRepository() {
        return repository;
    }


    public MediatorLiveData<NetworkResponse<Order>> getOrderDetails() {
        return repository.getOrderDetail(orderId);
    }

    public MediatorLiveData<NetworkResponse<ResponsePathHistory>> getPathHistory() {
        return repository.getPathHistory(paramPathHistory);
    }

    public MediatorLiveData<NetworkResponse<Order>> submitOrder() {
        return repository.submitOrder(paramNewOrder);
    }

    public MediatorLiveData<NetworkResponse<Order>> editOrder() {
        return repository.editOrder(orderId, paramNewOrder);
    }

    public List<PathCurrency> getPathCurrencies(int sourceCountryId, int targetCountryId) {
        return repository.getPathCurrencies(sourceCountryId, targetCountryId);
    }

    public City getCityById(int id) {
        return repository.getCity(id);
    }

//    public MediatorLiveData<NetworkResponse<Object>> updateProfile() {
//        return repository.updateProfile(personalInfo);
//    }
//
//    public MediatorLiveData<NetworkResponse<Object>> updateCarInfo() {
//        return repository.updateCarInfo(carInfo);
//    }
}
