package com.tir.user.webview;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import androidx.databinding.DataBindingUtil;

import com.tir.user.R;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityWebviewBinding;
import com.tir.user.repository.model.Blog;
import com.tir.user.repository.model.Notif;
import com.tir.user.services.ServiceFirebaseMessaging;


public class ActivityWebView extends BaseActivity {

    public static final String URL = "url";
    public static final String BLOG = "blog";
    private ActivityWebviewBinding binding;
    private boolean isRequestDone;
    private boolean isRequestFailed;
    private Blog blog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_webview, contentFrameLayout, true);
        setSupportActionBar(binding.toolbar);
        initToolbar(binding.toolbar, binding.toolbarShadow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ////////////////////////////////////////////////////////////////////////////////////////////
        binding.content.webview.setWebViewClient(new WebViewClient());
        setWebViewSettings();

        if (getIntent().hasExtra(ServiceFirebaseMessaging.NOTIFICATION)) {
            Notif notif = getIntent().getParcelableExtra(ServiceFirebaseMessaging.NOTIFICATION);
            blog = notif.extractBlog();
            binding.content.webview.loadUrl(blog.url);
        } else if (getIntent().hasExtra(BLOG)) {
            blog = getIntent().getParcelableExtra(BLOG);
            binding.content.webview.loadUrl(blog.url);
        } else if (getIntent().hasExtra(URL)) {
            binding.content.webview.loadUrl(getIntent().getStringExtra(URL));
        }
    }

    private void setWebViewSettings() {
        WebSettings wSettings = binding.content.webview.getSettings();
        wSettings.setJavaScriptEnabled(true);
        wSettings.setDomStorageEnabled(true);
        wSettings.setUserAgentString(wSettings.getUserAgentString() + "; TirMobileApp");

        binding.content.webview.setWebViewClient(
                new WebViewClient() {
                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    }

                    @Override
                    public void onPageFinished(WebView webView, String url) {
                        super.onPageFinished(webView, url);
                        isRequestDone = !isRequestFailed;
                        nextStep();
                    }

                    @Override
                    @TargetApi(Build.VERSION_CODES.M)
                    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                        super.onReceivedError(view, request, error);
                        isRequestFailed = true;
                        nextStep();
                    }

                    @SuppressWarnings("deprecation")
                    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                        super.onReceivedError(view, errorCode, description, failingUrl);
                        isRequestFailed = true;
                        nextStep();
                    }
                });
    }

    private void nextStep() {
            if (isRequestDone) {
                binding.content.progressBar.setVisibility(View.GONE);
                binding.content.webview.setVisibility(View.VISIBLE);
            } else if (isRequestFailed) {
                binding.content.webview.setVisibility(View.GONE);
                errorInConnection();
            }
    }

    private void errorInConnection() {
        binding.content.errorLayout.root.setVisibility(View.VISIBLE);
        if (!binding.content.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.content.errorLayout.txtRetry.setOnClickListener(view -> {
                binding.content.errorLayout.root.setVisibility(View.GONE);
                isRequestDone = false;
                isRequestFailed = false;
                binding.content.webview.loadUrl(blog.url);
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        return null;
    }
}
