package com.tir.user.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.tir.user.R;
import com.tir.user.app.AppExecutors;
import com.tir.user.app.Const;
import com.tir.user.repository.model.Download;
import com.tir.user.repository.remot.retrofit.ApiService;
import com.tir.user.utils.Utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;


public class ServiceDownload extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static boolean isRunning;
    public static final String DOWNLOAD_URL = "DownloadUrl";
    NotificationManager notificationManager;
    Notification.Builder notificationBuilder;
    private int totalFileSize;
    private static OnDownloadListener downloadListener;
    private String downloadedAmount = "";
    private String url;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        intent.getStringExtra(DOWNLOAD_URL);
        initDownload(intent.getStringExtra(DOWNLOAD_URL));
        return super.onStartCommand(intent, flags, startId);
    }

    private void initDownload(String url) {
        this.url = url;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Const.Endpoints.API_URL)
                .build();
        ApiService retrofitInterface = retrofit.create(ApiService.class);
        Call<ResponseBody> request = retrofitInterface.downloadFile(url);
        AppExecutors executors = new AppExecutors();
        executors.networkIO().execute(() -> {
            try {
                makeServiceForeground();
                isRunning = true;
                downloadFile(request.execute().body());
            } catch (Exception e) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(() -> Toast.makeText(getApplicationContext(), R.string.download_failed, Toast.LENGTH_LONG).show());
                e.printStackTrace();
                downloadListener.onDownloadFailed();
                finishService();
            }
        });
    }

    private void downloadFile(ResponseBody body) throws Exception {
        int count;
        byte[] data = new byte[1024 * 4];
        long fileSize = body.contentLength();
        BufferedInputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 16);
        String fileName = Utils.getFileNameFromUrl(url);
        File outputFile = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                fileName != null ? fileName : "File.apk");
        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        while ((count = bis.read(data)) != -1) {
            total += count;
            totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
            double current = Math.round(total / (Math.pow(1024, 2)));
            int progress = (int) ((total * 100) / fileSize);
            long currentTime = System.currentTimeMillis() - startTime;
            Download download = new Download();
            download.setTotalFileSize(totalFileSize);
            if (currentTime > 1000 * timeCount) {
                download.setCurrentFileSize((int) current);
                download.setProgress(progress);
                downloadedAmount = String.format(getString(R.string.downloaded) + ": (%d/%d) " + getString(R.string.mb),
                        download.getCurrentFileSize(), download.getTotalFileSize());
                sendNotification(download);
                timeCount++;
            }
            output.write(data, 0, count);
        }
        onDownloadComplete(outputFile);
        output.flush();
        output.close();
        bis.close();
    }

    private void makeServiceForeground() {
        Intent notificationIntent = getPackageManager().getLaunchIntentForPackage(getPackageName());
        notificationIntent.setPackage(null); // The golden row !!!
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            String id = "DCh";
            CharSequence name = "Download Channel"; //user visible
            String description = "This is for notifying download state"; //user visible
            NotificationChannel mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{0, 1000});
            notificationManager.createNotificationChannel(mChannel);
            notificationBuilder = new Notification.Builder(getApplicationContext(), id);
        } else {
            notificationBuilder = new Notification.Builder(getApplicationContext());
        }
        notificationBuilder
                .setContentTitle(getString(R.string.app_name))
                .setProgress(100, 0, false)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setContentIntent(pendingIntent)
                .setOngoing(true);
        startForeground(773, notificationBuilder.build());
    }

    private void sendNotification(Download download) {
        if (downloadListener != null) {
            downloadListener.onDownloadProgress(download.getProgress(), downloadedAmount);
        }
        notificationBuilder.setProgress(100, download.getProgress(), false);
        notificationBuilder.setContentText(downloadedAmount);
        notificationManager.notify(773, notificationBuilder.build());
    }

    private void onDownloadComplete(File apk) {
        Download download = new Download();
        download.setProgress(100);
        if (downloadListener != null) {
            downloadListener.onDownloadComplete();
        }
        installApk(apk);
        finishService();
    }

    private void finishService() {
        notificationManager.cancelAll();
        isRunning = false;
        removeDownloadListener();
        stopSelf();
    }

    private void installApk(File apkFile) {
        Uri uri = Uri.fromFile(apkFile);
        Intent promptInstall = new Intent(Intent.ACTION_VIEW);
        promptInstall.setDataAndType(uri, "application/vnd.android.package-archive");
        promptInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(promptInstall);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }

    public static void setDownloadListener(OnDownloadListener onDownloadListener) {
        downloadListener = onDownloadListener;
    }

    public static void removeDownloadListener() {
        downloadListener = null;
    }

    public interface OnDownloadListener {
        void onDownloadProgress(int progress, String downloadedAmount);

        void onDownloadComplete();

        void onDownloadFailed();
    }
}
