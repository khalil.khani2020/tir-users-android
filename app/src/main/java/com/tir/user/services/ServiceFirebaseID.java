package com.tir.user.services;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.annotation.NonNull;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.tir.user.app.MyApp;
import com.tir.user.app.StaticVariables;
import com.tir.user.di.service.ServiceComponent;
import com.tir.user.di.service.ServiceModule;
import com.tir.user.repository.RepositoryService;
import com.tir.user.repository.model.Status;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.parameter.ParamFcmToken;

import javax.inject.Inject;

import static com.tir.user.utils.Utils.isNullOrEmpty;

public class ServiceFirebaseID extends FirebaseInstanceIdService
        implements LifecycleOwner {
    private static final String TAG = "ServiceFirebaseID";

    @Inject
    RepositoryService repository;
    private ServiceComponent serviceComponent;
    private LifecycleRegistry mLifecycleRegistry;

    @Override
    public void onCreate() {
        super.onCreate();
        mLifecycleRegistry = new LifecycleRegistry(this);
        mLifecycleRegistry.markState(Lifecycle.State.STARTED);
        if (serviceComponent == null) {
            serviceComponent = MyApp.getInstance().getAppComponent().newServiceComponent(new ServiceModule());
        }
        serviceComponent.inject(this);
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID fcmToken.
        repository.getPrefs().setFcmTokenUpdateStatus(true);
        ParamFcmToken paramFcmToken = new ParamFcmToken();
        paramFcmToken.fcmToken = FirebaseInstanceId.getInstance().getToken();
        if (paramFcmToken.fcmToken != null) {
            Log.d(TAG, "Refreshed fcmToken: " + paramFcmToken.fcmToken);
            sendRegistrationToServer(paramFcmToken);
        }
    }

    private void sendRegistrationToServer(ParamFcmToken paramFcmToken) {
        if (!isNullOrEmpty(StaticVariables.SESSION.accessToken)) {
            repository.updateFcmToken(paramFcmToken).observe(this, networkResponse -> {
                if (networkResponse.getStatus() == Status.SUCCESS) {
                    if (networkResponse.getApiResponse().status.code == ApiResponse.COMMON_SUCCESSFUL) {
                        repository.getPrefs().setFcmTokenUpdateStatus(false);
                    }
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLifecycleRegistry.markState(Lifecycle.State.DESTROYED);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }


}