package com.tir.user.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.tir.user.R;
import com.tir.user.home.ActivityHome;
import com.tir.user.repository.model.Notif;

import java.util.Map;
import java.util.Random;

public class ServiceFirebaseMessaging extends FirebaseMessagingService {
    public static final String NOTIFICATION = "notification";
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
//            if (remoteMessage.getNotification() != null) {
//                sendNotification(remoteMessage.getNotification());
//            }
            if (remoteMessage.getData().size() > 0) {
                System.out.println("FcmData: " + remoteMessage.getData());
                sendNotification(remoteMessage.getData());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(Map<String, String> message){
        Log.d(TAG, "sendNotification: " + message);
        Gson gson = new Gson();
        Notif notif = gson.fromJson(gson.toJson(message), Notif.class);

        Notification.Builder builder = new Notification.Builder(getBaseContext());
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setAutoCancel(true);
        builder.setStyle(new Notification.BigTextStyle().bigText(notif.body));
        builder.setContentTitle(notif.title);

        Intent intent = new Intent(this, ActivityHome.class);
        intent.putExtra(NOTIFICATION, notif);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, new Random().nextInt(), intent, 0);
        builder.setContentIntent(pendingIntent);
//        builder.setContentText(Message);
//        builder.setSmallIcon(android.R.drawable.ic_notification_overlay);
//        builder.setSubText("Tap to view new orders");
//        Bitmap bitmap_image =
//                BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher_round);
//        NotificationCompat.BigPictureStyle s =
//                new NotificationCompat.BigPictureStyle().bigPicture(bitmap_image);
//        s.setSummaryText("Summary text appears on expanding the notification");
//        builder.setStyle(s);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notif.channelId, builder.build());
    }
}
