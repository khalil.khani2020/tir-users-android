package com.tir.user.rating;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tir.user.R;
import com.tir.user.app.MyApp;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityRatingBinding;
import com.tir.user.home.ActivityHome;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;


public class ActivityRating extends BaseActivity
        implements FragmentRating.OnFragmentInteractionListener {

    private ActivityRatingBinding binding;
    private ViewModelRating viewModel;
    private AdapterViewPagerRating pagerAdapter;
    private int colorRed;
    private int colorGreen;
    private int colorNormal;
    private MediatorLiveData<NetworkResponse<Object>> rateMediator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_rating, contentFrameLayout, true);
        setSupportActionBar(binding.toolbar);
        initToolbarAndNavigationDrawer(binding.toolbar, binding.toolbarShadow);
        ////////////////////////////////////////////////////////////////////////////////////////////
        viewModel = ViewModelProviders.of(this).get(ViewModelRating.class);

        colorRed = ContextCompat.getColor(getContext(), R.color.rating_factor_negative);
        colorGreen = ContextCompat.getColor(getContext(), R.color.rating_factor_positive);
        colorNormal = ContextCompat.getColor(getContext(), R.color.light_bg_dark_secondary_text);

        viewModel.order = (Order) getIntent().getExtras().getSerializable(ActivityHome.UN_RATED_ORDER);


        getSupportActionBar().setTitle(viewModel.order.sourceCity.name
                + " " + getString(R.string.to) + " "
                + viewModel.order.targetCity.name);

        setupListeners();
        setupPagerAndTabs(0);
        Utils.glideCircularImage(getContext(), binding.content.imgDriver,
                viewModel.order.driver.profileImageUrl, R.drawable.ic_placeholder_profile);
        binding.content.txtDriver.setText(viewModel.order.driver.fullName);
    }

    private void setupPagerAndTabs(int currentTab) {
        pagerAdapter = new AdapterViewPagerRating(getSupportFragmentManager(), this);
        binding.content.viewPager.setAdapter(pagerAdapter);
        binding.content.tabLayout.setupWithViewPager(binding.content.viewPager);
        binding.content.viewPager.setOffscreenPageLimit(3);
        binding.content.viewPager.setCurrentItem(currentTab);
        binding.content.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                setTabColors(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        setTabColors(currentTab);
    }

    private void setTabColors(int tabPosition) {
        if (tabPosition == 0) {
            binding.content.tabLayout.setTabTextColors(colorNormal, colorGreen);
            binding.content.tabLayout.setSelectedTabIndicatorColor(colorGreen);
        } else {
            binding.content.tabLayout.setTabTextColors(colorNormal, colorRed);
            binding.content.tabLayout.setSelectedTabIndicatorColor(colorRed);
        }
        changeTabsFont();
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) binding.content.tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildesCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildesCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(MyApp.getInstance().getTypeFace(R.font.iran_sans));
                }
            }
        }
    }

    private void setupListeners() {

    }

    private boolean validateForm() {
        if (binding.content.ratingBar.getRating() == 0) {
            Toast.makeText(getContext(), "لطفا امتیاز راننده را انتخاب نمایید", Toast.LENGTH_LONG).show();
            return false;
        }

        viewModel.paramRateDriver.rating = (int) binding.content.ratingBar.getRating();

        viewModel.paramRateDriver.orderId = viewModel.order.id;

        viewModel.paramRateDriver.description = binding.content.etxtOpinion.getText().toString().trim();

        List<Integer> selected = new ArrayList<>();
        selected.addAll(pagerAdapter.fragPositiveRatingFactors.getSelectedItems());
        selected.addAll(pagerAdapter.fragNegativeRatingFactors.getSelectedItems());
        viewModel.paramRateDriver.ratingFactors = selected;

        return true;
    }

    private void rateDriver() {
        if (rateMediator == null) {
            rateMediator = viewModel.rateDriver();
            rateMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress(true);
                        break;
                    case FAILURE:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                Toast.makeText(getContext(), R.string.registered, Toast.LENGTH_SHORT).show();
//                                ActivityHome.REFRESH_NEED = true;
                                finish();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showConnectionErrorMessage();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                break;
                        }
                        showProgress(false);
                        break;
                    default:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                }
            });
        } else {
            viewModel.rateDriver();
        }
    }

    public void showProgress(boolean show) {
        if (show)
            binding.content.progressBar.setVisibility(View.VISIBLE);
        else
            binding.content.progressBar.setVisibility(View.GONE);
    }

    private void showConnectionErrorMessage() {
        showMessage(getString(R.string.connection_error));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_rating, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                if (validateForm()) {
                    rateDriver();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (closeDrawer())
            return;
        super.onBackPressed();
    }

    @Override
    protected BaseViewModel getViewModel() {
        return this.viewModel;
    }

    @Override
    public void checkOpposite(int id) {
        if (binding.content.viewPager.getCurrentItem() == 0)
            pagerAdapter.fragNegativeRatingFactors.checkOpposite(id);
        else
            pagerAdapter.fragPositiveRatingFactors.checkOpposite(id);
    }
}
