package com.tir.user.rating;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.tir.user.R;
import com.tir.user.databinding.RecyclerItemRatingFactorBinding;
import com.tir.user.repository.model.room.entity.RatingFactor;

import java.util.ArrayList;
import java.util.List;


public class RecyclerAdapterRatingFactor extends RecyclerView.Adapter<RecyclerAdapterRatingFactor.ViewHolder> {

    private int layout;
    private Context mContext;
    private final List<RatingFactor> list;
    private OnAdapterInteractionsListener mListener;
    private int selectedColor;
    private int deSelectedColor;
    private Drawable selectedBackground;
    private Drawable deSelectedBackground;

    public RecyclerAdapterRatingFactor(Context context, List<RatingFactor> list, int layout,
                                       OnAdapterInteractionsListener listener, boolean isPositive) {
        this.mContext = context;
        this.list = list;
        this.layout = layout;
        this.mListener = listener;
        this.selectedColor = ContextCompat.getColor(context, R.color.white);
        if (isPositive) {
            this.deSelectedColor = ContextCompat.getColor(context, R.color.rating_factor_positive);
            this.selectedBackground = ContextCompat.getDrawable(context, R.drawable.background_border_fill_green);
            this.deSelectedBackground = ContextCompat.getDrawable(context, R.drawable.background_border_empty_blue);
        } else {
            this.deSelectedColor = ContextCompat.getColor(context, R.color.rating_factor_negative);
            this.selectedBackground = ContextCompat.getDrawable(context, R.drawable.background_border_fill_red);
            this.deSelectedBackground = ContextCompat.getDrawable(context, R.drawable.background_border_empty_red);
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemRatingFactorBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position));
        selectionStatus(holder);
    }

    public void checkOpposite(int id) {
        for (RatingFactor ratingFactor : list) {
            if (ratingFactor.oppositeId == id) {
                if (ratingFactor.isSelected) {
                    ratingFactor.isSelected = false;
                    notifyItemChanged(list.indexOf(ratingFactor));
                }
                return;
            }
        }
    }

    public List<Integer> getSelectedItems() {
        List<Integer> selected = new ArrayList();
        for (RatingFactor ratingFactor : list){
            if(ratingFactor.isSelected) {
                selected.add(ratingFactor.id);
            }
        }
        return selected;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void selectionStatus(ViewHolder holder) {
        if (list.get(holder.getAdapterPosition()).isSelected) {
            holder.binding.txtRatingFactor.setBackground(selectedBackground);
            holder.binding.txtRatingFactor.setTextColor(selectedColor);
        } else {
            holder.binding.txtRatingFactor.setBackground(deSelectedBackground);
            holder.binding.txtRatingFactor.setTextColor(deSelectedColor);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemRatingFactorBinding binding;

        ViewHolder(RecyclerItemRatingFactorBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.txtRatingFactor.setOnClickListener(v -> {
                list.get(getAdapterPosition()).isSelected = !list.get(getAdapterPosition()).isSelected;
                selectionStatus(this);
                if (list.get(getAdapterPosition()).isSelected)
                    mListener.onRecyclerItemClick(list.get(getAdapterPosition()).id);
            });
        }

        void bind(RatingFactor ratingFactor) {
            binding.setRatingFactor(ratingFactor);
            binding.executePendingBindings();
        }

        RecyclerItemRatingFactorBinding getBinding() {
            return binding;
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnAdapterInteractionsListener {
        void onRecyclerItemClick(int position);
    }
}
