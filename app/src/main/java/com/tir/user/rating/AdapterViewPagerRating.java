package com.tir.user.rating;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.tir.user.R;


/**
 * Created by khani on 15/10/2017.
 */

public class AdapterViewPagerRating extends FragmentPagerAdapter {
    private final Context mContext;
    public FragmentRating fragPositiveRatingFactors;
    public FragmentRating fragNegativeRatingFactors;

    public AdapterViewPagerRating(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                fragPositiveRatingFactors = FragmentRating.newInstance(true);
                return fragPositiveRatingFactors;
            case 1:
                fragNegativeRatingFactors = FragmentRating.newInstance(false);
                return fragNegativeRatingFactors;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.positive_points);
            case 1:
                return mContext.getString(R.string.negative_points);
            default:
                return super.getPageTitle(position);
        }
    }
}
