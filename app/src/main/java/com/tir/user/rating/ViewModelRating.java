package com.tir.user.rating;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.base.BaseViewModel;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryOrder;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamRateDriver;
import com.tir.user.repository.model.room.entity.RatingFactor;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by khani on 08/11/2017.
 **/

public class ViewModelRating extends BaseViewModel {

    public List<RatingFactor> positiveRatingFactors;
    public List<RatingFactor> negativeRatingFactors;
    public Order order;
    public ParamRateDriver paramRateDriver = new ParamRateDriver();
    private LiveData<NetworkResponse<Object>> rateLive;
    private MediatorLiveData<NetworkResponse<Object>> rateMediator = new MediatorLiveData<>();

    @Inject
    protected RepositoryOrder repository;

    public ViewModelRating(Application application) {
        super(application);
        getViewModelComponent().inject(this);
    }

    public List<RatingFactor> getPositiveRatingFactors(boolean isPositive) {
        if (isPositive)
            return positiveRatingFactors = repository.getRatingFactors(true);
        else
            return negativeRatingFactors = repository.getRatingFactors(false);
    }

    public MediatorLiveData<NetworkResponse<Object>> rateDriver() {
        if (rateLive != null) {
            rateMediator.removeSource(rateLive);
        }
        rateLive = repository.rateDriver(paramRateDriver);
        rateMediator.addSource(rateLive, rateMediator::setValue);
        return rateMediator;
    }

    @Override
    public RepositoryBasic getBaseRepository() {
        return repository;
    }
}