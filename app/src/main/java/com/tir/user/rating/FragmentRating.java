package com.tir.user.rating;


import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentRatingBinding;

import java.util.List;


public class FragmentRating extends BaseFragment
        implements RecyclerAdapterRatingFactor.OnAdapterInteractionsListener {

    private static final String PARAM = "isPositive";
    private FragmentRatingBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelRating viewModel;
    private RecyclerAdapterRatingFactor adapterRatingFactor;
    private boolean isPositive;

    public static FragmentRating newInstance(boolean isPositive) {
        FragmentRating f = new FragmentRating();
        Bundle args = new Bundle();
        args.putBoolean(PARAM, isPositive);
        f.setArguments(args);
        return f;
    }

    public boolean getIsPositive() {
        return getArguments().getBoolean(PARAM, isPositive);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rating, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelRating.class);

        isPositive = getIsPositive();
        setupRecycler();
        return binding.getRoot();
    }


    private void setupRecycler() {
        adapterRatingFactor = new RecyclerAdapterRatingFactor(getContext(), viewModel.getPositiveRatingFactors(isPositive),
                R.layout.recycler_item_rating_factor,this, isPositive);
        binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        binding.recyclerView.setAdapter(adapterRatingFactor);
    }

    public void checkOpposite(int id) {
        adapterRatingFactor.checkOpposite(id);
    }

    public List<Integer> getSelectedItems() {
        return adapterRatingFactor.getSelectedItems();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRecyclerItemClick(int id) {
        mListener.checkOpposite(id);
    }

    public interface OnFragmentInteractionListener {
        void checkOpposite(int id);
    }
}