package com.tir.user.base;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MediatorLiveData;

import com.google.firebase.iid.FirebaseInstanceId;
import com.tir.user.app.Const;
import com.tir.user.app.MyApp;
import com.tir.user.app.StaticVariables;
import com.tir.user.di.viewmodel.ViewModelComponent;
import com.tir.user.di.viewmodel.ViewModelModule;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamFcmToken;

import static com.tir.user.utils.Utils.isNullOrEmpty;

/**
 * Created by khani on 26/09/2017.
 */

public abstract class BaseViewModel extends AndroidViewModel {

    protected final String TAG = Const.APP_TAG + this.getClass().getSimpleName();
    private ViewModelComponent viewModelComponent;

    protected BaseViewModel(Application application) {
        super(application);
    }

    protected ViewModelComponent getViewModelComponent() {
        if (viewModelComponent == null) {
            viewModelComponent = MyApp.getInstance().getAppComponent().newViewModelComponent(new ViewModelModule());
        }
        return viewModelComponent;
    }

    public abstract RepositoryBasic getBaseRepository();

    public MediatorLiveData<NetworkResponse<Object>> updateFcmToken() {
        if (!isNullOrEmpty(StaticVariables.SESSION.accessToken)) {
            if (getBaseRepository().getPrefs().isDeleteFcmTokenNeeded()) {
                deleteFcmToken();
            }
            if (getBaseRepository().getPrefs().isUpdateFcmTokenNeeded()) {
                ParamFcmToken paramFcmToken = new ParamFcmToken();
                paramFcmToken.fcmToken = FirebaseInstanceId.getInstance().getToken();
                if (paramFcmToken.fcmToken != null) {
                    return getBaseRepository().updateFcmToken(paramFcmToken);
                }
            }
        }
        return null;
    }

    public MediatorLiveData<NetworkResponse<Object>> logout() {
        return getBaseRepository().logout();
    }

    private void deleteFcmToken() {
        new Thread(() -> {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
                getBaseRepository().getPrefs().setFcmTokenDeleteStatus(false);
                getBaseRepository().getPrefs().setFcmTokenUpdateStatus(true);
            } catch (Exception e) {
                getBaseRepository().getPrefs().setFcmTokenDeleteStatus(true);
                e.printStackTrace();
            }
        }).start();
    }
}
