package com.tir.user.base;

import android.app.Application;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryOrder;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamOrders;
import com.tir.user.repository.model.api.parameter.ParamPrice;

import javax.inject.Inject;

public class OrderViewModel extends BaseViewModel {

    public final ParamOrders paramOrders = new ParamOrders();
    public final ParamPrice paramPrice = new ParamPrice();
    public int selectedPosition;
    public Order selectedOrder;
    public int orderId;
    public boolean notifyItemChanged;
    public boolean notifyItemRemoved;

    @Inject
    protected RepositoryOrder repository;

    protected OrderViewModel(Application application) {
        super(application);
    }

    public MediatorLiveData<NetworkResponse<Order>> getOrderDetails() {
        return repository.getOrderDetail(orderId);
    }

    public MediatorLiveData<NetworkResponse<Object>> submitPrice() {
        return repository.submitPrice(paramPrice);
    }

    public MediatorLiveData<NetworkResponse<Object>> acceptOrder() {
        return repository.acceptOrder(selectedOrder.id);
    }

    public MediatorLiveData<NetworkResponse<Object>> deleteOffer() {
        return repository.deleteOffer(selectedOrder.id);
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public RepositoryBasic getBaseRepository() {
        return repository;
    }
}
