package com.tir.user.base;

import android.app.Dialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.view.Window;

import com.tir.user.R;
import com.tir.user.app.Const;
import com.tir.user.databinding.DialogAlertBinding;
import com.tir.user.di.ui.UiComponent;

public class BaseFragment extends Fragment {

    protected final String TAG = Const.APP_TAG + this.getClass().getSimpleName();
    private Dialog messageDialog;
    private DialogAlertBinding messageBinding;
    protected UiComponent getUiComponent() {
        return ((BaseActivity) getActivity()).getUiComponent();
    }

    protected Fragment getFragment() {
        return this;
    }

    protected void showMessage(String message) {
        if (messageDialog == null) {
            messageBinding = DataBindingUtil.inflate(getLayoutInflater(),
                    R.layout.dialog_alert, null, false);
            messageDialog = new Dialog(getContext());
            messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // removes dialog header
            messageDialog.setContentView(messageBinding.getRoot());
            messageBinding.txtYes.setOnClickListener(v -> {
                messageDialog.dismiss();
            });
            messageBinding.txtYes.setText(R.string.ok);
            messageBinding.txtNo.setVisibility(View.GONE);
        }
        messageBinding.txtMessage.setText(message);
        messageDialog.show();

    }
}
