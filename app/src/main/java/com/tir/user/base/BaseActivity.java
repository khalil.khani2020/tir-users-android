package com.tir.user.base;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.MediatorLiveData;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tir.user.R;
import com.tir.user.about.ActivityAbout;
import com.tir.user.app.Const;
import com.tir.user.app.MyApp;
import com.tir.user.app.StaticVariables;
import com.tir.user.databinding.DialogAlertBinding;
import com.tir.user.databinding.DialogMessageBinding;
import com.tir.user.databinding.DialogRequestPermissionBinding;
import com.tir.user.di.ui.UiComponent;
import com.tir.user.di.ui.UiModule;
import com.tir.user.home.ActivityHome;
import com.tir.user.notifications.ActivityNotification;
import com.tir.user.points.ActivityPoints;
import com.tir.user.profile.ActivityProfile;
import com.tir.user.register.ActivityRegister;
import com.tir.user.repository.model.Status;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.utils.Utils;

import java.io.IOException;
import java.lang.reflect.Field;

import static com.tir.user.utils.Utils.isNullOrEmpty;


public abstract class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected static final int REQUEST_LOCATION_PERMISSION = 200;
    protected static final int OPEN_LOCATION_SETTINGS = 100;
    private static final int TURN_ON_LOCATION = 300;
    protected final String TAG = Const.APP_TAG + this.getClass().getSimpleName();

    private DrawerLayout drawer;
    private UiComponent uiComponent;
    protected Handler handler;
    protected DrawerLayout rootView;
    protected FrameLayout contentFrame;
    private Dialog messageDialog;
    private DialogMessageBinding messageBinding;
    private int drawerSelectedItem;
    protected boolean disableTouches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApp.getInstance().addMe(this);
        MyApp.getInstance().setLanguage();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_base);
        rootView = findViewById(R.id.drawer_layout);
        contentFrame = findViewById(R.id.content_frame);
        ///////////////////////////////////////////////////////////////////////////////////////////
//        MyApp.getInstance().getAppComponent().inject(this);
        drawer = findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    protected void initToolbarAndNavigationDrawer(Toolbar toolbar, View toolbarShadow) {
        initToolbar(toolbar, toolbarShadow);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {

            }

            @Override
            public void onDrawerOpened(@NonNull View view) {

            }

            @Override
            public void onDrawerClosed(@NonNull View view) {
                switch (drawerSelectedItem) {
//                    case R.id.nav_home:
//                        if (!getActivityClass().equals(ActivityHome.class)) {
//                            finish();
//                        }
//                        break;
                    case R.id.nav_profile:
                        if (!getActivityClass().equals(ActivityProfile.class)) {
                            startActivity(new Intent(getContext(), ActivityProfile.class));
                            noTrack();
                        }
                        break;
                    case R.id.nav_notifications:
                        if (!getActivityClass().equals(ActivityNotification.class)) {
                            startActivity(new Intent(getContext(), ActivityNotification.class));
                            noTrack();
                        }
                        break;
                    case R.id.nav_my_points:
                        if (!getActivityClass().equals(ActivityPoints.class)) {
                            startActivity(new Intent(getContext(), ActivityPoints.class));
                            noTrack();
                        }
                        break;
                    case R.id.nav_logout:
                        showLogoutDialog();
                        break;
                    case R.id.nav_share:
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        // TODO: 2018/10/18 move to strings:
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "سلام\n" +
                                "با اپلیکیشن تیر بارهاتو سریع و ارزان جابجا کن\n" +
                                "https://Tir724.com/landing/invitation/" + StaticVariables.SESSION.userId);
                        startActivity(Intent.createChooser(shareIntent, getString(R.string.send_by)));
                        break;
                    case R.id.nav_about:
                        if (!getActivityClass().equals(ActivityAbout.class)) {
                            startActivity(new Intent(getContext(), ActivityAbout.class));
                            noTrack();
                        }
                        break;
                }
                drawerSelectedItem = 0;
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });
        TextView callSupport = findViewById(R.id.txt_call_support);
        callSupport.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + StaticVariables.SupportPhone));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
            drawer.closeDrawer(GravityCompat.START);
        });

        ImageView headerImage =
                navigationView.getHeaderView(0).findViewById(R.id.img_navigation_header);
        TextView headerTextFullName =
                navigationView.getHeaderView(0).findViewById(R.id.txt_full_name);
        TextView headerTextUserName =
                navigationView.getHeaderView(0).findViewById(R.id.txt_username);

//        final MenuItem logout = navigationView.getMenu().findItem(R.id.nav_logout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, +
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                //TODO: this is calling twice on slide
                super.onDrawerSlide(drawerView, slideOffset);
                if (!isNullOrEmpty(StaticVariables.SESSION.profileImageUrl)) {
                    Utils.glideCircularImage(getContext(), headerImage,
                            StaticVariables.SESSION.profileImageUrl, R.drawable.ic_placeholder_profile);
                }
                if (!isNullOrEmpty(StaticVariables.SESSION.firstName) || !isNullOrEmpty(StaticVariables.SESSION.lastName)) {
                    headerTextFullName.setText(
                            StaticVariables.SESSION.firstName + " " + StaticVariables.SESSION.lastName);
                    headerTextUserName.setText(StaticVariables.SESSION.userName);
                } else {
                    headerTextFullName.setText(StaticVariables.SESSION.userName);
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    protected void initToolbar(Toolbar toolbar, View toolbarShadow) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            toolbarShadow.setVisibility(View.VISIBLE);
        }
        setToolbarFont(toolbar);
    }

    protected void setToolbarFont(Toolbar toolbar) {
        try {
            Field field = toolbar.getClass().getDeclaredField("mTitleTextView");
            field.setAccessible(true);
            TextView txt_toolbar_title = (TextView) field.get(toolbar);
            txt_toolbar_title.setTypeface(MyApp.getInstance().getTypeFace(R.font.iran_sans));
            txt_toolbar_title.setTextSize(18);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void isFirstLogin() {
        if (StaticVariables.IS_LOGGED_IN) {
//            startLocationReporting();
            getViewModel().getBaseRepository().saveLoginStatus(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (disableTouches)
            return true;//consume
        else
            return super.dispatchTouchEvent(ev);
    }

    protected void checkFcmToken(BaseViewModel viewModel) {
        MediatorLiveData<NetworkResponse<Object>> updateFcmToken = viewModel.updateFcmToken();
        if (updateFcmToken != null) {
            updateFcmToken.observe(this, networkResponse -> {
                if (networkResponse.getStatus() == Status.SUCCESS) {
                    if (networkResponse.getApiResponse().status.code == ApiResponse.COMMON_SUCCESSFUL) {
                        viewModel.getBaseRepository().getPrefs().setFcmTokenUpdateStatus(false);
                    }
                }
            });
        }
    }

    public UiComponent getUiComponent() {
        if (uiComponent == null) {
            uiComponent = MyApp.getInstance().getAppComponent().newUiComponent(new UiModule(this));
        }
        return uiComponent;
    }

    private Class<?> getActivityClass() {
        return getClass();
    }

    protected boolean closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return true;
        } else {
            return false;
        }
    }

    protected void requestLocationPermission() {
        if (isLocationPermissionGranted()) return;
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // You may display a non-blocking explanation here, read more in the documentation:
            // https://developer.android.com/training/permissions/requesting.html
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_LOCATION_PERMISSION);
    }

    private boolean isLocationPermissionGranted() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void showPermissionDialog() {
        Dialog permissionDialog = new Dialog(getContext());
        DialogRequestPermissionBinding permissionBinding =
                DataBindingUtil.inflate(getLayoutInflater(),
                        R.layout.dialog_request_permission, null, false);
        permissionBinding.txtMessage.setText(R.string.location_permission_needed);
        permissionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // removes dialog header
        permissionDialog.setContentView(permissionBinding.getRoot());
        permissionDialog.setCancelable(false);

        permissionBinding.txtOk.setOnClickListener(v -> {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, OPEN_LOCATION_SETTINGS);
            permissionDialog.dismiss();
        });

        permissionBinding.txtCancel.setOnClickListener(v -> {
            permissionDialog.dismiss();
            finish();
        });

        permissionDialog.show();
    }

    protected boolean isPlayServicesUpdateNeeded() {
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(this);

        return GoogleApiAvailability.GOOGLE_PLAY_SERVICES_VERSION_CODE > gApi.getApkVersion(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
//                    startLocationReporting();
                } else {
                    // permission denied
                    showPermissionDialog();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TURN_ON_LOCATION) {
//            if (resultCode == -1) {
//            startLocationService();
//            } else {
//                if (locationSwitchDialog != null) {
//                    locationSwitchDialog.dismiss();
//                }
//            }
        } else if (requestCode == OPEN_LOCATION_SETTINGS) {
            if (isLocationPermissionGranted()) {
//                startLocationReporting();
            } else {
                requestLocationPermission();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!closeDrawer()) {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerSelectedItem = item.getItemId();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        // TODO: 2018/09/20 call logout api and refresh fcm token
//        getViewModel().logout().observe(this, networkResponse -> {
//            if (networkResponse.getStatus() == Status.SUCCESS) {
//                if (networkResponse.getApiResponse().state.code == ApiResponse.COMMON_SUCCESSFUL) {
//                }
//            }
//        });

        getViewModel().getBaseRepository().clearSession();
        getViewModel().getBaseRepository().clearPrefs();
        // TODO: 2018/09/22 inject appExecutors
        new Thread(() -> {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        Intent intent = new Intent(getApplicationContext(), ActivityRegister.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        finishAffinity();
        startActivity(intent);
    }

    protected void showMessage(String message) {
        if (messageDialog == null) {
            messageBinding = DataBindingUtil.inflate(getLayoutInflater(),
                    R.layout.dialog_message, null, false);
            messageDialog = new Dialog(getContext());
            messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // removes dialog header
            messageDialog.setContentView(messageBinding.getRoot());
            messageBinding.txtYes.setOnClickListener(v -> {
                messageDialog.dismiss();
            });
        }
        messageBinding.txtMessage.setText(message);
        messageDialog.show();
    }

    private void showLogoutDialog() {
        Dialog alertDialog = new Dialog(getContext());
        DialogAlertBinding alertBinding =
                DataBindingUtil.inflate(getLayoutInflater(),
                        R.layout.dialog_alert, null, false);
        alertBinding.txtMessage.setText(R.string.would_you_like_to_login);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // removes dialog header
        alertDialog.setContentView(alertBinding.getRoot());
//        alertDialog.setCancelable(false);

        alertBinding.txtYes.setOnClickListener(v -> {
            logout();
            alertDialog.dismiss();
        });

        alertBinding.txtNo.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        alertDialog.show();
    }

    private void restartApp() {
        Intent intent = new Intent(getApplicationContext(), ActivityRegister.class);
        int mPendingIntentId = 307;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    private void noTrack() {
        if (!getClass().equals(ActivityHome.class)) {
            getHandler().postDelayed(() -> finish(), 500);
        }
    }

    protected Activity getContext() {
        return this;
    }

    protected Handler getHandler() {
        if (handler == null)
            handler = new Handler();
        return handler;
    }

    protected abstract BaseViewModel getViewModel();

    @Override
    protected void onDestroy() {
        MyApp.getInstance().removeMe(this);
        super.onDestroy();
    }

    protected void subscribeToTopic() {
        String topic = "";
        if (StaticVariables.SESSION.roles.equals("trader"))
            topic = "AndroidTrader";
        if (StaticVariables.SESSION.roles.equals("forwarder"))
            topic = "AndroidForwarder";
        System.out.println("topic: " + topic);
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
                .addOnCompleteListener(task -> {
                    String msg = "Subscription succeed";
                    if (!task.isSuccessful()) {
                        msg = "Subscription failed";
                    }
                    Log.d(TAG, msg);
                });
    }
}
