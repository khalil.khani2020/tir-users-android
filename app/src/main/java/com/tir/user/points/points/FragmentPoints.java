package com.tir.user.points.points;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentPointsBinding;
import com.tir.user.points.ViewModelPoints;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponsePoints;


public class FragmentPoints extends BaseFragment
        implements RecyclerAdapterPointActions.OnAdapterInteractionsListener {

    MediatorLiveData<NetworkResponse<ResponsePoints>> pointsMediator;
    private FragmentPointsBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelPoints viewModel;
    private RecyclerAdapterPointActions adapterPointActions;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_points, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelPoints.class);
//        mListener.changeToolbarTitle(getString(R.string.orders));
        getPoints();
        setupRecycler();
        setupSwipeRefreshLayout();
        return binding.getRoot();
    }

    public void setupCounter(int points) {
        int increment;
        int interval;

        if (points == 0) {
            increment = 1;
            interval = 1;
        } else if (points <= 100) {
            increment = 1;
            interval = 2000 / points;
        } else {
            increment = points / 100;
            interval = 20;
        }
        binding.counter.setEndValue(points);
        binding.counter.setIncrement(increment);
        binding.counter.setTimeInterval(interval);
        binding.counter.start();
    }

    public void getPoints() {
        if (pointsMediator == null) {
            pointsMediator = viewModel.getPoints();
            pointsMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.actions.clear();
                                viewModel.actions.addAll(networkResponse.getApiResponse().data.pointActions.items);
                                adapterPointActions.notifyDataSetChanged();
                                setupCounter(networkResponse.getApiResponse().data.userPoints);
                                showForm();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        } else {
            viewModel.getPoints();
        }
    }

    private void setupRecycler() {
        adapterPointActions = new RecyclerAdapterPointActions(getContext(), viewModel.actions, this, false);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false));
        binding.recyclerView.setAdapter(adapterPointActions);
    }

    private void setupSwipeRefreshLayout() {
        binding.swipeRefresh.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorAccent));
        binding.swipeRefresh.setOnRefreshListener(() -> getPoints());
    }

    private void showProgress() {
        if (binding.swipeRefresh.isRefreshing()) {
            binding.progressBar.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.GONE);
            binding.txtGetPoints.setVisibility(View.GONE);
            binding.counter.setVisibility(View.GONE);
        } else {
            binding.swipeRefresh.setEnabled(false);
            binding.progressBar.setVisibility(View.VISIBLE);
            binding.errorLayout.root.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.GONE);
            binding.counter.setVisibility(View.GONE);
            binding.txtGetPoints.setVisibility(View.GONE);
        }
    }

    private void showForm() {
        binding.swipeRefresh.setRefreshing(false);
        binding.swipeRefresh.setEnabled(true);
        binding.progressBar.setVisibility(View.GONE);
        binding.errorLayout.root.setVisibility(View.GONE);
        binding.recyclerView.setVisibility(View.VISIBLE);
        binding.counter.setVisibility(View.VISIBLE);
        if (viewModel.actions.size() > 0) {
            binding.txtGetPoints.setVisibility(View.VISIBLE);
        } else {
            binding.txtGetPoints.setVisibility(View.GONE);
        }
    }

    private void showError() {
        binding.swipeRefresh.setRefreshing(false);
        binding.swipeRefresh.setEnabled(false);
        binding.progressBar.setVisibility(View.GONE);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        binding.recyclerView.setVisibility(View.GONE);
        binding.counter.setVisibility(View.GONE);
        binding.txtGetPoints.setVisibility(View.GONE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> getPoints());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRecyclerItemClick(int position) {

    }

//    @Override
//    public void onRecyclerItemClick(int position) {
//        viewModel.selectedPosition = position;
//        viewModel.orderId = viewModel.rewards.get(position).id;
//        mListener.showOrderDetail();
//    }

    public interface OnFragmentInteractionListener {
        void changeToolbarTitle(String title);
    }
}