package com.tir.user.points.points;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.tir.user.R;
import com.tir.user.databinding.RecyclerItemPointActionBinding;
import com.tir.user.repository.model.PointAction;
import com.tir.user.utils.RecyclerViewAnimator;

import java.util.List;

/**
 * Created by khani on 08/12/2017.
 */

public class RecyclerAdapterPointActions extends RecyclerView.Adapter<RecyclerAdapterPointActions.ViewHolder> {

    private final List<PointAction> list;
    private OnAdapterInteractionsListener mListener;
    private RecyclerViewAnimator mAnimator;
    private boolean animateItems;
    private Context mContext;

    public RecyclerAdapterPointActions(Context context, List<PointAction> list, OnAdapterInteractionsListener listener, boolean animateItems) {
        this.mContext = context;
        this.list = list;
        this.mListener = listener;
        this.animateItems = animateItems;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (animateItems)
            mAnimator = new RecyclerViewAnimator(recyclerView, RecyclerViewAnimator.VERTICAL);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemPointActionBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.recycler_item_point_action, parent, false);
        if (animateItems)
            mAnimator.onCreateViewHolder(binding.getRoot());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PointAction action = list.get(position);
        holder.bind(action);
//        Utils.glideImage(mContext, holder.getBinding().imageView, action.shipmentType.imageUrl);
        if (animateItems)
            mAnimator.onBindViewHolder(holder.getBinding().getRoot(), position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnAdapterInteractionsListener {
        void onRecyclerItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemPointActionBinding binding;

        ViewHolder(RecyclerItemPointActionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(v -> mListener.onRecyclerItemClick(getAdapterPosition()));
        }

        void bind(PointAction pointAction) {
            binding.setAction(pointAction);
            binding.executePendingBindings();
        }

        RecyclerItemPointActionBinding getBinding() {
            return binding;
        }
    }
}
