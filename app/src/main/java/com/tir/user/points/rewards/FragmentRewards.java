package com.tir.user.points.rewards;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.FragmentRewardsBinding;
import com.tir.user.points.ViewModelPoints;
import com.tir.user.points.points.FragmentPoints;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.response.ResponseNull;
import com.tir.user.repository.model.api.response.ResponseRewards;


public class FragmentRewards extends BaseFragment
        implements RecyclerAdapterRewards.OnAdapterInteractionsListener {

    MediatorLiveData<NetworkResponse<ResponseRewards>> rewardsMediator;
    MediatorLiveData<NetworkResponse<ResponseNull>> spendMediator;
    private FragmentRewardsBinding binding;
    private ViewModelPoints viewModel;
    private RecyclerAdapterRewards adapterRewards;
    private OnFragmentInteractionListener mListener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rewards, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelPoints.class);
        mListener.changeToolbarTitle(getString(R.string.orders));
        getRewards();
        setupRecycler();
        setupSwipeRefreshLayout();
        return binding.getRoot();
    }

    public void getRewards() {
        if (rewardsMediator == null) {
            rewardsMediator = viewModel.getRewards();
            rewardsMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.rewards.clear();
                                viewModel.rewards.addAll(networkResponse.getApiResponse().data.items);
                                adapterRewards.notifyDataSetChanged();
                                showForm();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        } else {
            viewModel.getRewards();
        }
    }

    public void spendPoints() {
        if (spendMediator == null) {
            spendMediator = viewModel.spendPoints();
            spendMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.paramSpendPoints.rewardId = 0;
                                mListener.UpdateValues();
                                showForm();
                                showMessage(getContext().getString(R.string.reward_registered));
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                viewModel.paramSpendPoints.rewardId = 0;
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        } else {
            viewModel.spendPoints();
        }
    }

    private void setupRecycler() {
        adapterRewards = new RecyclerAdapterRewards(getContext(), viewModel.rewards, this, true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false));
        binding.recyclerView.setAdapter(adapterRewards);
    }

    private void setupSwipeRefreshLayout() {
        binding.swipeRefresh.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorAccent));
        binding.swipeRefresh.setOnRefreshListener(
                () -> getRewards()
        );
    }

    private void showProgress() {
        if (binding.swipeRefresh.isRefreshing()) {
            binding.progressBar.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.GONE);
            binding.txtNoItem.setVisibility(View.GONE);
        } else {
            binding.swipeRefresh.setEnabled(false);
            binding.progressBar.setVisibility(View.VISIBLE);
            binding.errorLayout.root.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.GONE);
            binding.txtNoItem.setVisibility(View.GONE);
        }
    }

    private void showForm() {
        binding.swipeRefresh.setRefreshing(false);
        binding.swipeRefresh.setEnabled(true);
        binding.progressBar.setVisibility(View.GONE);
        binding.errorLayout.root.setVisibility(View.GONE);
        binding.recyclerView.setVisibility(View.VISIBLE);
        if (viewModel.rewards.size() > 0)
            binding.txtNoItem.setVisibility(View.GONE);
        else
            binding.txtNoItem.setVisibility(View.VISIBLE);
    }

    private void showError() {
        binding.swipeRefresh.setRefreshing(false);
        binding.swipeRefresh.setEnabled(false);
        binding.progressBar.setVisibility(View.GONE);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        binding.recyclerView.setVisibility(View.GONE);
        binding.txtNoItem.setVisibility(View.GONE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> {
                if (viewModel.paramSpendPoints.rewardId != 0)
                    spendPoints();
                else
                    getRewards();
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentPoints.OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRecyclerItemClick(int position) {
        viewModel.paramSpendPoints.rewardId = viewModel.rewards.get(position).id;
        spendPoints();
    }

    public interface OnFragmentInteractionListener {
        void changeToolbarTitle(String title);

        void UpdateValues();
    }
}