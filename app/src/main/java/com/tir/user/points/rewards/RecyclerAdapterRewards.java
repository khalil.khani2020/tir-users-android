package com.tir.user.points.rewards;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.tir.user.R;
import com.tir.user.databinding.RecyclerItemRewardsBinding;
import com.tir.user.repository.model.Reward;
import com.tir.user.utils.RecyclerViewAnimator;
import com.tir.user.utils.Utils;

import java.util.List;

/**
 * Created by khani on 08/12/2017.
 */

public class RecyclerAdapterRewards extends RecyclerView.Adapter<RecyclerAdapterRewards.ViewHolder> {

    private final List<Reward> list;
    private OnAdapterInteractionsListener mListener;
    private RecyclerViewAnimator mAnimator;
    private boolean animateItems;
    private Context mContext;

    public RecyclerAdapterRewards(Context context, List<Reward> list, OnAdapterInteractionsListener listener, boolean animateItems) {
        this.mContext = context;
        this.list = list;
        this.mListener = listener;
        this.animateItems = animateItems;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (animateItems)
            mAnimator = new RecyclerViewAnimator(recyclerView, RecyclerViewAnimator.VERTICAL);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemRewardsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.recycler_item_rewards, parent, false);
        if (animateItems)
            mAnimator.onCreateViewHolder(binding.getRoot());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Reward reward = list.get(position);
        holder.bind(reward);
        Utils.glideImage(mContext, holder.getBinding().imageView, reward.imageUrl);
        if (animateItems)
            mAnimator.onBindViewHolder(holder.getBinding().getRoot(), position);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnAdapterInteractionsListener {
        void onRecyclerItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemRewardsBinding binding;

        ViewHolder(RecyclerItemRewardsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.txtChangePoints.setOnClickListener(v -> mListener.onRecyclerItemClick(getAdapterPosition()));
        }

        void bind(Reward reward) {
            binding.setReward(reward);
            binding.executePendingBindings();
        }

        RecyclerItemRewardsBinding getBinding() {
            return binding;
        }
    }
}
