package com.tir.user.points;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.tir.user.R;
import com.tir.user.points.points.FragmentPoints;
import com.tir.user.points.rewards.FragmentRewards;
import com.tir.user.points.userrewards.FragmentUserRewards;


/**
 * Created by khani on 15/10/2017.
 */

class ViewPagerAdapterPoints extends FragmentPagerAdapter {
    private final Context mContext;
    public FragmentPoints fragmentPoints;
    public FragmentRewards fragmentRewards;
    public FragmentUserRewards fragmentUserRewards;

    public ViewPagerAdapterPoints(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                fragmentPoints = new FragmentPoints();
                return fragmentPoints;
            case 1:
                fragmentRewards = new FragmentRewards();
                return fragmentRewards;
            case 2:
                fragmentUserRewards = new FragmentUserRewards();
                return fragmentUserRewards;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.points);
            case 1:
                return mContext.getString(R.string.rewards);
            case 2:
                return mContext.getString(R.string.my_rewards);
            default:
                return super.getPageTitle(position);
        }
    }
}
