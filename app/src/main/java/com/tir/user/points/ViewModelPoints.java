package com.tir.user.points;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.base.BaseViewModel;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryPoints;
import com.tir.user.repository.model.PointAction;
import com.tir.user.repository.model.Reward;
import com.tir.user.repository.model.UserReward;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamPoints;
import com.tir.user.repository.model.api.parameter.ParamRewards;
import com.tir.user.repository.model.api.parameter.ParamSpendPoints;
import com.tir.user.repository.model.api.response.ResponseNull;
import com.tir.user.repository.model.api.response.ResponsePoints;
import com.tir.user.repository.model.api.response.ResponseRewards;
import com.tir.user.repository.model.api.response.ResponseUserRewards;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by khani on 08/11/2017.
 **/

public class ViewModelPoints extends BaseViewModel {


    public final List<Reward> rewards = new ArrayList<>();
    public final List<UserReward> userRewards = new ArrayList<>();
    public final List<PointAction> actions = new ArrayList<>();
    public final ParamSpendPoints paramSpendPoints = new ParamSpendPoints();

    @Inject
    RepositoryPoints repository;
    private LiveData<NetworkResponse<ResponseUserRewards>> userRewardsLive;
    private MediatorLiveData<NetworkResponse<ResponseUserRewards>> userRewardsMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<ResponsePoints>> actionsLive;
    private MediatorLiveData<NetworkResponse<ResponsePoints>> actionsMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<ResponseRewards>> rewardsLive;
    private MediatorLiveData<NetworkResponse<ResponseRewards>> rewardsMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<ResponseNull>> spendLive;
    private MediatorLiveData<NetworkResponse<ResponseNull>> spendMediator = new MediatorLiveData<>();

    public ViewModelPoints(Application application) {
        super(application);
        getViewModelComponent().inject(this);
    }

    @Override
    public RepositoryBasic getBaseRepository() {
        return repository;
    }


    public MediatorLiveData<NetworkResponse<ResponseUserRewards>> getUserRewards() {
        if (userRewardsLive != null) {
            userRewardsMediator.removeSource(userRewardsLive);
        }
        userRewardsLive = repository.getUserRewards(new ParamRewards());
        userRewardsMediator.addSource(userRewardsLive, userRewardsMediator::setValue);
        return userRewardsMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponsePoints>> getPoints() {
        if (actionsLive != null) {
            actionsMediator.removeSource(actionsLive);
        }
        actionsLive = repository.getPoints(new ParamPoints());
        actionsMediator.addSource(actionsLive, actionsMediator::setValue);
        return actionsMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponseRewards>> getRewards() {
        if (rewardsLive != null) {
            rewardsMediator.removeSource(rewardsLive);
        }
        rewardsLive = repository.getRewards(new ParamRewards());
        rewardsMediator.addSource(rewardsLive, rewardsMediator::setValue);
        return rewardsMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponseNull>> spendPoints() {
        if (spendLive != null) {
            spendMediator.removeSource(spendLive);
        }
        spendLive = repository.spendPoints(paramSpendPoints);
        spendMediator.addSource(spendLive, spendMediator::setValue);
        return spendMediator;
    }
}