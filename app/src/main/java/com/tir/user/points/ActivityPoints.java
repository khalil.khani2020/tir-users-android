package com.tir.user.points;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.tir.user.R;
import com.tir.user.app.MyApp;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityPointsBinding;
import com.tir.user.points.points.FragmentPoints;
import com.tir.user.points.rewards.FragmentRewards;
import com.tir.user.points.userrewards.FragmentUserRewards;
import com.tir.user.repository.model.Notif;
import com.tir.user.services.ServiceFirebaseMessaging;


public class ActivityPoints extends BaseActivity implements
        FragmentPoints.OnFragmentInteractionListener,
        FragmentRewards.OnFragmentInteractionListener,
        FragmentUserRewards.OnFragmentInteractionListener {

    private ActivityPointsBinding binding;
    private ViewModelPoints viewModel;
    private ViewPagerAdapterPoints pagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_points, contentFrameLayout, true);
        setSupportActionBar(binding.toolbar);
        initToolbar(binding.toolbar, binding.toolbarShadow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ////////////////////////////////////////////////////////////////////////////////////////////
        viewModel = ViewModelProviders.of(this).get(ViewModelPoints.class);
        checkFcmToken(viewModel);

        if (getIntent().hasExtra(ServiceFirebaseMessaging.NOTIFICATION)) {
            Notif notif = getIntent().getParcelableExtra(ServiceFirebaseMessaging.NOTIFICATION);
            int orderId = notif.extractOrderId();
            if (orderId > 0) {
                switch (notif.type) {
//                    case Notif.Types.OfferAccepted:
//                        viewModel.setOrderId(orderId);
//                        viewModel.selectedType = ViewModelMyOrders.CURRENT_ORDER;
//                        setupPagerAndTabs(0);
//                        showOrderDetail();
//                        break;
//                    case Notif.Types.AllocationCanceled:
//                        viewModel.setOrderId(orderId);
//                        viewModel.selectedType = ViewModelMyOrders.PREVIOUS_ORDER;
//                        setupPagerAndTabs(2);
//                        showOrderDetail();
//                        break;
                    default:
                        setupPagerAndTabs(0);
                        break;
                }
            } else {
                setupPagerAndTabs(0);
            }
        } else {
            setupPagerAndTabs(0);
        }
    }

    private void setupPagerAndTabs(int currentTab) {
        pagerAdapter = new ViewPagerAdapterPoints(getSupportFragmentManager(), this);
        binding.content.viewPager.setAdapter(pagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.content.viewPager);
        binding.content.viewPager.setOffscreenPageLimit(2);
        binding.content.viewPager.setCurrentItem(currentTab);
        binding.content.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        changeTabsFont();
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) binding.tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildesCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildesCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(MyApp.getInstance().getTypeFace(R.font.iran_sans));
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        return this.viewModel;
    }

    @Override
    public void changeToolbarTitle(String title) {

    }

    @Override
    public void UpdateValues() {
        pagerAdapter.fragmentUserRewards.getRewards();
        pagerAdapter.fragmentPoints.getPoints();
    }
}
