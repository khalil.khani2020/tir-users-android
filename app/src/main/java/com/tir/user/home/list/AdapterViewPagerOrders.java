package com.tir.user.home.list;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.tir.user.R;


/**
 * Created by khani on 15/10/2017.
 */

public class AdapterViewPagerOrders extends FragmentPagerAdapter {
    private final Context mContext;
    public FragmentCurrent fragmentCurrent;
    public FragmentPrevious fragmentPrevious;

    public AdapterViewPagerOrders(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                fragmentCurrent = new FragmentCurrent();
                return fragmentCurrent;
            case 1:
                fragmentPrevious = new FragmentPrevious();
                return fragmentPrevious;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.in_progress);
            case 1:
                return mContext.getString(R.string.pending_offers);
            default:
                return super.getPageTitle(position);
        }
    }
}
