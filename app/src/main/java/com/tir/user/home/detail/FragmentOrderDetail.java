package com.tir.user.home.detail;


import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.stfalcon.frescoimageviewer.ImageViewer;
import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.DialogAlertBinding;
import com.tir.user.databinding.DialogDescriptionBinding;
import com.tir.user.databinding.FragmentDetailsBinding;
import com.tir.user.home.ActivityHome;
import com.tir.user.home.ViewModelHome;
import com.tir.user.neworder.ActivityOrder;
import com.tir.user.repository.model.CustomImage;
import com.tir.user.repository.model.GeoLoc;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;
import static com.tir.user.utils.Utils.isNullOrEmpty;
import static com.tir.user.utils.Utils.isValidLocation;

public class FragmentOrderDetail extends BaseFragment {

    private MediatorLiveData<NetworkResponse<Order>> detailMediator;
    private MediatorLiveData<NetworkResponse<Object>> deleteMediator;
    private MediatorLiveData<NetworkResponse<Order>> recreateMediator;
    private OnFragmentInteractionListener mListener;
    private ViewModelHome viewModel;
    private FragmentDetailsBinding binding;
    protected GoogleMap googleMap;
    protected LatLng sourceLatLng;
    protected LatLng targetLatLng;
    protected CameraPosition sourceCameraPosition;
    protected CameraPosition targetCameraPosition;
    protected boolean isMapReady;
    private Marker sourceMarker;
    private Marker targetMarker;
    private BitmapDescriptor originIcon;
    private BitmapDescriptor targetIcon;
    private boolean isZoomed;
    private int zoomLevel = 15;
    private boolean isRequestDone;
    Bundle savedInstanceState;
    protected DialogAlertBinding deleteOrderBinding;
    protected Dialog deleteOrderDialog;
    ClipboardManager clipboardManager;
    private Dialog descriptionDialog;

    private List<CustomImage> driverImageUrls = new ArrayList<>();
    private List<CustomImage> deliveryImageUrls = new ArrayList<>();
    private DialogDescriptionBinding descriptionBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelHome.class);
        clipboardManager = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
//        orderViewModel = viewModel;
//        mListener.changeToolbarTitle(getString(R.string.order_details));
        setupListeners();
        initMap();
        getOrderDetails();
        return binding.getRoot();
    }

    private void setupListeners() {
        binding.imgDriverImage.setOnClickListener(view -> {
            if (viewModel.selectedOrder.driver != null) {
                if (!isNullOrEmpty(viewModel.selectedOrder.driver.profileImageUrl)) {
                    new ImageViewer.Builder<>(getContext(), driverImageUrls)
                            .setFormatter(customImage -> customImage.url)
                            .setBackgroundColorRes(R.color.transparent_grey_dark_2)
                            .setStartPosition(0)
                            .show();
                }
            }
        });
        binding.imgCarImage.setOnClickListener(view -> {
            if (!isNullOrEmpty(viewModel.selectedOrder.driver.car.carImageUrl)) {
                if (driverImageUrls.size() == 0) {
                    new ImageViewer.Builder<>(getContext(), driverImageUrls)
                            .setFormatter(customImage -> customImage.url)
                            .setBackgroundColorRes(R.color.transparent_grey_dark_2)
                            .setStartPosition(0)
                            .show();
                } else {
                    new ImageViewer.Builder<>(getContext(), driverImageUrls)
                            .setFormatter(customImage -> customImage.url)
                            .setBackgroundColorRes(R.color.transparent_grey_dark_2)
                            .setStartPosition(1)
                            .show();
                }
            }
        });
        binding.imgLoadingImage.setOnClickListener(view -> {
            if (!isNullOrEmpty(viewModel.selectedOrder.loadImageUrl)) {
                if (deliveryImageUrls.size() == 0) {
                    new ImageViewer.Builder<>(getContext(), deliveryImageUrls)
                            .setFormatter(customImage -> customImage.url)
                            .setBackgroundColorRes(R.color.transparent_grey_dark_2)
                            .setStartPosition(0)
                            .show();
                } else {
                    new ImageViewer.Builder<>(getContext(), deliveryImageUrls)
                            .setFormatter(customImage -> customImage.url)
                            .setBackgroundColorRes(R.color.transparent_grey_dark_2)
                            .setStartPosition(1)
                            .show();
                }
            }
        });
        binding.imgDeliveryImage.setOnClickListener(view -> {
            if (!isNullOrEmpty(viewModel.selectedOrder.deliverImageUrl)) {
                new ImageViewer.Builder<>(getContext(), deliveryImageUrls)
                        .setFormatter(customImage -> customImage.url)
                        .setBackgroundColorRes(R.color.transparent_grey_dark_2)
                        .setStartPosition(0)
                        .show();
            }
        });
        binding.txtSourceAddress.setOnLongClickListener(v -> {
            ClipData clipData = ClipData.newPlainText("Source Text", viewModel.selectedOrder.sourceAddress);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getContext(), R.string.address_copied, Toast.LENGTH_SHORT).show();
            return true;
        });
        binding.txtTargetAddress.setOnLongClickListener(v -> {
            ClipData clipData = ClipData.newPlainText("Source Text", viewModel.selectedOrder.targetAddress);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getContext(), R.string.address_copied, Toast.LENGTH_SHORT).show();
            return true;
        });
        binding.txtDriverPhone.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + viewModel.selectedOrder.driver.cellPhone));
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        });
        binding.txtDriverPhone.setOnLongClickListener(v -> {
            ClipData clipData = ClipData.newPlainText("Source Text", viewModel.selectedOrder.driver.cellPhone);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getContext(), R.string.phone_copied, Toast.LENGTH_SHORT).show();
            return true;
        });
        binding.txtRetryOrder.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), ActivityOrder.class);
            intent.putExtra(ActivityOrder.ORDER_ID, viewModel.orderId);
            intent.putExtra(ActivityOrder.RECREATE, true);
            startActivity(intent);
        });
    }

    private void getOrderDetails() {
        if (detailMediator == null) {
            detailMediator = viewModel.getOrderDetails();
            detailMediator.observe(this, networkResponse -> {
                ActivityHome.ORDER_ID = 0;
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        isRequestDone = false;
                        showProgress(true);
                        break;
                    case FAILURE:
                        isRequestDone = false;
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                isRequestDone = true;
                                setOrder(networkResponse.getApiResponse().data);
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                isRequestDone = false;
                                showConnectionErrorMessage();
                                break;
                            default:
                                isRequestDone = false;
                                showMessage(networkResponse.getApiResponse().status.message);
                                break;
                        }
                        showProgress(false);
                        break;
                    default:
                        isRequestDone = false;
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                }
            });
        } else {
            viewModel.getOrderDetails();
        }
    }

    private void deleteOrder() {
        if (deleteMediator == null) {
            deleteMediator = viewModel.deleteOrder();
            deleteMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress(true);
                        break;
                    case FAILURE:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                    case SUCCESS:
                        showProgress(false);
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                ActivityHome.REFRESH_NEED = true;
                                mListener.onDeleteOrder();
//                                setOrder(networkResponse.getApiResponse().data);
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showConnectionErrorMessage();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                break;
                        }
                        break;
                    default:
                        showProgress(false);
                        showConnectionErrorMessage();
                        break;
                }
            });
        } else {
            viewModel.deleteOrder();
        }
    }

    protected void setOrder(Order order) {
        viewModel.selectedOrder = order;
        binding.setOrder(order);
        setupImageViewers(order);
        setupActionMenuItems(order);
        mListener.showRefreshOption(false);
        if (isMapReady) {
            showMarkers();
        }
    }

    private void setupActionMenuItems(Order order) {
        if (order.orderState < Order.STATE_CONFIRMED) {
            mListener.showEditOption(true);
            mListener.showDeleteOption(true);
        } else if (order.orderState < Order.STATE_ALLOCATED) {
            mListener.showEditOption(false);
            mListener.showDeleteOption(true);
        } else {
            mListener.showEditOption(false);
            mListener.showDeleteOption(false);
        }
    }

    private void setupImageViewers(Order order) {
        if (order.driver != null) {
            if (!isNullOrEmpty(order.driver.profileImageUrl)) {
                Utils.glideCircularImage(getContext(), binding.imgDriverImage,
                        order.driver.profileImageUrl, R.drawable.ic_placeholder_profile);
                driverImageUrls.add(new CustomImage(order.driver.profileImageUrl,
                        getActivity().getString(R.string.driver_image_description)));
            }
            if (!isNullOrEmpty(order.driver.car.carImageUrl)) {
                Utils.glideCircularImage(getContext(), binding.imgCarImage,
                        order.driver.car.carImageUrl, R.drawable.ic_placeholder_car);
                driverImageUrls.add(new CustomImage(order.driver.car.carImageUrl,
                        getActivity().getString(R.string.car_image_description)));
            }
        }

        if (!isNullOrEmpty(order.deliverImageUrl)) {
            Utils.glideRoundedCornerImage(getContext(), binding.imgDeliveryImage,
                    order.deliverImageUrl, R.drawable.ic_image_placeholder);
            deliveryImageUrls.add(new CustomImage(order.deliverImageUrl,
                    getActivity().getString(R.string.deliver_image_description)));
        }

        if (!isNullOrEmpty(order.loadImageUrl)) {
            Utils.glideRoundedCornerImage(getContext(), binding.imgLoadingImage,
                    order.loadImageUrl, R.drawable.ic_image_placeholder);
            deliveryImageUrls.add(new CustomImage(order.loadImageUrl,
                    getActivity().getString(R.string.loading_image_description)));
        }
    }

    private void clearImageViewers() {
        driverImageUrls.clear();
        deliveryImageUrls.clear();
        binding.imgDriverImage.setImageResource(R.drawable.ic_placeholder_profile);
        binding.imgCarImage.setImageResource(R.drawable.ic_placeholder_car);
        binding.imgLoadingImage.setImageResource(R.drawable.ic_image_placeholder);
        binding.imgDeliveryImage.setImageResource(R.drawable.ic_image_placeholder);
    }

    private void showDeleteOrderDialog() {
        if (deleteOrderDialog == null) {
            deleteOrderBinding = DataBindingUtil.inflate(getLayoutInflater(),
                    R.layout.dialog_alert, null, false);
            deleteOrderDialog = new Dialog(getContext());
            deleteOrderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // removes dialog header
            deleteOrderDialog.setContentView(deleteOrderBinding.getRoot());
            deleteOrderBinding.txtYes.setOnClickListener(v -> {
                deleteOrder();
                deleteOrderDialog.dismiss();
            });
            deleteOrderBinding.txtNo.setOnClickListener(v -> deleteOrderDialog.dismiss());
        }
        deleteOrderBinding.txtMessage.setText(getString(R.string.would_you_like_delete_this_order));
        deleteOrderDialog.show();
    }

    private void showProgress(boolean show) {
        mListener.showProgress(show);
    }

    private void showConnectionErrorMessage() {
        showRefreshOption();
        showMessage(getString(R.string.connection_error));
    }

    private void showRefreshOption() {
        mListener.showRefreshOption(true);
        mListener.showEditOption(false);
        mListener.showDeleteOption(false);
    }

    protected void initMap() {
        binding.mapView.onCreate(savedInstanceState);
        binding.mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        binding.mapView.getMapAsync(mMap -> {
            googleMap = mMap;
            isMapReady = true;
            googleMap.getUiSettings().setCompassEnabled(false);
            googleMap.getUiSettings().setRotateGesturesEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);


//            int markerSizeInPixel = (int) getResources().getDimension(R.dimen.marker_size);
//            googleMap.setPadding(markerSizeInPixel/2, markerSizeInPixel, markerSizeInPixel/2, 0);

//            googleMap.getUiSettings().setAllGesturesEnabled(false);
            googleMap.setOnMarkerClickListener(marker -> {
                if (isZoomed) {
                    fitToBounds();
                } else {
                    if (marker.equals(sourceMarker))
                        goToOrigin();
                    else if (marker.equals(targetMarker))
                        goToTarget();
                }
                return false;
            });
            if (isRequestDone)
                showMarkers();
        });
    }

    protected void showMarkers() {
        GeoLoc sourceLocation = viewModel.selectedOrder.sourceLocation;
        if (!isValidLocation(sourceLocation)) {
            sourceLocation = new GeoLoc(viewModel.selectedOrder.sourceCity.latitude, viewModel.selectedOrder.sourceCity.longitude);
        }
        GeoLoc targetLocation = viewModel.selectedOrder.targetLocation;
        if (!isValidLocation(targetLocation)) {
            targetLocation = new GeoLoc(viewModel.selectedOrder.targetCity.latitude, viewModel.selectedOrder.targetCity.longitude);
        }
        if (!(isValidLocation(sourceLocation) && isValidLocation(targetLocation))) {
            return;
        }
        sourceLatLng = new LatLng(sourceLocation.latitude, sourceLocation.longitude);
        targetLatLng = new LatLng(targetLocation.latitude, targetLocation.longitude);
        addMarkersToMap();
        fitToBounds();
    }

    private void addMarkersToMap() {
        originIcon = BitmapDescriptorFactory.fromBitmap(
                Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_marker_source_selected));

        targetIcon = BitmapDescriptorFactory.fromBitmap(
                Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.ic_map_marker_target_selected));

        targetMarker = googleMap.addMarker(new MarkerOptions()
                .position(targetLatLng)
                .icon(targetIcon));

        sourceMarker = googleMap.addMarker(new MarkerOptions()
                .position(sourceLatLng)
                .icon(originIcon));
    }

    private void goToOrigin() {
        if (isValidLocation(sourceLatLng)) {
            if (sourceCameraPosition == null) {
                sourceCameraPosition = new CameraPosition.Builder().target(sourceLatLng).zoom(zoomLevel).build();
            }
            binding.mapView.post(() -> {
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(sourceCameraPosition));
            });
            isZoomed = true;
        }
    }

    private void goToTarget() {
        if (isValidLocation(sourceLatLng)) {
            if (targetCameraPosition == null) {
                targetCameraPosition = new CameraPosition.Builder().target(targetLatLng).zoom(zoomLevel).build();
            }
            binding.mapView.post(() -> {
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(targetCameraPosition));
            });
            isZoomed = true;
        }
    }

    private void fitToBounds() {
        int markerSize = (int) getResources().getDimension(R.dimen.marker_size);
        int width = binding.mapView.getMeasuredWidth() - (int) (markerSize * 0.75);
        int height = binding.mapView.getMeasuredHeight() - (int) (markerSize * 1.75);
        binding.mapView.post(() -> {
            LatLngBounds bounds = new LatLngBounds.Builder()
                    .include(sourceMarker.getPosition())
                    .include(targetMarker.getPosition())
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, 16));
            isZoomed = false;
        });
    }

    public void onRefresh() {
        reload();
        getOrderDetails();
    }

    public void onDelete() {
        showDeleteOrderDialog();
    }

    public void onEdit() {
        mListener.onEditOrder(viewModel.orderId);
    }

    public void reload() {
        if (googleMap == null) {
            initMap();
        } else {
            googleMap.clear();
        }
        sourceCameraPosition = null;
        targetCameraPosition = null;
        sourceLatLng = null;
        targetLatLng = null;
        clearImageViewers();
        viewModel.selectedOrder = null;
        binding.setOrder(null);
        getOrderDetails();
//        mListener.changeToolbarTitle(getString(R.string.order_details));
    }

    public void onBackPressed() {
//        showProgress();
//        viewModel.notifications.get(viewModel.selectedPosition).driverPrice =
//                viewModel.selectedOrder.driverPrice;
//        originLatLng = null;
//        destinationLatLng = null;
//        originCameraPosition = null;
//        destinationCameraPosition = null;
//        viewModel.selectedOrder = null;
    }


    private void showDescriptionDialog(String address) {
        if (Utils.isNullOrEmpty(address))
            return;
        if (descriptionDialog == null) {
            descriptionBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dialog_description, null, false);
            descriptionDialog = new Dialog(getContext());
            descriptionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // removes dialog header
            descriptionDialog.setContentView(descriptionBinding.getRoot());
            descriptionBinding.txtOk.setOnClickListener(v -> {
                descriptionDialog.dismiss();
            });
        }
        descriptionBinding.txtDescription.setText(address);
        descriptionDialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (binding.mapView != null) {
            binding.mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (binding.mapView != null) {
            binding.mapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (binding.mapView != null) {
            binding.mapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (binding.mapView != null) {
            binding.mapView.onLowMemory();
        }
    }

    public interface OnFragmentInteractionListener {

        void changeToolbarTitle(String title);

        void showSortingOptions(boolean show);

        void showProgress(boolean show);

        void showEditOption(boolean show);

        void showDeleteOption(boolean show);

        void showRefreshOption(boolean show);

        void onDeleteOrder();

        void onEditOrder(int orderId);
    }
}
