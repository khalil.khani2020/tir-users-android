package com.tir.user.home.list;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.tir.user.R;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.model.room.entity.City;

import java.util.ArrayList;
import java.util.List;

public class ArrayAdapterCities extends ArrayAdapter {

    private final List<City> dataList;
    private final int itemLayout;
    private final RepositoryBasic repositoryBasic;
    private final ListFilter listFilter = new ListFilter();
    private final AutoCompleteTextView autoCompleteTextView;
    private City city;
    private OnAdapterInteractions mListener;
    private boolean isClickEvent;

    public ArrayAdapterCities(Context context, List<City> storeDataLst,
                              RepositoryBasic repository, AutoCompleteTextView autoCompleteTextView, OnAdapterInteractions listener) {
        super(context, R.layout.suggestions, storeDataLst);
        this.itemLayout = R.layout.suggestions;
        this.dataList = storeDataLst;
        this.repositoryBasic = repository;
        this.autoCompleteTextView = autoCompleteTextView;
        this.mListener = listener;
        autoCompleteTextView.setOnItemClickListener((adapterView, view, i, l) -> {
            city = dataList.get(i);
            isClickEvent = true;
            autoCompleteTextView.setText(city.name);
            autoCompleteTextView.setSelection(autoCompleteTextView.getText().length());
            mListener.onCitySelected(city);
        });
//        autoCompleteTextView.setOnFocusChangeListener((v, hasFocus) -> {
//            if (!hasFocus) {
//                if (dataList.size() == 1 && autoCompleteTextView.getText().toString().trim().equals(dataList.get(0).name)) {
//                    city = dataList.get(0);
//                    mListener.onCitySelected(city);
//                }
//            }
//        });
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public City getItem(int position) {
        return dataList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        }
        TextView strName = view.findViewById(R.id.txt_suggestion);
        strName.setText(getItem(position).name);
        return view;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return listFilter;
    }

    public City setCityById(int cityId) {
        city = repositoryBasic.getCity(cityId);
        if (city != null) {
            autoCompleteTextView.setText(city.name);
            mListener.onCitySelected(city);
        }
        return city;
    }

    public City getCity() {
        return city;
    }

    class ListFilter extends Filter {
        final FilterResults results = new FilterResults();

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            if (!(prefix == null || prefix.length() == 0)) {
                List<City> matchValues = repositoryBasic.searchCity(prefix.toString());
                results.values = matchValues;
                results.count = matchValues.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                if (autoCompleteTextView.getText().toString().trim().isEmpty()) {
                    results.count = 0;
                    results.values = null;
                    city = null;
                } else {
                    dataList.clear();
                    dataList.addAll((ArrayList<City>) results.values);
                }
            }
            if (results.count > 0) {
                notifyDataSetChanged();
                if (results.count == 1 && autoCompleteTextView.getText().toString().trim().equals(dataList.get(0).name)) {
                    if (isClickEvent) {
                        isClickEvent = false;
                    } else {
                        city = dataList.get(0);
                        mListener.onCitySelected(city);
                    }
                }
            } else {
                city = null;
                notifyDataSetInvalidated();
                if (autoCompleteTextView.getText().length() > 1) {
                    autoCompleteTextView.setError(getContext().getString(R.string.city_not_found), null);
                    autoCompleteTextView.requestFocus();
                }
            }
        }
    }

    public interface OnAdapterInteractions {
        void onCitySelected(City city);
    }

}
