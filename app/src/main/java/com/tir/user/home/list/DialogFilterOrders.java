package com.tir.user.home.list;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;

import com.tir.user.R;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.DialogFilterOrderBinding;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.OrderFilter;
import com.tir.user.repository.model.room.entity.CarType;
import com.tir.user.repository.model.room.entity.City;
import com.tir.user.repository.model.room.entity.OrderState;
import com.tir.user.repository.model.room.entity.ShipmentType;
import com.tir.user.repository.model.room.entity.WeightRange;
import com.tir.user.views.CustomDatePicker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by khani on 06/03/2018.
 */

public class DialogFilterOrders extends Dialog
        implements CustomDatePicker.OnDateTimeSetListener,
        ArrayAdapterCities.OnAdapterInteractions {


    private final BaseViewModel viewModel;
    private final DialogFilterOrderBinding binding;
    private OrderFilter filter;
    private final List<City> sourceCitiesArray = new ArrayList<>();
    private final List<City> destinationCitiesArray = new ArrayList<>();
    private List<WeightRange> weightRanges = new ArrayList<>();
    private List<ShipmentType> shipmentTypes = new ArrayList<>();
    private List<CarType> carTypes = new ArrayList<>();
    private List<OrderState> orderStates = new ArrayList<>();
    private City sourceCity;
    private City destinationCity;
    private String loadingDate = "";
    private CustomDatePicker datePicker;
    private final OnFilterListener mListener;
    private boolean isPrevious;

    public DialogFilterOrders(@NonNull Context context, BaseViewModel viewModel,
                              OrderFilter filter, OnFilterListener listener, boolean isPrevious) {
        super(context);
        this.viewModel = viewModel;
        this.filter = filter;
        this.mListener = listener;
        this.isPrevious = isPrevious;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                R.layout.dialog_filter_order, null, false);
        setContentView(binding.getRoot());
        initAdapters();
        setListeners();

    }

    public void showDialog() {
        show();
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void initAdapters() {
        ArrayAdapterCities sAdapter = new ArrayAdapterCities(getContext(),
                sourceCitiesArray, viewModel.getBaseRepository(), binding.aTxtSourceCity, this);
        binding.aTxtSourceCity.setAdapter(sAdapter);
//        binding.skill.setThreshold(1);
        binding.aTxtSourceCity.setOnItemClickListener((adapterView, view, i, l) -> {
            sourceCity = sourceCitiesArray.get(i);
            binding.aTxtSourceCity.setText(sourceCity.name);
        });


        ArrayAdapterCities dAdapter = new ArrayAdapterCities(getContext(),
                destinationCitiesArray, viewModel.getBaseRepository(), binding.aTxtDestinationCity, this);
        binding.aTxtDestinationCity.setAdapter(dAdapter);
//        binding.skill.setThreshold(1);
        binding.aTxtDestinationCity.setOnItemClickListener((adapterView, view, i, l) -> {
            destinationCity = destinationCitiesArray.get(i);
            binding.aTxtDestinationCity.setText(destinationCity.name);
        });

        weightRanges = viewModel.getBaseRepository().getWeights();
        shipmentTypes = viewModel.getBaseRepository().getShipmentTypes();
        carTypes = viewModel.getBaseRepository().getCarTypes();
        orderStates = isPrevious ?
                viewModel.getBaseRepository().getOrderStatesGreaterThan(Order.STATE_COMPLETED)
                : viewModel.getBaseRepository().getOrderStates();

        ArrayAdapter<String> weightsAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_simple);
        weightsAdapter.add(getContext().getString(R.string.order_weight));
        for (WeightRange w : weightRanges) {
            weightsAdapter.add(w.name);
        }
        binding.spinnerWeightRange.setAdapter(weightsAdapter);

        ArrayAdapter<String> shipmentTypesAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_simple);
        shipmentTypesAdapter.add(getContext().getString(R.string.shipment_type));
        for (ShipmentType s : shipmentTypes) {
            shipmentTypesAdapter.add(s.name);
        }
        binding.spinnerShipmentType.setAdapter(shipmentTypesAdapter);

        ArrayAdapter<String> carTypesAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_simple);
        carTypesAdapter.add(getContext().getString(R.string.car_type));
        for (CarType c : carTypes) {
            carTypesAdapter.add(c.name);
        }
        binding.spinnerCarType.setAdapter(carTypesAdapter);

        ArrayAdapter<String> OrderStatusesAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_simple);
        OrderStatusesAdapter.add(getContext().getString(R.string.order_state));
        for (OrderState os : orderStates) {
            OrderStatusesAdapter.add(os.name);
        }
        binding.spinnerOrderStatus.setAdapter(OrderStatusesAdapter);
    }

    private void setListeners() {
        binding.imgPickDate.setOnClickListener(v -> {
            if (datePicker == null) {
                datePicker = new CustomDatePicker(getContext(), this);
            }
            datePicker.showDatePicker(loadingDate, 0);
        });

        binding.imgClearDate.setOnClickListener(v -> {
            binding.txtLoadingDate.setText("");
            binding.imgClearDate.setVisibility(View.GONE);
            loadingDate = "";
        });

        binding.txtOk.setOnClickListener(v -> {
            if (validateInputs()) return;
            mListener.onFilter(collectFilter());
            dismiss();
        });

        binding.txtCancel.setOnClickListener(v -> dismiss());

    }

    private boolean validateInputs() {
        String source = binding.aTxtSourceCity.getText().toString().trim();
        if (!source.isEmpty()) {
            if (sourceCitiesArray.size() == 0 ||
                    (sourceCitiesArray.size() == 1 && !sourceCitiesArray.get(0).name.equals(source))) {
                binding.aTxtSourceCity.setError(getContext().getString(R.string.city_not_found));
                binding.aTxtSourceCity.requestFocus();
                sourceCity = null;
                return true;
            }
        } else {
            sourceCity = null;
        }

        String destination = binding.aTxtDestinationCity.getText().toString().trim();
        if (!destination.isEmpty()) {
            if (destinationCitiesArray.size() == 0 ||
                    (destinationCitiesArray.size() == 1 && !destinationCitiesArray.get(0).name.equals(destination))) {
                binding.aTxtDestinationCity.setError(getContext().getString(R.string.city_not_found));
                binding.aTxtDestinationCity.requestFocus();
                destinationCity = null;
                return true;
            }
        } else {
            destinationCity = null;
        }
        return false;
    }

    private OrderFilter collectFilter() {
        if (filter == null) {
            filter = new OrderFilter();
        }
        int weightPosition = binding.spinnerWeightRange.getSelectedItemPosition();
        int shipmentTypePosition = binding.spinnerShipmentType.getSelectedItemPosition();
        int carTypePosition = binding.spinnerCarType.getSelectedItemPosition();
        int statusPosition = binding.spinnerOrderStatus.getSelectedItemPosition();

        filter.sourceCityId = sourceCity == null ? null : sourceCity.id;
        filter.targetCityId = destinationCity == null ? null : destinationCity.id;
        filter.weightRangeId = weightPosition == 0 ? null : weightRanges.get(weightPosition - 1).id;
        filter.shipmentTypeId = shipmentTypePosition == 0 ? null : shipmentTypes.get(shipmentTypePosition - 1).id;
        filter.carTypeId = carTypePosition == 0 ? null : carTypes.get(carTypePosition - 1).id;
        filter.orderState = statusPosition == 0 ? null : orderStates.get(statusPosition - 1).code;
        filter.loadingDate = loadingDate;

        return filter;
    }

    public void clearFilter() {
        // TODO: 5/4/2018 complete this
    }

    @Override
    public void onDateSet(String formattedDate, long timeStamp, int requestCode) {
        binding.txtLoadingDate.setText(formattedDate);
        binding.imgClearDate.setVisibility(View.VISIBLE);
        loadingDate = formattedDate;
    }

    @Override
    public void onCitySelected(City city) {

    }

    public interface OnFilterListener {
        void onFilter(OrderFilter filter);
    }

}
