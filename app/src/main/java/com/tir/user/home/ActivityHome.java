package com.tir.user.home;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceOnClickListener;
import com.tir.user.R;
import com.tir.user.base.BaseActivity;
import com.tir.user.base.BaseViewModel;
import com.tir.user.databinding.ActivityHomeBinding;
import com.tir.user.databinding.DialogDownloadProgressBinding;
import com.tir.user.databinding.DialogUpgradeAppBinding;
import com.tir.user.home.detail.FragmentOrderDetail;
import com.tir.user.home.list.AdapterViewPagerOrders;
import com.tir.user.home.list.FragmentCurrent;
import com.tir.user.home.list.FragmentPrevious;
import com.tir.user.neworder.ActivityOrder;
import com.tir.user.rating.ActivityRating;
import com.tir.user.repository.model.AppUpdateLinks;
import com.tir.user.repository.model.Notif;
import com.tir.user.repository.model.Order;
import com.tir.user.services.ServiceDownload;
import com.tir.user.services.ServiceFirebaseMessaging;
import com.tir.user.webview.ActivityWebView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import static com.tir.user.app.Const.Endpoints.URL_Play_Services_Google_Play;
import static com.tir.user.utils.Utils.isNullOrEmpty;


public class ActivityHome extends BaseActivity
        implements FragmentCurrent.OnFragmentInteractionListener,
        FragmentPrevious.OnFragmentInteractionListener,
        FragmentOrderDetail.OnFragmentInteractionListener,
        ServiceDownload.OnDownloadListener {

    private static final int RIGHT_TO_LEFT = 0;
    private static final int LEFT_TO_RIGHT = 1;

    public static String UPDATE_LINKS = "UpdateLinks";
    public static String UN_RATED_ORDER = "UnRatedOrder";
    private static final int STORAGE_PERMISSION_REQUEST_CODE = 102;
    public static boolean REFRESH_NEED = false;
    public static int ORDER_ID = 0;
    private ActivityHomeBinding binding;
    private FragmentOrderDetail detailFrag;
    private ViewModelHome viewModel;
    private String downloadUrl;
    private DialogDownloadProgressBinding dialogDownloadProgressBinding;
    private Dialog dialogDownloadProgress;
    private int currentPosition = 1;
    private AdapterViewPagerOrders pagerAdapter;
    MenuItem itemRefresh;
    MenuItem itemDelete;
    MenuItem itemEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = findViewById(R.id.content_frame);
        binding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.activity_home, contentFrameLayout, true);
        setSupportActionBar(binding.toolbar);
        initToolbarAndNavigationDrawer(binding.toolbar, binding.toolbarShadow);
        ////////////////////////////////////////////////////////////////////////////////////////////
        viewModel = ViewModelProviders.of(this).get(ViewModelHome.class);
        checkFcmToken(viewModel);
        isFirstLogin();
        FirebaseInstanceId.getInstance().getToken();
        subscribeToTopic();


        binding.space.initWithSaveInstanceState(savedInstanceState);
        binding.space.addSpaceItem(new SpaceItem(getString(R.string.previous_orders), R.drawable.ic_cares));
        binding.space.addSpaceItem(new SpaceItem(getString(R.string.current_orders), R.drawable.ic_cares));
        binding.space.showTextOnly();
        binding.space.changeCurrentItem(1);
        setupListeners();
        setupPager(0);
        if (getIntent().hasExtra(UPDATE_LINKS)) {
            AppUpdateLinks appUpdateLinks = getIntent().getExtras().getParcelable(UPDATE_LINKS);
            if (ServiceDownload.isRunning) {
                ServiceDownload.setDownloadListener(this);
                showDownloadProgressDialog();
            } else {
                if (appUpdateLinks.isPlayServicesUpdateNeeded) {
                    showPlayUpdateDialog(appUpdateLinks);
                } else if (appUpdateLinks.isUpdateAvailable) {
                    showAppUpdateDialog(appUpdateLinks);
                }
            }
        } else if (getIntent().hasExtra(UN_RATED_ORDER)) {
            Order unRatedOrder = (Order) getIntent().getExtras().getSerializable(UN_RATED_ORDER);
            if (unRatedOrder.seenByCustomer) {
                Intent intent = new Intent(getContext(), ActivityRating.class);
                intent.putExtra(UN_RATED_ORDER, unRatedOrder);
                startActivity(intent);
            }
        }

        if (getIntent().hasExtra(ServiceFirebaseMessaging.NOTIFICATION)) {
            Notif notif = getIntent().getParcelableExtra(ServiceFirebaseMessaging.NOTIFICATION);
            if (notif.clickAction.equals(Notif.ClickActions.OrderDetail)) {
                int orderId = notif.extractOrderId();
                if (orderId > 0) {
                    viewModel.orderId = orderId;
                    showOrderDetail();
                }
            } else if (notif.clickAction.equals(Notif.ClickActions.Blog)) {
                 Intent intent = new Intent(getContext(), ActivityWebView.class);
                 intent.putExtra(ServiceFirebaseMessaging.NOTIFICATION, notif);
                 startActivity(intent);
            }
        }
    }

    private void setupPager(int currentTab) {
        pagerAdapter = new AdapterViewPagerOrders(getSupportFragmentManager(), this);
        binding.content.viewPager.setAdapter(pagerAdapter);
        binding.content.viewPager.setOffscreenPageLimit(4);
        binding.content.viewPager.setRotationY(180);
        binding.content.viewPager.setCurrentItem(currentTab);
        setupPagerWithBottomBar();
    }

    private void setupPagerWithBottomBar() {
        binding.content.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                binding.space.changeCurrentItem(inverse(i));
                setSortByTitle();
                setFilterByTitle();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


        binding.space.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                startActivity(new Intent(getContext(), ActivityOrder.class));
//                Toast.makeText(getContext(), "Center Button Clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                binding.content.viewPager.setCurrentItem(inverse(itemIndex));
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                binding.content.viewPager.setCurrentItem(inverse(itemIndex));
            }
        });
    }

    private int inverse(int i) {
        if (i == 0)
            return 1;
        else
            return 0;
    }

    private void setupListeners() {
        binding.sort.setOnClickListener(view -> showDialogSort());
        binding.filter.setOnClickListener(view -> showDialogFilter());
        binding.imgClearFilter.setOnClickListener(view -> clearFilter());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (REFRESH_NEED) {
            if (binding.content.viewPager.getVisibility() == View.VISIBLE) {
                refreshLists();
                if (ORDER_ID > 0) {
                    viewModel.orderId = ORDER_ID;
                    showOrderDetail();
                }
            } else {
                if (ORDER_ID > 0)
                    viewModel.orderId = ORDER_ID;
                detailFrag.onRefresh();
            }
        }
    }

    private void showListFrag() {
        showListOptions(true);
        showActionMenuItems(false);
        binding.content.viewPager.setVisibility(View.VISIBLE);
        binding.content.containerDetail.setVisibility(View.GONE);
        if (REFRESH_NEED) {
            refreshLists();
        }
        changeToolbarTitle(getString(R.string.orders));
    }

    private void refreshLists() {
        if (pagerAdapter.fragmentCurrent != null)
            pagerAdapter.fragmentCurrent.refresh();
        if (pagerAdapter.fragmentPrevious != null)
            pagerAdapter.fragmentPrevious.refresh();
    }

    private void showDetailFrag() {
        showListOptions(false);
        binding.content.viewPager.setVisibility(View.GONE);
        binding.content.containerDetail.setVisibility(View.VISIBLE);
        changeToolbarTitle(getString(R.string.order_detail));
    }

    private void showListOptions(boolean show) {
        if (show) {
            showSortingOptions(true);
            binding.space.setVisibility(View.VISIBLE);
        } else {
            showSortingOptions(false);
            binding.space.setVisibility(View.GONE);
        }
    }

    private void showActionMenuItems(boolean show) {
        itemEdit.setVisible(show);
        itemDelete.setVisible(show);
        itemRefresh.setVisible(show);
    }

    private void showDialogSort() {
        if (isCurrentOrdersFrag())
            pagerAdapter.fragmentCurrent.showDialogSort();
        else
            pagerAdapter.fragmentPrevious.showDialogSort();
    }

    private void showDialogFilter() {
        if (isCurrentOrdersFrag())
            pagerAdapter.fragmentCurrent.showDialogFilter();
        else
            pagerAdapter.fragmentPrevious.showDialogFilter();
    }

    private void clearFilter() {
        if (isCurrentOrdersFrag())
            pagerAdapter.fragmentCurrent.clearFilter();
        else
            pagerAdapter.fragmentPrevious.clearFilter();
    }

    private boolean isCurrentOrdersFrag() {
        if (binding.content.viewPager.getCurrentItem() == 0)
            return true;
        else
            return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_home, menu);
        itemEdit = menu.findItem(R.id.edit);
        itemDelete = menu.findItem(R.id.delete);
        itemRefresh = menu.findItem(R.id.refresh);
        itemEdit.setVisible(false);
        itemDelete.setVisible(false);
        itemRefresh.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                detailFrag.onRefresh();
                return true;
            case R.id.edit:
                detailFrag.onEdit();
                return true;
            case R.id.delete:
                detailFrag.onDelete();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showSortingOptions(boolean show) {
        if (show) {
            binding.sortingOptions.setVisibility(View.VISIBLE);
        } else {
            binding.sortingOptions.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress(boolean show) {
        if (show)
            binding.content.progressBar.setVisibility(View.VISIBLE);
        else
            binding.content.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEditOption(boolean show) {
        itemEdit.setVisible(show);
    }

    @Override
    public void showDeleteOption(boolean show) {
        itemDelete.setVisible(show);
    }

    @Override
    public void showRefreshOption(boolean show) {
        itemRefresh.setVisible(show);
    }

    @Override
    public void onDeleteOrder() {
        // TODO: 10/12/2018 : check this
        onBackPressed();
        Toast.makeText(getContext(), getString(R.string.order_deleted_successfully), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onEditOrder(int orderId) {
        Intent intent = new Intent(getContext(), ActivityOrder.class);
        intent.putExtra(ActivityOrder.ORDER_ID, orderId);
        startActivity(intent);
    }

    @Override
    public void showOrderDetail() {
        if (detailFrag == null) {
            detailFrag = new FragmentOrderDetail();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container_detail, detailFrag, detailFrag.getClass().getSimpleName())
                    .addToBackStack(detailFrag.getClass().getSimpleName())
                    .commit();
        } else {
            detailFrag.reload();
        }
        showDetailFrag();
    }

    @Override
    public void setSortByTitle() {
        String title;
        if (isCurrentOrdersFrag())
            title = pagerAdapter.fragmentCurrent.getSortByTitles();
        else
            title = pagerAdapter.fragmentPrevious.getSortByTitles();
        binding.txtSort.setText(title);
    }

    @Override
    public void setFilterByTitle() {
        String title;
        if (isCurrentOrdersFrag())
            title = pagerAdapter.fragmentCurrent.getFilterTitles();
        else
            title = pagerAdapter.fragmentPrevious.getFilterTitles();
        binding.txtFilter.setText(title);
        if (title.equals("")) {
            binding.imgClearFilter.setVisibility(View.GONE);
        } else {
            binding.imgClearFilter.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void changeToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onBackPressed() {
        if (binding.content.progressBar.getVisibility() == View.VISIBLE)
            return;
        if (closeDrawer())
            return;
        if (binding.content.containerDetail.getVisibility() == View.VISIBLE) {
            showListFrag();
            detailFrag.onBackPressed();
            // TODO: 27/11/2018 check this line:
//            currentFrag.onBackPressed();
        } else if (currentPosition == 0) {
            super.onBackPressed();
        } else {
            finish();
            System.gc();
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        return this.viewModel;
    }

    public void downloadFile() {
        if (checkStoragePermission()) {
            startDownload();
        } else {
            requestStoragePermission();
        }
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startDownload();
                } else {
                    Toast.makeText(getContext(), R.string.give_needed_permissions_to_app, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean checkStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void startDownload() {
        Intent intent = new Intent(this, ServiceDownload.class);
        intent.putExtra(ServiceDownload.DOWNLOAD_URL, downloadUrl);
        startService(intent);
        ServiceDownload.setDownloadListener(this);
        showDownloadProgressDialog();
    }

    @Override
    protected void onStop() {
        super.onStop();
        ServiceDownload.removeDownloadListener();
        dialogDownloadProgress = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ServiceDownload.removeDownloadListener();
    }

    @Override
    public void onDownloadProgress(int progress, String downloadedAmount) {
        runOnUiThread(() -> {
            if (dialogDownloadProgressBinding != null) {
                dialogDownloadProgressBinding.progressBarDownload.setProgress(progress);
                dialogDownloadProgressBinding.txtDownloaded.setText(downloadedAmount);
            }
        });
    }

    @Override
    public void onDownloadComplete() {
        runOnUiThread(() -> {
            if (dialogDownloadProgress != null) {
                dialogDownloadProgress.cancel();
            }
            finish();
        });
    }

    @Override
    public void onDownloadFailed() {
        runOnUiThread(() -> {
            if (dialogDownloadProgress != null) {
                dialogDownloadProgress.cancel();
            }
            finish();
        });
    }

    private void showAppUpdateDialog(AppUpdateLinks appUpdateLinks) {
        // TODO: 5/14/2018 complete this
        DialogUpgradeAppBinding dialogUpgradeAppBinding = DataBindingUtil.inflate(
                getLayoutInflater(), R.layout.dialog_upgrade_app, null, false);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogUpgradeAppBinding.getRoot());
        dialog.setCancelable(false);
        dialogUpgradeAppBinding.txtMessage.setText(R.string.new_version_released);

        if (!isNullOrEmpty(appUpdateLinks.playLink)) {
            dialogUpgradeAppBinding.cvGooglePlay.setVisibility(View.VISIBLE);
            dialogUpgradeAppBinding.cvGooglePlay.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(appUpdateLinks.playLink));
                startActivity(intent);
                finish();
            });
        }

        if (!isNullOrEmpty(appUpdateLinks.bazarLink)) {
            dialogUpgradeAppBinding.cvCafeBazar.setVisibility(View.VISIBLE);
            dialogUpgradeAppBinding.cvCafeBazar.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(appUpdateLinks.bazarLink));
                startActivity(intent);
                finish();
            });
        }

        if (!isNullOrEmpty(appUpdateLinks.directLink)) {
            dialogUpgradeAppBinding.cvDirectDownload.setVisibility(View.VISIBLE);
            dialogUpgradeAppBinding.cvDirectDownload.setOnClickListener(v -> {
                downloadUrl = appUpdateLinks.directLink;
                downloadFile();
                dialog.dismiss();
            });
        }
        if (!appUpdateLinks.isForceUpdate) {
            dialogUpgradeAppBinding.txtCancel.setVisibility(View.VISIBLE);
            dialogUpgradeAppBinding.txtCancel.setOnClickListener(v -> {
                dialog.dismiss();
            });
        }
        dialog.show();
    }

    protected void showPlayUpdateDialog(AppUpdateLinks appUpdateLinks) {
        DialogUpgradeAppBinding dialogUpgradeAppBinding = DataBindingUtil.inflate(
                getLayoutInflater(), R.layout.dialog_upgrade_app, null, false);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogUpgradeAppBinding.getRoot());
        dialog.setCancelable(false);
        dialogUpgradeAppBinding.txtMessage.setText(R.string.play_services_update_needed);

        dialogUpgradeAppBinding.cvGooglePlay.setVisibility(View.VISIBLE);
        dialogUpgradeAppBinding.cvGooglePlay.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(URL_Play_Services_Google_Play));
            startActivity(intent);
            finish();
        });

        if (!isNullOrEmpty(appUpdateLinks.playServicesUpdateUrl)) {
            dialogUpgradeAppBinding.cvDirectDownload.setVisibility(View.VISIBLE);
            dialogUpgradeAppBinding.cvDirectDownload.setOnClickListener(v -> {
                downloadUrl = appUpdateLinks.playServicesUpdateUrl;
                downloadFile();
                dialog.dismiss();
            });
        }
        dialogUpgradeAppBinding.txtCancel.setText(R.string.cancel);
        dialogUpgradeAppBinding.txtCancel.setVisibility(View.VISIBLE);
        dialogUpgradeAppBinding.txtCancel.setOnClickListener(v -> {
            finish();
        });
        dialog.show();
    }

    private void showDownloadProgressDialog() {
        dialogDownloadProgressBinding = DataBindingUtil.inflate(
                getLayoutInflater(), R.layout.dialog_download_progress, null, false);
        dialogDownloadProgress = new Dialog(this);
        dialogDownloadProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDownloadProgress.setContentView(dialogDownloadProgressBinding.getRoot());
        dialogDownloadProgress.setCancelable(false);
        dialogDownloadProgress.show();
    }

}
