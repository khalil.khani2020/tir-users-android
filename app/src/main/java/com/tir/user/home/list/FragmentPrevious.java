package com.tir.user.home.list;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.tir.user.R;
import com.tir.user.base.BaseFragment;
import com.tir.user.databinding.DialogSortOrderBinding;
import com.tir.user.databinding.FragmentOrdersBinding;
import com.tir.user.home.ViewModelHome;
import com.tir.user.repository.model.OrderFilter;
import com.tir.user.repository.model.api.ApiResponse;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamOrders;
import com.tir.user.repository.model.api.parameter.SortBy;
import com.tir.user.repository.model.api.response.ResponseOrders;

import java.util.ArrayList;

import static com.tir.user.utils.Utils.isNullOrEmpty;


public class FragmentPrevious extends BaseFragment
        implements DialogFilterOrders.OnFilterListener,
        RecyclerAdapterOrders.OnAdapterInteractionsListener {

    private FragmentOrdersBinding binding;
    private OnFragmentInteractionListener mListener;
    private ViewModelHome viewModel;
    private RecyclerAdapterOrders adapterOrders;
    private DialogFilterOrders dialogFilter;
    private Dialog dialogSort;
    private final Handler handler = new Handler();
    MediatorLiveData<NetworkResponse<ResponseOrders>> ordersMediator;
    DialogSortOrderBinding bindingDialogSort;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_orders, container, false);
        binding.relativeLayout.setRotationY(180);
        viewModel = ViewModelProviders.of(getActivity()).get(ViewModelHome.class);
        mListener.changeToolbarTitle(getString(R.string.orders));
        if (viewModel.previousOrders == null) {
            viewModel.previousOrders = new ArrayList<>();
            getOrders();
        }
        setupRecycler();
        setupSwipeRefreshLayout();
        return binding.getRoot();
    }

    private void getOrders() {
        if (ordersMediator == null) {
            ordersMediator = viewModel.getPreviousOrders();
            ordersMediator.observe(this, networkResponse -> {
                switch (networkResponse.getStatus()) {
                    case LOADING:
                        showProgress();
                        break;
                    case FAILURE:
                        showError();
                        break;
                    case SUCCESS:
                        switch (networkResponse.getApiResponse().status.code) {
                            case ApiResponse.COMMON_SUCCESSFUL:
                                viewModel.previousOrders.clear();
                                viewModel.previousOrders.addAll(networkResponse.getApiResponse().data.items);
                                adapterOrders.notifyDataSetChanged();
                                showForm();
                                break;
                            case ApiResponse.COMMON_FAILURE:
                                showError();
                                break;
                            default:
                                showMessage(networkResponse.getApiResponse().status.message);
                                showForm();
                                break;
                        }
                        break;
                    default:
                        showError();
                        break;
                }
            });
        } else {
            viewModel.getPreviousOrders();
        }
    }

    private void setupRecycler() {
        adapterOrders = new RecyclerAdapterOrders(getContext(), viewModel.previousOrders,
                R.layout.recycler_item_order, this, false);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        binding.recyclerView.setAdapter(adapterOrders);
    }

    private void setupSwipeRefreshLayout() {
        binding.swipeRefresh.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorAccent));
        binding.swipeRefresh.setOnRefreshListener(
                () -> getOrders()
        );
    }

    public void refresh() {
        getOrders();
    }

    public void showDialogFilter() {
        if (dialogFilter == null) {
            dialogFilter = new DialogFilterOrders(getContext(), viewModel,
                    viewModel.getPreviousFilter(), this, true);
        }
        dialogFilter.showDialog();
    }

    public void clearFilter() {
        // TODO: 5/4/2018 improve this by implementing dialogFilter's clearFilter method
        dialogFilter = null;
        viewModel.setPreviousFilter(new OrderFilter());
        mListener.setFilterByTitle();
        viewModel.getPreviousOrders();
    }

    public void showDialogSort() {
        if (dialogSort == null) {
            dialogSort = new Dialog(getContext());
            bindingDialogSort = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                    R.layout.dialog_sort_order, null, false);
            dialogSort.requestWindowFeature(Window.FEATURE_NO_TITLE); // removes dialog header
            dialogSort.setContentView(bindingDialogSort.getRoot());
            bindingDialogSort.radioGroupSort.setOnCheckedChangeListener((radioGroup, i) -> {
                switch (i) {
                    case R.id.radio_newest:
                        viewModel.setPreviousSortBy(new SortBy(ParamOrders.Newest, true));
                        break;
                    case R.id.radio_origin:
                        viewModel.setPreviousSortBy(new SortBy(ParamOrders.SourceCity, false));
                        break;
                    case R.id.radio_destination:
                        viewModel.setPreviousSortBy(new SortBy(ParamOrders.TargetCity, false));
                        break;
                    case R.id.radio_shipment_type:
                        viewModel.setPreviousSortBy(new SortBy(ParamOrders.ShipmentType, false));
                        break;
                    case R.id.radio_weight_range:
                        viewModel.setPreviousSortBy(new SortBy(ParamOrders.WeightRange, false));
                        break;
                    case R.id.radio_loading_date:
                        viewModel.setPreviousSortBy(new SortBy(ParamOrders.LoadingDate, true));
                        break;
                    case R.id.radio_car_type:
                        viewModel.setPreviousSortBy(new SortBy(ParamOrders.CarType, false));
                        break;
                    case R.id.radio_order_state:
                        viewModel.setPreviousSortBy(new SortBy(ParamOrders.OrderState, false));
                        break;
                    default:
                        break;
                }
                viewModel.getPreviousOrders();
                mListener.setSortByTitle();
                handler.postDelayed(() -> dialogSort.dismiss(), 200);
            });
        }
        dialogSort.show();
    }

    public String getSortByTitles() {
        SortBy sortBy = viewModel.getPreviousSortBy();
        if (sortBy == null)
            return "";
        switch (sortBy.by) {
            case ParamOrders.Newest:
                return getString(R.string.newest);
            case ParamOrders.SourceCity:
                return getString(R.string.source);
            case ParamOrders.TargetCity:
                return getString(R.string.destination);
            case ParamOrders.ShipmentType:
                return getString(R.string.shipment_type);
            case ParamOrders.WeightRange:
                return getString(R.string.order_weight_ton);
            case ParamOrders.LoadingDate:
                return getString(R.string.loading_date);
            case ParamOrders.CarType:
                return getString(R.string.car_type);
            case ParamOrders.OrderState:
                return getString(R.string.order_state);
            default:
                return "";
        }
    }

    public String getFilterTitles() {
        String titles = "";
        if (viewModel.getPreviousFilter().sourceCityId != null) {
            titles += getString(R.string.source);
            titles += getString(R.string.comma);
        }
        if (viewModel.getPreviousFilter().targetCityId != null) {
            titles += getString(R.string.destination);
            titles += getString(R.string.comma);
        }
        if (viewModel.getPreviousFilter().shipmentTypeId != null) {
            titles += getString(R.string.shipment_type);
            titles += getString(R.string.comma);
        }
        if (viewModel.getPreviousFilter().weightRangeId != null) {
            titles += getString(R.string.order_weight);
            titles += getString(R.string.comma);
        }
        if (viewModel.getPreviousFilter().carTypeId != null) {
            titles += getString(R.string.car_type);
            titles += getString(R.string.comma);
        }
        if (viewModel.getPreviousFilter().orderState != null) {
            titles += getString(R.string.order_state);
            titles += getString(R.string.comma);
        }
        if (!isNullOrEmpty(viewModel.getPreviousFilter().loadingDate)) {
            titles += getString(R.string.loading_date);
        }
        titles = titles.replaceAll(getString(R.string.comma) + "$", "");
        return titles;
    }

    private void showProgress() {
        if (binding.swipeRefresh.isRefreshing()) {
            binding.progressBar.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.GONE);
        } else {
            binding.swipeRefresh.setEnabled(false);
            binding.progressBar.setVisibility(View.VISIBLE);
//            mListener.showSortingOptions(false);
            binding.errorLayout.root.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.GONE);
            binding.txtNoOrder.setVisibility(View.GONE);
        }
    }

    private void showForm() {
        binding.swipeRefresh.setRefreshing(false);
        binding.swipeRefresh.setEnabled(true);
        binding.progressBar.setVisibility(View.GONE);
//        mListener.showSortingOptions(true);
        binding.errorLayout.root.setVisibility(View.GONE);
        binding.recyclerView.setVisibility(View.VISIBLE);
        if (viewModel.previousOrders.size() > 0) {
            binding.txtNoOrder.setVisibility(View.GONE);
        } else {
            binding.txtNoOrder.setVisibility(View.VISIBLE);
        }
    }

    private void showError() {
        binding.swipeRefresh.setRefreshing(false);
        binding.swipeRefresh.setEnabled(false);
        binding.progressBar.setVisibility(View.GONE);
//        mListener.showSortingOptions(true);
        binding.errorLayout.root.setVisibility(View.VISIBLE);
        binding.recyclerView.setVisibility(View.GONE);
        binding.txtNoOrder.setVisibility(View.GONE);
        errorInConnection();
    }

    private void errorInConnection() {
        if (!binding.errorLayout.txtRetry.hasOnClickListeners()) {
            binding.errorLayout.txtRetry.setOnClickListener(view -> viewModel.getPreviousOrders());
        }
    }

    public void onBackPressed() {
        mListener.changeToolbarTitle(getString(R.string.orders));
        if (viewModel.notifyItemChanged) {
            adapterOrders.removeItem(viewModel.selectedPosition);
        }
        showForm();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onFilter(OrderFilter filter) {
        viewModel.setPreviousFilter(filter);
        viewModel.getPreviousOrders();
        mListener.setFilterByTitle();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ordersMediator = null;
    }

    @Override
    public void onRecyclerItemClick(int position) {
//        viewModel.selectedPosition = position;
        viewModel.orderId = viewModel.previousOrders.get(position).id;
        mListener.showOrderDetail();
    }

    public interface OnFragmentInteractionListener {
        void changeToolbarTitle(String title);

        void showSortingOptions(boolean show);

        void showOrderDetail();

        void setSortByTitle();

        void setFilterByTitle();
    }
}