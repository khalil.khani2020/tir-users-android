package com.tir.user.home.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.tir.user.R;
import com.tir.user.databinding.RecyclerItemOrderBinding;
import com.tir.user.repository.model.Order;
import com.tir.user.utils.RecyclerViewAnimator;
import com.tir.user.utils.Utils;

import java.util.List;

/**
 * Created by khani on 08/12/2017.
 */

public class RecyclerAdapterOrders extends RecyclerView.Adapter<RecyclerAdapterOrders.ViewHolder> {

    private Context mContext;
    private final List<Order> list;
    private int layout;
    private OnAdapterInteractionsListener mListener;
    private RecyclerViewAnimator mAnimator;
    private boolean animateItems;

    public RecyclerAdapterOrders(Context context, List<Order> list, int layout,
                                 OnAdapterInteractionsListener listener, boolean animateItems) {
        this.mContext = context;
        this.list = list;
        this.layout = layout;
        this.mListener = listener;
        this.animateItems = animateItems;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (animateItems)
            mAnimator = new RecyclerViewAnimator(recyclerView, RecyclerViewAnimator.VERTICAL);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemOrderBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layout, parent, false);
        if (animateItems)
            mAnimator.onCreateViewHolder(binding.getRoot());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Order order = list.get(position);
        holder.bind(order);
        Utils.glideImage(mContext, holder.getBinding().imageView, order.shipmentType.imageUrl);
//        holder.getBinding().imageView.setImageResource(list.get(position).getIconDrawable());
        setBadge(order, holder.getBinding().imgBadge);
        if (animateItems)
            mAnimator.onBindViewHolder(holder.getBinding().getRoot(), position);
    }

    private void setBadge(Order order, ImageView imgBadge) {
        switch (order.orderState) {
            case Order.STATE_NEW:
                imgBadge.setImageResource(R.drawable.ic_badge_waiting);
                break;
            case Order.STATE_REJECTED:
                imgBadge.setImageResource(R.drawable.ic_badge_reject);
                break;
            case Order.STATE_CONFIRMED:
                imgBadge.setImageResource(R.drawable.ic_badge_pending);
                break;
            case Order.STATE_PENDING:
                imgBadge.setImageResource(R.drawable.ic_badge_pending);
                break;
            case Order.STATE_ALLOCATED:
                imgBadge.setImageResource(R.drawable.ic_badge_driver);
                break;
            case Order.STATE_RECEIVED:
                imgBadge.setImageResource(R.drawable.ic_badge_loaded);
                break;
            case Order.STATE_DELIVERED:
                imgBadge.setImageResource(R.drawable.ic_badge_delivered);
                break;
            case Order.STATE_COMPLETED:
                imgBadge.setImageResource(R.drawable.ic_badge_accept);
                break;
            case Order.STATE_CANCELED:
                imgBadge.setImageResource(R.drawable.ic_badge_cancel);
                break;
            case Order.STATE_DELETED:
                imgBadge.setImageResource(R.drawable.ic_badge_deleted);
                break;
            default:
                imgBadge.setImageResource(R.drawable.ic_badge_waiting);
                break;
        }
        imgBadge.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemOrderBinding binding;

        ViewHolder(RecyclerItemOrderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.getRoot().setOnClickListener(v -> mListener.onRecyclerItemClick(getAdapterPosition()));
        }

        void bind(Order order) {
            binding.setOrder(order);
            binding.executePendingBindings();
        }

        RecyclerItemOrderBinding getBinding() {
            return binding;
        }
    }

    public void removeItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnAdapterInteractionsListener {
        void onRecyclerItemClick(int position);
    }
}
