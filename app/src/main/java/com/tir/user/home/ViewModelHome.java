package com.tir.user.home;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.tir.user.base.BaseViewModel;
import com.tir.user.repository.RepositoryBasic;
import com.tir.user.repository.RepositoryOrder;
import com.tir.user.repository.model.Order;
import com.tir.user.repository.model.OrderFilter;
import com.tir.user.repository.model.api.NetworkResponse;
import com.tir.user.repository.model.api.parameter.ParamOrders;
import com.tir.user.repository.model.api.parameter.SortBy;
import com.tir.user.repository.model.api.response.ResponseOrders;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by khani on 08/11/2017.
 **/

public class ViewModelHome extends BaseViewModel {

    public final ParamOrders paramCurrentOrders = new ParamOrders();
    public final ParamOrders paramPreviousOrders = new ParamOrders();
    public List<Order> currentOrders;
    public List<Order> previousOrders;
    public int orderId;
    public Order selectedOrder;
    public int selectedPosition;
    private LiveData<NetworkResponse<ResponseOrders>> currentOrdersLive;
    private MediatorLiveData<NetworkResponse<ResponseOrders>> currentOrdersMediator = new MediatorLiveData<>();
    private LiveData<NetworkResponse<ResponseOrders>> previousOrdersLive;
    private MediatorLiveData<NetworkResponse<ResponseOrders>> previousOrdersMediator = new MediatorLiveData<>();
    public boolean notifyItemChanged;
    public boolean notifyItemRemoved;

    @Inject
    protected RepositoryOrder repository;

    public ViewModelHome(Application application) {
        super(application);
        getViewModelComponent().inject(this);
        paramCurrentOrders.category = ParamOrders.Current;
        paramPreviousOrders.category = ParamOrders.Previous;
    }


    public MediatorLiveData<NetworkResponse<ResponseOrders>> getCurrentOrders() {
        if (currentOrdersLive != null) {
            currentOrdersMediator.removeSource(currentOrdersLive);
        }
        currentOrdersLive = repository.getOrders(paramCurrentOrders);
        currentOrdersMediator.addSource(currentOrdersLive, currentOrdersMediator::setValue);
        return currentOrdersMediator;
    }

    public MediatorLiveData<NetworkResponse<ResponseOrders>> getPreviousOrders() {
        if (previousOrdersLive != null) {
            previousOrdersMediator.removeSource(previousOrdersLive);
        }
        previousOrdersLive = repository.getOrders(paramPreviousOrders);
        previousOrdersMediator.addSource(previousOrdersLive, previousOrdersMediator::setValue);
        return previousOrdersMediator;
    }

    public MediatorLiveData<NetworkResponse<Order>> getOrderDetails() {
        return repository.getOrderDetail(orderId);
    }

    public MediatorLiveData<NetworkResponse<Object>> deleteOrder() {
        return repository.deleteOrder(orderId);
    }

    public MediatorLiveData<NetworkResponse<Order>> recreateOrder() {
        return repository.recreateOrder(orderId);
    }

    public void setCurrentFilter(OrderFilter filter) {
        this.paramCurrentOrders.setFilterBy(filter);
    }

    public void setPreviousFilter(OrderFilter filter) {
        this.paramPreviousOrders.setFilterBy(filter);
    }

    public void setCurrentSortBy(SortBy sortBy) {
        paramCurrentOrders.sortBy.clear();
        paramCurrentOrders.sortBy.add(sortBy);
    }

    public void setPreviousSortBy(SortBy sortBy) {
        paramPreviousOrders.sortBy.clear();
        paramPreviousOrders.sortBy.add(sortBy);
    }

    public OrderFilter getCurrentFilter() {
        return paramCurrentOrders.getFilterBy();
    }

    public OrderFilter getPreviousFilter() {
        return paramPreviousOrders.getFilterBy();
    }

    public SortBy getCurrentSortBy() {
        return paramCurrentOrders.sortBy.get(0);
    }

    public SortBy getPreviousSortBy() {
        return paramPreviousOrders.sortBy.get(0);
    }


    @Override
    public RepositoryBasic getBaseRepository() {
        return repository;
    }
}