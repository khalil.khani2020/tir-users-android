package com.tir.user.app;

import com.tir.user.repository.model.room.entity.Session;

/**
 * Created by khani on 26/09/2017.
 */

public class StaticVariables {

    public static Session SESSION;
    public static String CULTURE = Const.Cultures.PERSIAN;
    public static boolean IS_LOGGED_IN;
    public static String CALENDAR_TYPE = "";
    // TODO: 2018/09/30 correct this
    public static int LocationInterval = 12000;
    public static int FastestLocationInterval = 9000;
    public static String SupportPhone = "";
}

