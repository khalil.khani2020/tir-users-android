package com.tir.user.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import androidx.core.content.res.ResourcesCompat;
import androidx.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.tir.user.R;
import com.tir.user.base.BaseActivity;
import com.tir.user.di.application.AppComponent;
import com.tir.user.di.application.AppModule;
import com.tir.user.di.application.DaggerAppComponent;
import com.tir.user.di.application.NetworkModule;
import com.tir.user.repository.SharedPrefsHelper;

import net.danlew.android.joda.JodaTimeAndroid;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.fabric.sdk.android.BuildConfig;
import io.fabric.sdk.android.Fabric;


public class MyApp extends MultiDexApplication {

    private static MyApp mInstance;
    @Inject
    SharedPrefsHelper sharedPrefsHelper;
    private AppComponent appComponent;
    private ArrayList<BaseActivity> activities = new ArrayList<>();

    public static synchronized MyApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
//        configureCrashReporting();
        JodaTimeAndroid.init(this);
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
        appComponent.inject(this);
        sharedPrefsHelper.getPrefs();
        setLanguage();
        Fresco.initialize(this);

//        Fresco.initialize(this);

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public void setLanguage() {
        // TODO: 29/03/2017 add appropriate snippet for setting locale for up to api 24
        String languageToLoad = getCulture();

        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        //        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        //            LocaleList localeList = new LocaleList(locale);
        //            Configuration overrideConfiguration =
        //                    getBaseContext().getResources().getConfiguration();
        //            overrideConfiguration.setLocales(localeList);
        //            Context context = createConfigurationContext(overrideConfiguration);
        //        } else {
        //            //            locale = mContext.getResources().getConfiguration().locale;
        //        }
        setFont(languageToLoad);
    }

    private void setFont(String language) {
        Typeface customTypeface;
        switch (language) {
            case "fa":
                customTypeface = getTypeFace(R.font.iran_sans);
                break;
            default:
                return;
        }
        replaceFont("DEFAULT", customTypeface);
        replaceFont("DEFAULT_BOLD", customTypeface);
        replaceFont("MONOSPACE", customTypeface);
        replaceFont("SERIF", customTypeface);
        replaceFont("SANS_SERIF", customTypeface);
    }

    private void replaceFont(String staticTypefaceFieldName, Typeface customTypeface) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Map<String, Typeface> newMap = new HashMap<>();
                newMap.put("sans-serif", customTypeface);
                final Field staticField = Typeface.class.getDeclaredField("sSystemFontMap");
                staticField.setAccessible(true);
                staticField.set(null, newMap);
            }
            //            else {
            //                final Field staticField =
            // Typeface.class.getDeclaredField(staticTypefaceFieldName);
            //                staticField.setAccessible(true);
            //                staticField.set(null, customTypeface);
            //            }
            final Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, customTypeface);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Typeface getTypeFace(int fontId) {
        return ResourcesCompat.getFont(this, fontId);
    }

    private String getCulture() {
        String lang = Locale.getDefault().getLanguage();
        if (StaticVariables.CULTURE.equals("")) {
            if (lang.equals("fa") || lang.equals("az") || lang.equals("tr") || lang.equals("ru")) {
                StaticVariables.CULTURE = lang;
            } else {
                StaticVariables.CULTURE = "fa";
            }
        }
        return StaticVariables.CULTURE;
    }

    public void setCulture(String language) {
        sharedPrefsHelper.saveCulture(language);
    }

    public boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public int getVersionCode() {
        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getVersionName() {
        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            return info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getPlayServicesVersion() {
        try {
            PackageInfo pi = getPackageManager().getPackageInfo("com.google.android.gms", 0);
            String currentPlayVersion = pi.versionName;
            Pattern pattern = Pattern.compile("([0-9]*-[0-9]*)");
            Matcher matcher = pattern.matcher(currentPlayVersion);

            // TODO: 7/25/2018 check this for lower api's
            if (matcher.find()) {
                String s = matcher.group(1).split("-")[0];
                if (s.length() == 3) {
                    int i = Integer.valueOf(s);
                    s = String.format("%02d%02d%02d",
                            i / 100,
                            (i / 10) % 10,
                            (i % 100) % 10);
                }
                return s;
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void addMe(BaseActivity activity) {
        activities.add(activity);
    }

    public void removeMe(BaseActivity activity) {
        activities.remove(activity);
    }

    public void finishAllActivities() {
        for (BaseActivity activity : activities) {
            activity.finish();
        }
    }



    private void configureCrashReporting() {
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        Fabric.with(this, crashlyticsKit);
    }
}
