package com.tir.user.app;

public class Const {

    public static final String DB_NAME = "tir_db";
    public static final String APP_TAG = "tir--";

    public static class SmsValidationTypes {
        public static final int Register = 10;
        public static final int ResetPassword = 20;
    }

    public static class UserRoles {
        public static final String TRADER = "trader";
        public static final String FORWARDER = "forwarder";
    }

    public static class Cultures {
        public static final String PERSIAN = "fa";
        public static final String ENGLISH = "en";
    }

    public static class Endpoints {
        public static final String SITE_URL = "www.TIR724.com";
        public static final String AGREEMENTS_URL = "https://tir724.com/Terms";
        //        public static final String API_URL = "http://test.nti724.com/";
        public static final String API_URL = "https://api.tir724.com/";
        public static final String URL_GetBasicInfo = "/BasicInfo";
        public static final String URL_login = "/account/login";
        public static final String URL_SendSms = "/PhoneValidation/SendSms";
        public static final String URL_VerifySms = "/PhoneValidation/verify";
        public static final String URL_ResetPassword = "/Account/ResetPassword";
        public static final String URL_Register = "/Account/Register";
        public static final String URL_Logout = "/Account/Logout";
        public static final String URL_GetProfile = "/Account/UserInfo";
        public static final String URL_GetNotifications = "Notifications";
        public static final String URL_UpdateProfile = "/Account";
        public static final String URL_UpdateCar = "/Driver/Profile/UpdateCar";
        public static final String URL_uploadImage = "/UploadImage";
        public static final String URL_GetOrders = "/Trader/Orders";
        public static final String URL_DeleteOrder = "Trader/Orders/Delete";
        public static final String URL_GetPathHistory = "/Trader/Orders/PathHistory";
        public static final String URL_SubmitOrder = "/Trader/Orders/Create";
        public static final String URL_EditOrder = "/Trader/Orders";
        public static final String URL_RecreateOrder = "/Trader/Orders/Recreate";
        public static final String URL_RateDriver = "Trader/Orders/RateDriver";
        public static final String URL_AcceptOrder = "/Driver/Orders/AcceptOrder";
        public static final String URL_PriceOffer = "/Driver/Orders/PriceOffer";
        public static final String URL_DeleteOffer = "/Driver/Orders/DeleteOffer";
        public static final String URL_GetNotifiedShipments = "/Driver/Orders/OfferedToMe";
        public static final String URL_SetOrderStatus = "/Driver/Orders/SetOrderStatus";
        public static final String URL_UpdateFcmToken = "/Accessibility/SetFcmToken";
        public static final String URL_UpdateLocation = "/Accessibility/SendLocation";
        public static final String URL_Play_Services_Google_Play = "https://play.google.com/store/apps/details?id=com.google.android.gms&hl=en";
        public static final String URL_GetRewards = "/Rewards";
        public static final String URL_SpendPoints = "/UserRewards/Spend";
        public static final String URL_GetUserRewards = "/UserRewards";
        public static final String URL_GetPointActions = "/Points";
        public static final String URL_RefreshToken = "/Account/RefreshToken";
    }
}
